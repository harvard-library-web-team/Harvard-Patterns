### Description
This is the standard input pattern

### Status
* Stable as of 1.0.0

### Variant options
* [theme]

### Variables
~~~
input: {
  labelText:
    type: string / required,
  required: 
    type: boolean,
  id: 
    type: string (unique per page) / required
  name: 
    type: string / required
  type:
    type: string (html5 input types => 'text','email', 'number', etc...) / required
  maxlength:
    type: number / optional
  pattern:
    type: string / optional (ex: "[0-9]" for numbers only)
  width:
    type: string (number value) / optional
  placeholder:
    type: string / optional
  errorMsg:
    type: string / optional
}
~~~
