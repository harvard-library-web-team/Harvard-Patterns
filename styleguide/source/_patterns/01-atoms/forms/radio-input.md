### Description
This is a radio input

### Status
* Stable as of 1.0.0


### Variables
~~~
radioInput {
  name:
    type: string / required
  value:
    type: string / required
  label:
    type: string / required
  checked:
    type: boolean
}
~~~
