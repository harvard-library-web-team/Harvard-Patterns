### Description
This is a variant of the [Label](./?p=atoms-label) pattern showing the inline theme version.

### How to generate   
* set the `theme` variable to `inline`
