### Description
This is the standard input label pattern

### Status
* Stable as of 6.0.0

### Variant options   
* [theme]   


### Variables   
~~~   
label: {
  text:   
    type: string / required   
  inputId:
    type: string / optional   
  theme:
    type: string / optional ("", "inline")
}   
~~~
