import React      from "react";
import ReactSVG   from "react-inlinesvg";

import { string, bool, func } from "prop-types";

const propTypes = {
  checked: bool.isRequired,
  icon: string,
  inline: bool,
  label: string.isRequired,
  name: string.isRequired,
  onChange: func.isRequired,
  subLabel: string,
  theme: string,
  value: string.isRequired
};

const CheckboxFilter = (props) => {

  const handleChange = (event) => {
    props.onChange(event);
  };

  const { name, value, label, subLabel = "", checked, icon, inline, theme = "dark" } = props;
  const inlineClass = inline ? "hl__checkbox-filter--inline" : "";
  const themeClass = theme ? `hl__checkbox-filter--${ theme }` : "";

  return (
    <label className={ `hl__checkbox-filter ${themeClass} ${inlineClass}` }>
      <input 
        name={ name } 
        type="checkbox" 
        value={ value } 
        checked= { checked }
        onChange= { handleChange }
        className="hl__checkbox-filter__label" />
      <div className="hl__checkbox-filter__container">
        { icon && !inline && (
          <span className="hl__checkbox-filter__icon">
            <img src={ icon } alt="" />
          </span>
        )}
        { icon && inline && (
          <ReactSVG src={ icon } className="hl__checkbox-filter__icon"/>
        )}
        { subLabel === "" && (
          <span className="hl__checkbox-filter__label">
            { label }
          </span>
        )}
        { subLabel !== "" && (
          <span className="hl__checkbox-filter__label">
            { label } &mdash; <small>{ subLabel }</small>
          </span>
        )}
      </div>
    </label>
  );
};

CheckboxFilter.propTypes = propTypes; // defined at the top of the file

export default CheckboxFilter;
