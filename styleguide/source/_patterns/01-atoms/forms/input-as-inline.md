### Description
This is a variant of the [Input](./?p=atoms-input) pattern showing the inline theme version.

### How to generate
* set the `theme` variable to `inline`
