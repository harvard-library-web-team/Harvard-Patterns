### Description
To support the design aesthetic:

* <strong>Crimson to Cambridge Red</strong>: use for some icons and as a background on filters and subnavigation
* <strong>Dark Blue to Breakthrough Blue</strong>: use as a color wash over hero images for "How To" pages and as a background on filters for pages geared toward Harvard students
* <strong>Bookmark Blue to Light Bookmark Blue</strong>: use as a background on any filters and blocks related to Harvard Library collections
* <strong>Brilliant Yellow to Light Yellow</strong>: use as a background on filters for services and tools
