### Description
To support the design aesthetic:

* <strong>#1E1E1E</strong>: for headings and prominent text
* <strong>#414141</strong>: for most body copy
* <strong>#6C6C6C</strong>: for subtle text and some icons
* <strong>#C0C0C0</strong>: for dividers and borders
* <strong>#F3F3F3</strong>: for background shading
* <strong>#F9F9F9</strong>: for background shading
