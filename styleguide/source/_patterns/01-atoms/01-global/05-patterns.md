### Description
Use these patterns to add texture and dimension to backgrounds on some components and calls-to-action. Use discretion when applying patterns so as not to distract, overwhelm, or impact readability.
