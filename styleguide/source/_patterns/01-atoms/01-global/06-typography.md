### Description
<strong>Trueno</strong>

Trueno is the primary typeface used on Harvard Library’s website. It originated from Montserrat and therefore has a similar look and feel. Trueno is used for primary headers, navigation, links, buttons, and callout text.

<strong>Lora</strong>

Lora is the secondary typeface used on Harvard Library’s website. It is a well-balanced contemporary serif with roots in calligraphy. It is a text typeface with moderate contrast well suited for body text. Lora is used for body text, subtitles, and quotes.
