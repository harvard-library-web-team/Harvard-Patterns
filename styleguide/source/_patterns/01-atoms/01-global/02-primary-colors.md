### Description
These are the colors that make up Harvard Library’s website. The primary color palette includes bright, vibrant colors to represent our dynamic library, while the Harvard crimson and a slate blue are used to illustrate the traditions of our iconic brand.

* <strong>Breakthrough Blue</strong>: use this color for links, buttons, and as an accent element for content geared towards students at Harvard
* <strong>Cambridge Red</strong>: pair this color in a gradient with Crimson to brighten decorative background elements.
* <strong>Harry's Crimson</strong>: Harvard’s signature color, use in areas that call for a more familiar Harvard feel
* <strong>Bookmark Blue</strong>: use this muted hue to represent Harvard Library’s collections across  the site
* <strong>Brilliant Yellow</strong>: use this bright accent color to call attention to dates, hours, services and tools
