### Description
Displays a figure using the figure HTML element.

### Status
* alpha

### Components
Image

### Variables
~~~
figure: {
  image: {
    type: Image /required
  }
  caption:
    type: string /optional
  credit:
    type: string /optional
  align:
    type: string ("left","right","") / optional
}

~~~
