### Description
Displays a figure in a bubble shape using the figure HTML element.

### Status
* alpha

### Components
Image

### Variables
~~~
bubbleFigure: {
  image: {
    type: Image /required
  }
  caption:
    type: string /optional
  description:
    type: string /optional
}
~~~
