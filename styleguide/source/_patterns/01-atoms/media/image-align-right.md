### Description
This is a variant of the [Image](./?p=atoms-image) pattern showing an example aligned to the right.

### How to generate
* set the `align` variable to 'right'
