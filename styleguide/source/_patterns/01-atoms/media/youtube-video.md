### Description
This pattern shows a youTube video using an iframe.

### Status
* Stable as of 1.0.0

### Variables
~~~
youtubeVideo: {
  src: 
    type: string (embed url) / required,
  label: 
    type: string / required,
  width: 
    type: string / optional,
  height: 
    type: string / optional
}
~~~
