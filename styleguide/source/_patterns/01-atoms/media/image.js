export default () => {
  
  if ('srcset' in document.createElement('img')) {
    return;
  }

  let script    = document.createElement("script");
  script.type   = "text/javascript";
  script.src    = "../../assets/js/vendor/picturefill.js";
  script.async  = true;
  (document.getElementsByTagName('head')[0]).appendChild(script);
  
};
