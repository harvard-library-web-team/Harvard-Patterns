### Description
This is a variant of the [Figure](./?p=atoms-figure) pattern showing it aligned to the left.

### How to generate
* set the `align` variable to `left`
