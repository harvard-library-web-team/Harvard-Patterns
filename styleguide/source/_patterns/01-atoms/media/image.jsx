import React      from "react";

import { string } from "prop-types";

const propTypes = {
  align: string,
  alt: string,
  src: string.isRequired,
  height: string,
  width: string
};

const Image = (props) => {

  const { align, alt, src, height, width } = props;

  const imageAlign = align ? `hl__image--${ align }` : "";

  return (
    <img 
        className={ `hl__image${ imageAlign }` }
        alt={ alt }
        src={ src }
        height={ height } 
        width={ width } />
  );
};

Image.propTypes = propTypes; // defined at the top of the file

export default Image;
