### Description
This is a variant of the [Figure](./?p=atoms-figure) pattern showing it aligned to the right.

### How to generate
* set the `align` variable to `right`
