### Description
Displays an image using the image HTML element

### Status
* aplha

### Variables
~~~
image {
  align:
    type: string ("left","right","") / optional
  alt:
    type: string / required
  height:
    type: string / required
  width:
    type: string / required
  src:
    type: string / optional
  sizes:
    type: array of string / optional
  srcset:
    type: array of string / required (ignored if src is used)
}
~~~
