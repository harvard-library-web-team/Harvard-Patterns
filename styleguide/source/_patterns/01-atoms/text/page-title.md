### Description
This pattern contains an `<h1>` used as a Title for the various pages.

### Status
* Stable as of 1.0.0

### Variables
~~~
pageTitle: {
  text: 
    type: string / required,
}
~~~
