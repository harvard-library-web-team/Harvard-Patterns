### Description
This is a variant of the [Highlighted Text](./?p=atoms-highlighted-text) pattern showing an example set to show as an outline.

### How to generate
* set the `outline` variable to `true`
