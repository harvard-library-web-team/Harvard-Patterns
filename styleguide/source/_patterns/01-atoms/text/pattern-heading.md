### Description
This pattern shows a title with a background pattern and top border.

### Status
* Stable as of 1.0.0

### Variables
~~~
patternHeading: {
  level: 
    type: number / optional,
  text: {
    type: string / required
  }
}
~~~
