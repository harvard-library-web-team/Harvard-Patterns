### Description
This pattern shows an eyebrow for use in a Page Header.

### Status
* Stable as of 1.0.0

### Variables
~~~
eyebrow: {
  text: 
    type: string / required,
  icon: 
    type: string (svg path) / optional
}
~~~
