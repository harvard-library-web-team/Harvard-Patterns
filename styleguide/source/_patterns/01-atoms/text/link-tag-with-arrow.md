### Description
This is a variant of the [Link Tag](./?p=atoms-link-tag) pattern showing an example with an additional style element.

### How to generate
* set the `withArrow` variable to `true`
