### Description
This is pattern is used as a title for components on a page.

### Status
* Stable as of 1.0.0

### Variant options
* This pattern can also be rendered [centered](./?p=atoms-comp-heading-centered)

### Variables
~~~
compHeading: {
  text: 
    type: string / required,
  level:
    type: number / optional (defaults to 2)
  theme: 
    type: string ("", "small") / optional
  align: 
    type: string / optional
}
~~~
