### Description
This pattern shows text formated in a yellow box and is usually used to show event dates.

### Status
* Stable as of 1.0.0

### Variant options
* This pattern can also be render as an [outline](./?p=atoms-highlighted-text-as-outline)

### Variables
~~~
highlightedText: {
  colorInverted: 
    type: boolean,
  outline:
    type: boolean
  invertedColor:
    type: boolean
  text: 
    type: string / required
}
~~~
