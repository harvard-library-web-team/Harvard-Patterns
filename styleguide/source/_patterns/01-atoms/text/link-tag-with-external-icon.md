### Description
This is a variant of the [Link Tag](./?p=atoms-link-tag) pattern showing an example with an additional style element.

### How to generate
* set the `externalIcon` variable to `true`
* set the `externalIconColor` variable to `white` or `blue` for a different icon color (default is `black`)
