### Description
This is a variant of the [Comp Heading](./?p=atoms-comp-heading) pattern showing an example with the text centered.

### How to generate
* set the `algin` variable to `center`
