### Description
This is a variant of the [Statistic](./?p=atoms-statistic) pattern showing an example centered.

### How to generate
* set the `align` variable to "centered"
