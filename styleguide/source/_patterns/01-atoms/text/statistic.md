### Description
This is pattern shows a statistic number and corresponding description text.

### Status
* Stable as of 1.0.0

### Variant options
* This pattern can also be rendered [centered](./?p=atoms-statistic-centered)

### Variables
~~~
"statistic": {
  value:
    type: string / required
  description:
    type: string / required
  align:
    type: string / optional
}
~~~
