### Description
A circled number list which outputs an `<ol>` element with its child `<li>` elements.

### Status
* Alpha

### Variables:
~~~
circledNumberedList: {
  items: [{ 
    text:
      type: string / required
  }]
}
~~~
