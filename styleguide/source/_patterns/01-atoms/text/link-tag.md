### Description
This is a sample of link `a` element.

### Status
* Stable as of 1.0.0

### Variant options
* This pattern can also be rendered with an [arrow](./?p=atoms-link-tag-with-arrow) or [external icon](./?p=atoms-link-tag-with-external-icon)

### Usage Guideline
* When this pattern is used to control an accordion, the `toggleText` is used as the text for the alternative state (closed or open) instead of the `text`

### Variables
~~~
linkTag: {
  href:
    type: string / required,
  text:
    type: string / required
  info:
    type: string / optional
  withArrow:
    type: boolean / optional
  cssClass:
    type: string / optional
  toggleText:
    type: string / optional
  label:
    type: string / optional
  externalIcon:
    type: boolean / optional
  externalIconColor:
    type: string / optional
}
~~~
