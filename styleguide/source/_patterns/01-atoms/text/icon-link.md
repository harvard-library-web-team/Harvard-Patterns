### Description
This is pattern shows an svg icon wrapped in a round link.

### Status
* Stable as of 1.0.0

### Variables
~~~
iconLink: {
  href: 
    type: string / required,
  info:
    type: string / required
  icon: 
    type: string (path to SVG twig file) / required
}
~~~
