### Description
Displays a paragraph of text. 

### Status
* Alpha

### Variables:
~~~
paragraph {
  text:
    type: string
  cssClass:
    type: string / optional
}
~~~
