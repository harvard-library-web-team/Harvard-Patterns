### Description
An `<ol>` or `<ul>`  element with its child `<li>` elements and optional nested child `<ol>` or `<ul>` with its `<li>` elements.

### Status
* Alpha

### Variables:
~~~
list: {
  ordered:
    type: boolean
  items: [{ 
    text:
      type: string / required
    subitems (optional) [{ 
      text:
        type: string / required
    }]
  }]
}
~~~
