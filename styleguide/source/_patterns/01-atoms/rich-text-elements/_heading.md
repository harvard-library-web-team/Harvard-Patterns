### Description
This is a Heading atom for use in a Rich Text editor.

### Status
* Stable as of 5.0.0

### Pattern Contains
* Comp Heading
* Contact Us

### Variant options
* The heading can be rendered in different levels (h1 - h6) by setting the `level` variable.

### Variables
~~~
heading: {
  level: 
    type: number / required,
  text: {
    type: string / required
  }
}
~~~
