import React from "react";
import ReactDOM from "react-dom";

import { string } from "prop-types";

const propTypes = {
  fill: string,
  width: string,
  height: string,
  viewBox: string
}

const GridSVG = (props) => {
  return (
    <svg
      width="24px"
      height="24px"
      viewBox="0 0 24 24"
      xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink"
      fill="#f9f9f9">
      <g transform="translate(-1517.000000, -661.000000)" >
        <g transform="translate(0.000000, 597.000000)">
          <g transform="translate(1455.000000, 53.000000)">
              <path d="M62,22 L73,22 L73,11 L62,11 L62,22 L62,22 Z M62,35 L73,35 L73,24 L62,24 L62,35 L62,35 Z M75,35 L86,35 L86,24 L75,24 L75,35 L75,35 Z M75,11 L75,22 L86,22 L86,11 L75,11 L75,11 Z"></path>
          </g>
        </g>
      </g>
    </svg>
  )
}

GridSVG.propTypes = propTypes; // defined at the top of the file

export default GridSVG;
