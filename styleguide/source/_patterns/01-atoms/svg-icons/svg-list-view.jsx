import React from "react";
import ReactDOM from "react-dom";

import { string } from "prop-types";

const propTypes = {
  fill: string,
  width: string,
  height: string,
  viewBox: string
}

const ListSVG = (props) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      xmlnsXlink="http://www.w3.org/1999/xlink"
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="#f9f9f9">
      <g>
        <g transform="translate(-1517 -653)">
          <path d="M1517 670v-3h10.9v3zm0 7v-3h10.9v3zm0-14v-3h10.9v3zm0-10h10.9v3H1517zm13.1 17v-3h10.9v3zm-.1 7v-3h10.9v3zm.1-14v-3h10.9v3zm0-10h10.9v3h-10.9z"/>
        </g>
      </g>
    </svg>
  )
}

ListSVG.propTypes = propTypes; // defined at the top of the file

export default ListSVG;
