import React    from "react";
import ReactDOM from "react-dom";

import { string } from "prop-types";

const propTypes = {
  fill: string,
  width: string,
  height: string,
  viewBox: string
}

const MasonrySVG = (props) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      xmlnsXlink="http://www.w3.org/1999/xlink"
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="#f9f9f9">
      <g>
        <g transform="translate(-1468 -653)">
          <path d="M1468 666v-13h11v13zm0 11v-8h11v8zm13 0v-13h11v13zm0-24h11v8h-11z"/>
        </g>
      </g>
    </svg>
  )
}

MasonrySVG.propTypes = propTypes; // defined at top of the filter

export default MasonrySVG;
