import React from "react";
import ReactDOM from "react-dom";

import { string } from "prop-types";

const propTypes = {
  fill: string,
  width: string,
  height: string,
  viewBox: string
}

const SingleListSVG = (props) => {
  return (
    <svg
      width="24px"
      height="24px"
      viewBox="0 0 24 24"
      xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink"
      fill="#f9f9f9">
      <g transform="translate(-1468.000000, -661.000000)">
        <g transform="translate(0.000000, 597.000000)">
          <g transform="translate(1455.000000, 53.000000)">
            <path d="M29.9090909,25 L37,25 L37,28 L29.9090909,28 L13,28 L13,25 L29.9090909,25 Z M31.9090909,32 L36.9090909,32 L36.9090909,35 L31.9090909,35 L13,35 L13,32 L31.9090909,32 Z M28.9090909,18 L37,18 L37,21 L28.9090909,21 L13,21 L13,18 L28.9090909,18 Z M26.9090909,11 L37,11 L37,14 L26.9090909,14 L13,14 L13,11 L26.9090909,11 Z" ></path>
          </g>
        </g>
      </g>
    </svg>
  )
}

SingleListSVG.propTypes = propTypes; // defined at the top of the file

export default SingleListSVG;
