import React from "react";
import ReactDOM from "react-dom";

import { string } from "prop-types";

const propTypes = {
  iconName: string,
  fill: string,
  width: string,
  height: string,
  viewBox: string
}

const GiftSVG = (props) => {
  // const iconClass = iconName ? `hl__icon--${iconName}` : "";
  const iconClass = "hl__icon--gift";

  return (
    <span className={`hl__icon ${iconClass}`}>
      <svg
      width="20"
      height="20"
      viewBox="0 0 20 20"
      xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink"
      fill="#f9f9f9">
        <g transform="translate(-879.000000, -680.000000)"
          fillRule="nonzero">
            <g transform="translate(0.000000, 598.000000)">
                <g transform="translate(879.000000, 82.000000)">
                    <path d="M19,5 L16.2,5 C16.7,4.5 17,3.8 17,3 C17,1.3 15.7,0 14,0 C13,0 11.2,0.9 10,2.1 C8.8,0.9 7,0 6,0 C4.3,0 3,1.3 3,3 C3,3.8 3.3,4.5 3.8,5 L1,5 C0.4,5 0,5.4 0,6 L0,8.8 C0,8.9 0.1,9 0.2,9 L8.6,9 C8.8,9 9,8.8 9,8.6 L9,5.4 C9,5.2 9.2,5 9.4,5 L10.6,5 C10.8,5 11,5.2 11,5.4 L11,8.6 C11,8.8 11.2,9 11.4,9 L19.8,9 C19.9,9 20,8.9 20,8.8 L20,6 C20,5.4 19.6,5 19,5 Z M6,4 C5.4,4 5,3.6 5,3 C5,2.4 5.4,2 6,2 C6.6,2 8.4,3.2 8.9,3.8 C8.6,3.9 6,4 6,4 Z M14,4 C14,4 11.4,3.9 11.1,3.8 C11.7,3.1 13.4,2 14,2 C14.6,2 15,2.4 15,3 C15,3.6 14.6,4 14,4 Z"></path>
                    <path d="M8.6,11 L1.2,11 C1.1,11 1,11.1 1,11.2 L1,19 C1,19.6 1.4,20 2,20 L8.6,20 C8.8,20 9,19.8 9,19.6 L9,11.4 C9,11.2 8.8,11 8.6,11 Z"></path>
                    <path d="M18.8,11 L11.4,11 C11.2,11 11,11.2 11,11.4 L11,19.6 C11,19.8 11.2,20 11.4,20 L18,20 C18.6,20 19,19.6 19,19 L19,11.2 C19,11.1 18.9,11 18.8,11 Z"></path>
                </g>
            </g>
        </g>
      </svg>
    </span>
  )
}

GiftSVG.propTypes = propTypes; // defined at the top of the file

export default GiftSVG;
