### Description
This is a variant of the [Button](./?p=atoms-button) pattern showing the inline theme version.

### How to generate
* set the `theme` variable to `inline`
