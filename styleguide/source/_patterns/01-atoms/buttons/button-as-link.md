### Description
This is a variant of the [Button](./?p=atoms-button) pattern showing the button using a link (`a`) instead of a button.

### How to generate
* populate the `href` variable with a valid url
