### Description
This is a pattern of an embedded Springshare chat widget.

### Status
* Stable

### JavaScript Used
* This pattern uses JavaScript that is generated when you create a new chat widget in Springshare. Copy and paste that code into the _01-foot.twig file.

### Variables
~~~
chatButton: {
  id:
    type: string / required (id generated in Springshare)
}
~~~
