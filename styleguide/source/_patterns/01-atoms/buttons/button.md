### Description
This is the standard button pattern

### Status
* Stable as of 6.0.0

### Variant options
* [link](./?p=atoms-button-as-link) instead of a button
* [inline](./?p=atoms-button-as-inline)
* [outline](./?p=atoms-button-as-outline)
* [secondary (blue)](./?p=atoms-button-as-secondary)
* [small](./?p=atoms-button-as-small)
* [with icon](./?p=atoms-button-with-icon)

### Variables
~~~
button: {
  href:
    type: string (url) / optional
  info:
    type: string / optional
  text:
    type: string / required
  type:
    type: string / optional (ex: "button", "submit")
  size:
    type: string / optional ("" or "small")
  theme:
    type: string / optional ("", "secondary", or "quaternary")
  outline:
    type: boolean,
  cssClass:
    type: string / optional,
  icon:
    type: strong (url) / optional
}
~~~
