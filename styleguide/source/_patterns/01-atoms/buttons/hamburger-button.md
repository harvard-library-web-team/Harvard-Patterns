### Description
This is a Hamburger Menu button.

### Status
* Stable as of 1.0.0

### JavaScript Used
* This pattern uses JavaScript to toggle a '.is-open' class

### Variables
~~~
hamburgerButton: {
  text: 
    type: string / required
}
~~~
