### Description
This is a variant of the [Button](./?p=atoms-button) pattern showing a small version.

### How to generate
* set the `size` variable to `small`
* set the `outline` variable to `true`
