import $ from "jquery";

export default function (window = window, document = document) {
  const $el = $(".js-hamburger-button");
  const $mainNavLinks = $(".js-main-nav-top-link");
  const $menus = $(".js-main-nav-menu");
  const keyCodes = {
    tab: 9,
    up: 38,
    down: 40,
    left: 37,
    right: 39,
    enter: 13,
    space: 32,
    escape: 27
  };

  const $quickLinks = $(".js-main-nav .js-quick-links"); // more that one

  let debounce = null;
  let documentWidth = $(document).width();

  // toggle the open class on the button
  $el.on("click", function() {
    if($el.hasClass("is-open")){
      close();
      setTimeout(()=> $el.focus(), 600);
    } else {
      open();
      // wait for css animation to finish (.5 secs)
      setTimeout(()=> $mainNavLinks.first().focus(), 600);
    }
  });

  $quickLinks.last().on("keydown", function(e) {
    switch (e.keyCode) {
      case keyCodes.tab:
        if($el.hasClass("is-open")) {
          close();
        }
        break;
      default:
    }
  });

  $el.on("keydown", function(e) {
    switch (e.keyCode) {
      case keyCodes.escape:
        e.preventDefault();
        close();
        break;
      case keyCodes.down:
      case keyCodes.enter:
      case keyCodes.space:
        e.preventDefault();
        if($el.hasClass("is-open")) {
          close();
        } else {
          open();
          // wait for css animation to finish (.5 secs)
          setTimeout(()=> $mainNavLinks.first().focus(), 600);
        }
        break;
      default:
    }
  });

  function open() {
    $el.addClass("is-open");
    $el.attr("aria-expanded", "true");
    $quickLinks.removeAttr("tabIndex");
  }

  function close() {
    $el.removeClass("is-open");
    $el.attr("aria-expanded", "false");
    $menus.removeClass("is-open");
    $mainNavLinks.attr("aria-expanded", "false");
    $quickLinks.attr({tabIndex:"-1"});
  }

  $mainNavLinks.on("keydown", function(e){
    if(e.keyCode === keyCodes.escape || e.keyCode === keyCodes.left) {
      $el.removeClass("is-open");
      $el.attr("aria-expanded", "false");
      setTimeout(()=> $el.focus(), 600);
    }
  });

  // reset buttons on resize
  $(window).resize(function(){
    if(documentWidth === $(document).width()) {
      return;
    } else {
      documentWidth = $(document).width();
    }

    clearTimeout(debounce);
    debounce = setTimeout(() => {
      $el.removeClass("is-open");
      $el.attr("aria-expanded", "false");
    },300);
  });
}
