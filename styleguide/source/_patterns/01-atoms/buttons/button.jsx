import React from "react";

import { string, bool, func } from "prop-types";

const propTypes = {
  href: string,
  info: string,
  text: string.isRequired,
  type: string,
  size: string,
  theme: string,
  outline: bool,
  cssClass: string,
  handleClick: func
};

// ImageCTA is a stateless functional component.
const Button = (props) => {
  const { href, info, text, type="button", size, theme, outline=false, cssClass="", handleClick } = props;

  const buttonSize = size ? `hl__button--${size}` : "";
  const buttonStyle = outline ? "hl__button--minor" : "";
  const buttonTheme = theme ? `hl__button--${theme}` : "";

  const buttonClass = `hl__button ${buttonSize} ${buttonTheme} ${buttonStyle} ${cssClass}`;

  if(href) {
    return (
      <a 
        href={ href } 
        className={ buttonClass }
        title={ info }>{ text }</a>
    );
  } else {
    return (
      <button 
        type={ type } 
        className={ buttonClass }
        aria-label={ info }
        onClick={ handleClick }>{ text }</button>
    );
  }
};

Button.propTypes = propTypes; // defined at the top of the file

export default Button;
