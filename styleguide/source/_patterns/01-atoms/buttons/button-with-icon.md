### Description
This is a variant of the [Button](./?p=atoms-button) pattern showing a optional icon.

### How to generate
* set the `icon` variable to the path of the icon twig file
