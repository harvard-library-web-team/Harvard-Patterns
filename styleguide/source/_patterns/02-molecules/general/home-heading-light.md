### Description
This is a variant of the [Home Heading](./?p=molecules-home-heading) pattern with a light image and dark background around the source.

### How to generate
* set `source.theme` to `light`
