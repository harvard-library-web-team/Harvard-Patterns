### Description
This pattern shows today's date using JavaScript followed by a tabular list of items with a see all link.

### Status
* Alpha

### Pattern Contains
* Link Tag

### Variables
~~~
libraryHours: {
  icon: string (path to icon) / required,
  hoursToday: {
    text: string / required
  }
  seeAll: {
    type: linkTag / optional
  }
}
~~~
