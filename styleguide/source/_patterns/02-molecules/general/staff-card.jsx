import React        from "react";
import ReactSVG     from "react-inlinesvg";

import { string, shape, bool } from "prop-types";

const propTypes = {
  assetsUrl: string.isRequired,
  href: string.isRequired,
  firstName: string.isRequired,
  lastName: string.isRequired,
  img: shape ({
    src: string
  }),
  jobTitle: string,
  email: string
};

const StaffCard = (props) => {

  const {
    assetsUrl,
    href,
    firstName,
    lastName,
    img,
    jobTitle,
    email } = props;

  return (
    <section className="hl__staff-card">
      <h2 className="hl__staff-card__name">
        <a className="hl__link-tag" href={ href } title={ `more about ${firstName} ${lastName}` }>{ firstName } { lastName }</a>
      </h2>

      <div className="hl__staff-card__email">
        <a className="hl__link-tag" href={`mailto:${email}`} title={ `email ${firstName} ${lastName}`}>
          <span>{ email }</span>
        </a>
      </div>

      <div className="hl__staff-card__job-title">
        { jobTitle }
      </div>


      <div className="hl__staff-card__image">
        { img && (
          <img src={ img.src } />
        )}
      </div>

    </section>
  );
};

StaffCard.propTypes = propTypes; // defined at the top of the file

export default StaffCard;
