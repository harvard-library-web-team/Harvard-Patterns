### Description
This is a variant of the [Home Heading Child](./?p=molecules-home-heading-child) pattern with a light image and dark background around the source.

### How to generate
* set `source.theme` to `light`
