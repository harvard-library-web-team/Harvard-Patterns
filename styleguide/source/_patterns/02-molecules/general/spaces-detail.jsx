import React, { Component }   from "react";
import ReactSVG   from "react-inlinesvg";

import Button                 from "../../01-atoms/buttons/button.jsx";
import IllustratedList        from "../../02-molecules/lists/illustrated-list.jsx";
import ImageCarousel          from "../../02-molecules/media/image-carousel.jsx";
import LibraryAddress         from "../../02-molecules/text/library-address.jsx";

import { string, array, arrayOf, number, shape, object, bool } from "prop-types";

export default class SpacesDetail extends Component {

  static propTypes = {
    assetsUrl: string.isRequired,
    booking_link: string,
    contact: string,
    description: string,
    features: array,
    floor: string,
    images: array,
    library: object,
    name: string,
    noise: shape({
      items: arrayOf(shape({
        value: string
      }))
    }),
    noise_level: string,
    open_space: bool,
    room_type: string,
    seats: string,
    what_you_need_to_know: string
  };

  renderNoise = (noiseFilters, noiseLevel) => {
    const temp = noiseFilters.items.filter(n => n.value === noiseLevel)[0];
    return (
      <div className="hl__spaces-detail__noise-wrapper">
        <ReactSVG className="hl__spaces-detail__noise-icon" src={ temp.icon } />
        <span className="hl__spaces-detail__noise-label">
          { temp.label }
        </span>
      </div>
    );
  }

  createFeatures = (features) => {
    return features.map(feature => {
      let temp = Object.assign({},feature);

      if(feature.name) {
        temp.text = feature.name;
      }
      if(feature.icon) {
        temp.icon = {
          src: feature.icon,
          height: "25",
          width: "25"
        };
      } else {
        temp.icon = {};
      }
      return temp;
    });
  }

  render() {
    const {
      assetsUrl,
      booking_link,
      contact,
      description,
      features,
      floor,
      images,
      library,
      name,
      noise,
      noise_level,
      open_space,
      room_type,
      seats,
      what_you_need_to_know
    } = this.props;

    const hideSeats = open_space || !seats;

    const button = { 
      href: booking_link,
      info: "reserve this room",
      text: "Reserve",
      type: "button",
      theme: "secondary"
    };

    return (
      <section className="hl__spaces-detail">
        <header>
          { images && images.length > 0 && (
            <div className="hl__spaces-detail__images">
              <ImageCarousel images= { images } />
            </div>
          )}
          <h2 className="hl__spaces-detail__title">{ name } &mdash; { floor }</h2>
          { hideSeats && (
            <h3 className="hl__spaces-detail__sub-title">{ room_type }</h3>
          )}
          { !hideSeats && (
            <h3 className="hl__spaces-detail__sub-title">{ room_type } &mdash; { seats } seats</h3>
          )}
          <div className="hl__spaces-detail__description">
            <div className="hl__rich-text" dangerouslySetInnerHTML={ {__html: description } } />
          </div>
        </header>

        { noise_level && (
          <div className="hl__spaces-detail__noise">
            <h3 className="hl__spaces-detail__section-title">Noise Level</h3>
            { this.renderNoise(noise, noise_level) }
          </div>
        )}
        { features && (
          <div className="hl__spaces-detail__features">
            <h3 className="hl__spaces-detail__section-title">Features</h3>
            <IllustratedList theme="small" items={ this.createFeatures(features) } />
          </div>
        )}
        <div className="hl__spaces-detail__booking">
          { (contact || booking_link) && (
            <h3 className="hl__spaces-detail__section-title">Interested in Booking this Space?</h3>            
          )}
          { booking_link && (
            <Button { ... button } />
          )}
          { !booking_link && contact && (
            <div className="hl__spaces-detail__booking-email">
              <ReactSVG src="../../assets/images/svg-icons/email.svg" className="hl__spaces-detail__booking-icon"/>
              <a className="hl__link-tag" href={ `mailto:${ contact}` }>{ contact }</a>
            </div>
          )}
        </div>
        { what_you_need_to_know && (
          <div className="hl__spaces-detail__additional-info">
            <h3 className="hl__spaces-detail__section-title">What you need to know</h3>
            <div className="hl__rich-text" dangerouslySetInnerHTML={ {__html: what_you_need_to_know }} />
          </div>
        )}
        { library && (
          <div className="hl__spaces-detail__library">
            <LibraryAddress 
              assetsUrl={ assetsUrl }
              { ...library } />
          </div>
        )}
      </section>
    );
  }
}
