### Description
This Pattern shows an round image and an optional link 

### Status
* Stable as of 1.0.0

### Pattern Contains
* Image
* Link Tag

### Variables
~~~
userBanner: {
  image: {
    type: image / required
  },
  link: {
    type: linkTag / optional
  }
}
~~~
