import $ from "jquery";

export default function (window = window, document = document) {
  var $el = $(".hl__home-heading");
  var $searchForm = $el.find("form");
  var $searchInput = $el.find("input");
  var $searchInputGroup = $el.find(".hl__single-input-form__input-group");
  var $searchButton = $el.find(".hl__button__text");

  // change button text for mobile view
  // check width on initial load
  if ($(window).width() < 480) {
    $searchButton.text("HOLLIS");
  } else {
    $searchButton.text("Search HOLLIS");
  }

  // check width when resizing window
  $(window).resize(function() {
    if ($(window).width() < 480) {
      $searchButton.text("HOLLIS");
    } else {
      $searchButton.text("Search HOLLIS");
    }
  });

  $searchForm.on("submit", function(e){
    let searchTerm = $searchInput.val();
    let query = 'any,contains,'+searchTerm;
    $searchInputGroup.append( $('<input type="hidden" name="tab" value="everything">'));
    $searchInputGroup.append( $('<input type="hidden" name="vid" value="HVD2">'));
    $searchInputGroup.append( $('<input type="hidden" name="lang" value="en_US">'));
    $searchInputGroup.append( $('<input type="hidden" name="offset" value="0">'));
    $searchInputGroup.append( $('<input type="hidden" name="query" value="'+query+'">'));
  });

}
