### Description
This pattern shows a left-aligned title and search box for a child theme homepage with image as the background.

### Status
* Stable as of 1.0.0

### Variants
* [Home Heading Child Light](./?p=molecules-home-heading-child-light)
* [Home Heading Child Callout](./?p=molecules-home-heading-child-callout)
* [Home Heading Child No Background](./?p=molecules-home-heading-child-no-background)
* [Home Heading Child No Search Box](./?p=molecules-home-heading-child-no-search)

### Pattern Contains
* Single input form
* Rich text

### Variables
~~~
homeHeading: {
  bgNarrow: {
    image: string (path to image) / optional
  }
  bgWide: {
    image: string (path to image) / optional
  }
  titleSmall: {
    text: string / optional
  }
  titleLarge: {
    text: string / optional
  }
  helperText: {
    text: string / optional
  }
  source: {
    theme: string / optional
    href: string / optional
  }
  library: {
    href: string / optional
  }
  singleInputForm: {
    type: input / required
  }  
}
~~~
