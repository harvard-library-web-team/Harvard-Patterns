### Description
This patterns is used to show brief information about a staff member.

### Status
* Stable as of 1.0.0

### Pattern Contains
* Contact Info
* Link Tag
* Rich Text

### JavaScript Used
* This pattern is also built in JSX for the staff directory page

### Variables
~~~
staffCard: {
  name: {
    type: linkTag / required
  },
  jobTitle: {
    type: string / optional
  },
  email: {
    type: contactList / optional
  },
  jobTitle: {
    type: string / optional
  },
  image: {
    src:
      type: string (image path) / optional
  }
}
~~~
