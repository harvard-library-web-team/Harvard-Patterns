### Description
This pattern shows a centered title and search box for the homepage with image as the background.

### Status
* Stable as of 1.0.0

### Variants
* [Home Heading Light](./?p=molecules-home-heading-light)

### Pattern Contains
* Single input form
* Rich text

### Variables
~~~
homeHeading: {
  bgNarrow: {
    image: string (path to image) / optional
  }
  bgWide: {
    image: string (path to image) / optional
  }
  titleSmall: {
    text: string / optional
  }
  titleLarge: {
    text: string / optional
  }
  helperText: {
    text: string / optional
  }
  source: {
    theme: string / optional
    href: string / optional
  }
  library: {
    href: string / optional
  }
  singleInputForm: {
    type: input / required
  }  
}
~~~
