### Description
This pattern shows an optional button to register for an event a label about restrictions and an optional save to calendar link.

### Status
* Stable as of 1.0.0

### Pattern Contains
* Button
* Link Tag

### Variables
~~~
registrationCTA: {
  button: {
    type: button / optional
  },
  type: 
    type: string / required,
  restricted: 
    type: boolean,
  calendar: {
    type: linkTag / optional
  }
}
~~~
