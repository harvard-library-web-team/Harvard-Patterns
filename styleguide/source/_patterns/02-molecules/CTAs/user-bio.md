### Description
This pattern shows information about a user and allows the user to contact that user.

### Status
* Stable as of 1.0.0

### Pattern Contains
* Button
* Image

### Variables
~~~
userBio: {
  level: 
    type: number / optional,
  image: {
    type: image / required
  },
  name: 
    type: string / required
  title: 
    type: string / required
  button: {
    type: button / required
  },
  tagsTitle: 
    type: string / required
  tags: [
    type: array of strings / required
  ]
}
~~~
