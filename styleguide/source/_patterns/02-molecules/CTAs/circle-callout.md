### Description
This pattern shows a short summary with a decorated icon above it.

### Status
* Stable as of 1.0.0

### Variables
~~~
circleCallout: {
  icon: 
    type: string (svg path) / optional,
  description: 
    type: string (html) / required
}
~~~
