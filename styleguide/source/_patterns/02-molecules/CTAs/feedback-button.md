### Description
This pattern shows a Feedback icon link with a label.

### Status
* Stable as of 1.0.0

### Pattern Contains
* Icon Link

### Variables
~~~
feedbackButton: {
  label: 
    type: string / optional
  iconLink: {
    type: iconLink / required
  }
}
~~~
