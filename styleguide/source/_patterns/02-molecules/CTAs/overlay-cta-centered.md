### Description
This is a variant of the [Overlay CTA](./?p=molecules-overlay-cta) pattern showing an example with content centered.

### How to generate
* set the `align` variable to "center"
