### Description
This is a variant of the [Fancy Link](./?p=molecules-fancy-link) pattern showing an example with an external link.

### How to generate
* set the `external` variable to `true`
