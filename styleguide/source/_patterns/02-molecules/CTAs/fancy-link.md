### Description
This pattern shows a link as a card.

### Status
* Stable as of 1.0.0

### Variant options
* The pattern can also be rendered as an [external](./?p=molecules-fancy-link-external) link

### Variables
~~~
fancyLink: {
  external:
    type: boolean,
  text: 
    type: string / required,
  href: 
    type: string (url) / required,
  info:
    type: string / optional
}
~~~
