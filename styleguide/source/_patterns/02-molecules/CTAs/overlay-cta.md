### Description
This patterns is a Call to Action with a background image

### Status
* Stable as of 1.0.0

### Variant options
* This pattern can also be rendered [centered](./?p=molecules-overlay-cta-centered)

### Pattern Contains
* Button

### Variables
~~~
patternCta: {
  align:
    type: string ("center", "") / optional
  title:
    type: string / required
  description: 
    type: string / optional
  background:
    type: string (image path) / required
  button: {
    type: button / required
  }
}
~~~
