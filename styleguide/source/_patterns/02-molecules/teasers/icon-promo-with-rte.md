### Description
This is a variant of the [Icon Promo](./?p=molecules-icon-promo) pattern showing it with Rich Text.

### How to generate
* populate the `content` variable with 'richText' data
* set the `description` variable to ""

