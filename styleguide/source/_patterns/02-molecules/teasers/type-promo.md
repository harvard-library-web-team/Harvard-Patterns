### Description
An illustrated pattern that displays an image as a background image, subject, title, description. All linked to further information

### Status
* Alpha

### Variant options
* can be rendered in a [lighter blue](./?p=molecules-type-promo-light)
* can be rendered in a [grayscale](./?p=molecules-type-promo-grayscale)
* can be rendered as a [list item](./?p=molecules-type-promo-as-list)
* can be rendered as a [condensed list](./?p=molecules-type-promo-condensed) in various colors based on the subject
* can be rendered with an [external icon](./?p=molecules-type-promo-external-icon)

### Variables
~~~
typePromo: {
  theme:
    type: string ("", "light","clear") / optional
  level: (optional)
    type: number / optional / default = 2
  icon:
    type: string (icon path) / required
  color:
    type: string / optional
  subject:
    type: string / required
  title:
    type: string / required
  external:
    type: bool / optional
  description:
    type: string (html) / optional
  image: (optional) {  
    src:
      type: string (image path) / required
  },
  link: (required) {
    href:
      type: string (link path) / required,
    info:
      type: string (link info) / optional
  }
}
~~~
