### Description
This pattern shows an icon, title, description and button within a dotted border.

### Status
* Stable as of 1.0.0

### Pattern Contains
* Button

### Variant options
* This pattern can also be rendered as a [link](./?p=molecules-circle-promo-as-link)

### Variables
~~~
circlePromo: {
  level: 
    type: number / optional
  isLink: 
    type: boolean
  icon: 
    type: string (url) / optional
  title: 
    type: string / required
  description: 
    type: string / required
  theme: 
    type: string ("alert", "") / optional
  button: {
    type: button / required
  },
}
~~~
