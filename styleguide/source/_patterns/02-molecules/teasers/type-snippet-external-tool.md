### Description
This is a variant of the [Type Snippet](./?p=molecules-type-snippet) pattern showing an example of it an external link icon next to the title

### How to generate
* set `external` variable to `true`
