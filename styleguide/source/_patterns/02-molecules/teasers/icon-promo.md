### Description
This is a simple promo (title, description, cta) with an icon and a styled background.

### Status
* Stable as of 1.0.0

### Pattern Contains
* Comp Heading
* Button

### Variant options
* This pattern can be rendered in a [smaller](./?p=molecules-icon-promo-as-small) size.
* This pattern can be rendered in with [Rich Text](./?p=molecules-icon-promo-with-rte) content.

### Variables
~~~
iconPromo: {
  compHeading: {
    type: compHeading / required
  },
  theme: 
    type: string ("", "small") / optional,
  icon: 
    type: string (url) / required,
  description: 
    type: string / required,
  button: {
    type: button / required
  }
}
~~~
