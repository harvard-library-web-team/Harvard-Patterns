### Description
An illustrated pattern that displays an image as a background image with a caption overlaying the image

### Status
* Stable as of 1.0.0

### Variant options
* This pattern can also be shown with [no caption](./?p=molecules-overlay-image-no-caption)

### Variables
~~~
overlayImage: {
  caption:
    type: string / optional
  image: (required) {  
    src:
      type: string (image path) / required
  }
}
~~~
