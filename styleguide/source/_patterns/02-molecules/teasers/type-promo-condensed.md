### Description
This is a variant of the [Type Promo as List](./?p=molecules-type-promo-as-list) pattern showing an example of it in a condensed version (header and title on the same line, less padding). The color of the icon varies based on what the subject is.

### How to generate
* set the `theme` variable to `clear`
* set the `condensed` variable to `true`
