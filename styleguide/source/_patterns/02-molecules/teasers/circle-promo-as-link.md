### Description
This is a variant of the [Circle Promo](./?p=molecules-circle-promo) pattern showing it as a link.

### How to generate
* set the `isLink` variable to `true`
