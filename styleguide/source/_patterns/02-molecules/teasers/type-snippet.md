### Description
An illustrated pattern that displays an image as a background image, subject, title, description. All linked to further information

### Status
* stable

### Pattern Contains
* icons
* compHeading
* richText
* linkTag

### Variant options
* can be rendered as a [library snippet](./?p=molecules-type-snippet-library) with and without an image, with and without an external link icon next to title
* can be rendered as a [staff snippet](./?p=molecules-type-snippet-staff) with an image
* can be rendered as a [collection snippet](./?p=molecules-type-snippet-collection) or [website snippet](./?p=molecules-type-snippet-website) with an image
* can be rendered as an [external service or tool snippet](./?p=molecules-type-snippet-external-tool)

### Variables
~~~
typePromo: {
  level:
    type: number / optional / default = 2
  icon:
    type: string (image path) / required
  subject:
    type: string / required
  title:
    type: string / required
  external:
    type: bool / optional
  description:
    type: string / optional
  hours:
    type: string / optional
  link:
    type: linkTag / required
  externalLink:
    type: linkTag / optional
  image:
    type: image / optional
}
~~~
