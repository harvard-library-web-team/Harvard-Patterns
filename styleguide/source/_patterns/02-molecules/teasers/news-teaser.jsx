import React from "react";

import { string } from "prop-types";

const propTypes = {
  title: string.isRequired,
  type: string,
  story_source: string,
  subText: string,
  date: string.isRequired,
  time: string,
  location: string,
  href: string.isRequired,
  href_target: string,
  info: string
};

const NewsTeaser = (props) => {
  const { title, type, story_source, story_subject, subText, date, href, href_target, info } = props;

  return (
    <a className="hl__image-promo hl__image-promo--link" href={ href } title={ info } target={href_target}>
      <div className="hl__image-promo__details">
        <div className="hl__image-promo__date--with-source">{ date }</div>
        { story_source && (
          <div className="hl__image-promo__source hl__image-promo__source--with-date">{ story_source }</div>
        )}
        <h3 className="hl__image-promo__title">{ title }</h3>
        { subText && (
          <div className="hl__image-promo__description"><section className="hl__rich-text">{ subText }</section></div>
        )}
      </div>
    </a>
  );
};

NewsTeaser.propTypes = propTypes; // defined at the top of the file

export default NewsTeaser;
