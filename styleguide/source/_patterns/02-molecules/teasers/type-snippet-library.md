### Description
This is a variant of the [Type Snippet](./?p=molecules-type-snippet) pattern showing an example of it with an associated image and external link icon next to the title

### How to generate
* include `snippetImage` variable
* set `external` variable to `true`
