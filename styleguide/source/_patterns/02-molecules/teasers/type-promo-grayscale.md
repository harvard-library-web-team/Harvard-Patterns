### Description
This is a variant of the [Type Promo](./?p=molecules-type-promo) pattern showing an example with a grayscale background color.

### How to generate
* set the `theme` variable to `grayscale`
