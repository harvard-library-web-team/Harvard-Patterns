### Description
This is a variant of the [Stacked Promo](./?p=molecules-stacked-promo) pattern showing an example without an image.

### How to generate
* set the `image` variable to `null`
