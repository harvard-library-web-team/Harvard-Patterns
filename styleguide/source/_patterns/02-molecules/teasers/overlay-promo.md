### Description
An illustrated pattern that displays an image as a background image, title, subtitle. All linked to further information

### Status
* Stable as of 1.0.0

### Variant options
* This pattern can also be shown with [no image](./?p=molecules-overlay-promo-no-image), with an [eyebrow](./?p=molecules-overlay-promo-eyebrow), or with an [external icon](./?p=molecules-overlay-promo-external-icon)

### Variables
~~~
overlayPromo: {
  level: (optional)
    type: number / optional / default = 2
  title:
    type: string / required
  subTitle:
    type: string / optional
  eyebrow:
    type: string / optional
  external:
    type: bool / optional
  image: (optional) {  
    src:
      type: string (image path) / required
  },
  link: (required) {
    href:
      type: string (link path) / required,
    info:
      type: string (link info) / optional
  }
}
~~~
