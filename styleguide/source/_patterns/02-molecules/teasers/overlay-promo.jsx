import React from "react";

import { string, shape } from "prop-types";

const propTypes = {
  title: string.isRequired,
  image: shape({
    src: string
  }),
  link: shape({
    href: string.isRequired,
    info: string
  }).isRequired,
  collection_type: string.isRequired,
  subTitle: string,
  id: string
};

const OverlayPromo = (props) => {
  const { title, image, link, collection_type, subTitle, id } = props;

  const bgImage = image && image.src ? {backgroundImage: `url(' ${ image.src } ')`} : {};
  const imageClass = image && image.src ? "" : "hl__overlay-promo--no-image";
  const level = level ? level : 2;

  return (
    <a className={ `hl__overlay-promo ${ imageClass }` } href={ link.href } title={ link.info } style={ bgImage }>
      <div className="hl__overlay-promo__details-spacer" aria-hidden="true">
        <div className="hl__overlay-promo__eyebrow">{ collection_type }</div>
        { level == 2 && (
          <h2 className="hl__overlay-promo__title">
            { title }
            { collection_type === "website" && (
              <span className="hl__link-tag__external-icon hl__link-tag__external-icon--white">
                <svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" width="20" height="20" viewBox="0 0 20 20">
                  <g>
                    <g transform="translate(-906 -1005)">
                      <path d="M923.78 1022.78V1015H926v7.78c0 1.22-1 2.22-2.22 2.22h-15.56c-1.23 0-2.22-1-2.22-2.22v-15.56c0-1.22.99-2.22 2.22-2.22H916v2.22h-7.78v15.56zm-5.71-17.78H926v7.93h-2.27v-4.07L912.6 1020l-1.6-1.6 11.14-11.13h-4.07z"/>
                    </g>
                  </g>
                </svg>
              </span>
            )}
          </h2>
        )}
        { level == 3 && (
          <h3 className="hl__overlay-promo__title">
            { title }
            { collection_type === "website" && (
              <span className="hl__link-tag__external-icon hl__link-tag__external-icon--white">
                <svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" width="20" height="20" viewBox="0 0 20 20">
                  <g>
                    <g transform="translate(-906 -1005)">
                      <path d="M923.78 1022.78V1015H926v7.78c0 1.22-1 2.22-2.22 2.22h-15.56c-1.23 0-2.22-1-2.22-2.22v-15.56c0-1.22.99-2.22 2.22-2.22H916v2.22h-7.78v15.56zm-5.71-17.78H926v7.93h-2.27v-4.07L912.6 1020l-1.6-1.6 11.14-11.13h-4.07z"/>
                    </g>
                  </g>
                </svg>
              </span>
            )}
          </h3>
        )}
      </div>
      <div className="hl__overlay-promo__details">
        <div className="hl__overlay-promo__eyebrow">{ collection_type }</div>
        { level == 2 && (
          <h2 className="hl__overlay-promo__title">
            { title }
            { collection_type === "website" && (
              <span className="hl__link-tag__external-icon hl__link-tag__external-icon--white">
                <svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" width="20" height="20" viewBox="0 0 20 20">
                  <g>
                    <g transform="translate(-906 -1005)">
                      <path d="M923.78 1022.78V1015H926v7.78c0 1.22-1 2.22-2.22 2.22h-15.56c-1.23 0-2.22-1-2.22-2.22v-15.56c0-1.22.99-2.22 2.22-2.22H916v2.22h-7.78v15.56zm-5.71-17.78H926v7.93h-2.27v-4.07L912.6 1020l-1.6-1.6 11.14-11.13h-4.07z"/>
                    </g>
                  </g>
                </svg>
              </span>
            )}
          </h2>
        )}
        { level == 3 && (
          <h3 className="hl__overlay-promo__title">
            { title }
            { collection_type === "website" && (
              <span className="hl__link-tag__external-icon hl__link-tag__external-icon--white">
                <svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" width="20" height="20" viewBox="0 0 20 20">
                  <g>
                    <g transform="translate(-906 -1005)">
                      <path d="M923.78 1022.78V1015H926v7.78c0 1.22-1 2.22-2.22 2.22h-15.56c-1.23 0-2.22-1-2.22-2.22v-15.56c0-1.22.99-2.22 2.22-2.22H916v2.22h-7.78v15.56zm-5.71-17.78H926v7.93h-2.27v-4.07L912.6 1020l-1.6-1.6 11.14-11.13h-4.07z"/>
                    </g>
                  </g>
                </svg>
              </span>
            )}
          </h3>
        )}
      </div>
    </a>
  );
};

OverlayPromo.propTypes = propTypes; // defined at the top of the file

export default OverlayPromo;
