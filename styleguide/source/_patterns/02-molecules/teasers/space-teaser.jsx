import React              from "react";
import Image              from "../../01-atoms/media/image.jsx";
import IllustratedList    from "../../02-molecules/lists/illustrated-list.jsx";
import ReactSVG           from "react-inlinesvg";

import { string, object, array, shape, func } from "prop-types";

const propTypes = {
  id: string.isRequired,
  name: string.isRequired,
  floor: string.isRequired,
  room_type: string,
  library: shape({
    name: string.isRequired
  }).isRequired,
  image: object.isRequired,
  features: array,
  building_name: string,
  handleClick: func.isRequired,
  assetsUrl: string.isRequired
};

const SpaceTeaser = (props) => {

  const createFeatures = (features) => {
    return features.map(feature => {
      let temp = Object.assign({},feature);

      if(feature.name) {
        temp.text = feature.name;
      }
      if(feature.icon) {
        temp.icon = {
          src: feature.icon,
          height: "25",
          width: "25"
        };
      } else {
        temp.icon = {};
      }
      return temp;
    });
  };

  const handleClick = (e) => {
    e.preventDefault();
    props.handleClick(props.id);
  };

  const {
    assetsUrl,
    name,
    floor,
    room_type,
    image,
    features,
    library
  } = props;

  return (
    <a href="#" className="hl__space-teaser" onClick={ handleClick }>
      <h2 className="hl__space-teaser__title">{ name } &mdash; { floor }</h2>
      { room_type && (
        <div className="hl__space-teaser__type">{ room_type }</div>
      )}
      { library.name && library.address && library.address.building_name && (
        <div className="hl__space-teaser__building">
          <ReactSVG src={ `${ assetsUrl }/assets/images/svg-icons/map-marker.svg` } className="hl__space-teaser__location-icon"/>
          <span>{ library.name } in { library.address.building_name }</span>
        </div>
      )}
      { image && image.src && (
        <div className="hl__space-teaser__image"><Image { ...image } /></div>
      )}
      { features.length > 0 && (
        <div className="hl__space-teaser__features">
          <h3 className="hl__space-teaser__feature-title">Features</h3>
          <IllustratedList items={ createFeatures(features).slice(0, 4) } theme="small" />
        </div>
      )}
    </a>
  );
};

SpaceTeaser.propTypes = propTypes; // defined at the top of the file

export default SpaceTeaser;
