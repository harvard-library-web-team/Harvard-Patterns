### Description
This is a variant of the [Overlay Promo](./?p=molecules-overlay-promo) pattern showing an example with an external icon.

### How to generate
* set `external` variable to `true`
* alternatively, external icon will also appear if `eyebrow` variable is set to `website`
