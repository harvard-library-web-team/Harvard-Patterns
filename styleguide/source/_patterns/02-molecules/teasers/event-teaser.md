### Description
This Pattern shows event details with an optional image.

### Status
* Stable as of 1.0.0

### Pattern Contains
* Highlighted Text
* Button
* Image

### Variant options
* This pattern can also be shown as an [overlay](./?p=molecules-event-teaser-overlay)

### Variables
~~~
eventTeaser: {
  title: 
    type: string / required
  type:
    type: string / optional
  subText:
    type: string / optional
  level:
    type: number / optional (defaults to 2)
  date: {
    type: highlightText / required
  } 
  time: 
    type: string / required
  location: 
    type: string / required
  link: 
    type: linkTag / optional
  overlayPattern:
    type: boolean
  image: {
    type: image / optional
  }
}
~~~
