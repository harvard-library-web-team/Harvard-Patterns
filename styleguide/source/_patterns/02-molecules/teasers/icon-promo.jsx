import React      from "react";
import ReactSVG   from "react-inlinesvg";
import Button     from "../../01-atoms/buttons/button.jsx";

import { string, object, shape } from "prop-types";

const propTypes = {
  compHeading: shape({
    text: string.isRequired
  }).isRequired,
  theme: string,
  icon: string.isRequired,
  description: string.isRequired,
  button: object.isRequired
};

const iconPromo = (props) => {

  const { compHeading, theme, icon, description, button } = props;

  const themeClass = theme ? `hl__icon-promo--${theme}` : "";

  return (
    <section className={ `hl__icon-promo ${ themeClass }` }>
      <div className="hl__icon-promo__container">
        <div className="hl__icon-promo__content">
          <div className="hl__icon-promo__icon" aria-hidden="true">
            <ReactSVG src={ icon } />
          </div>
          <h2 className="hl__comp-heading">{ compHeading.text }</h2>
          <div className="hl__icon-promo__text-action">
            <p className="hl__icon-promo__description">{ description }</p>
            <div className="hl__icon-promo__action-container">
              <Button { ...button } />
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

iconPromo.propTypes = propTypes; // defined at the top of the file

export default iconPromo;
