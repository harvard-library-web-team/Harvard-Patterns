### Description
This is a variant of the [Overlay Promo](./?p=molecules-overlay-image) pattern showing an example without a caption.

### How to generate
* set the `caption` variable to `null`
