### Description
This is a pattern showing/hiding complicated text using an expandable section.

### Status
* Stable

### Pattern Contains
* Link Tag
* Rich Text

### JavaScript Used
* This pattern uses JavaScript for the accordions (js/helpers/accordions.js) and expand/collapse feature

### Variables
~~~
accordionItem: {
  title:
    type: string / required
  description:
    type: string / required
}
~~~
