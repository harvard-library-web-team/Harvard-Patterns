### Description
This is a variant of the [Overlay Promo](./?p=molecules-overlay-promo) pattern showing an example without an image.

### How to generate
* set the `image` variable to `null`
