### Description
An illustrated pattern that displays an image, category, title, description and a link to further information

### Status
* Stable as of 1.0.0

### Pattern Contains
* Image
* Rich Text
* Button

### Variant options
* The image can also be used as a background image with the text aligned [left](./?p=molecules-image-promo-overlay-left) or [bottom](./?p=molecules-image-promo-overlay-bottom)

### Variables
~~~
imagePromo: {
  isLink:
    type: boolean
  overlayPattern:
    type: boolean
  image: (optional) {  
    src:
      type: string (image path) / required
    alt:
      type: string / required - describes the image
    href:
      type: string (url) / optional,
    width:
      type: string (number) / optional
    height:
      type: string (number) / optional
  },
  subject: {
    text:
      type: string / required,
    href:
      type: string(url) / optional
    info:
      type: string / optional
  },
  date:
    type: string / optional,
  title: {
    text:
      type: string / required,
    href:
      type: string(url) / optional
    info:
      type: string / optional
  },
  subText: {
    type: string / optional
  },
  description: {
    type: richText / optional
  },
  link: {
    type: button / optional
  }
}
~~~
