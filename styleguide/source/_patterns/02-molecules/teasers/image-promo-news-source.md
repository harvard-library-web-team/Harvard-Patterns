### Description
This is a variant of the [Image Promo](./?p=molecules-image-promo) pattern showing the subject (source) before date.

### How to generate
* set the `newsSource` variable to true
* change `subject` key to `source`
