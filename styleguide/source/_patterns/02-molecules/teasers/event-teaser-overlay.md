### Description
This is a variant of the [Event Teaser](./?p=molecules-event-teaser) pattern showing an example as an overlay.

### How to generate
* set the `overlayPattern` variable to `true`
