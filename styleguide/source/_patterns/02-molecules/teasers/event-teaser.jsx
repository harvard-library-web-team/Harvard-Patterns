import React from "react";

import { string } from "prop-types";

const propTypes = {
  title: string.isRequired,
  type: string,
  subText: string,
  date: string.isRequired,
  time: string,
  location: string,
  href: string.isRequired,
  info: string
};

const EventTeaser = (props) => {
  const { title, type, subText, date, time, location, href, info } = props;

  return (
    <a className="hl__event-teaser hl__event-teaser--link" href={ href } title={ info }>
      <div className="hl__event-teaser__details">
        <h3 className="hl__event-teaser__title">{ title }</h3>
        { type && (
          <div className="hl__event-teaser__type">{ type }</div>
        )}
        { subText && (
          <div className="hl__event-teaser__sub-text">{ subText }</div>
        )}
        <div className="hl__event-teaser__date"><span className="hl__highlighted-text">{ date }</span></div>
        { time && (
          <div className="hl__event-teaser__time">{ time }</div>
        )}
        { location && (
          <div className="hl__event-teaser__location">Location: { location }</div>
        )}
      </div>
    </a>
  );
};

EventTeaser.propTypes = propTypes; // defined at the top of the file

export default EventTeaser;
