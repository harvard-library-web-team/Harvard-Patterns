### Description
A pattern that displays an image, title, description and a link to further information

### Status
* Alpha

### Pattern Contains
* Image
* Rich Text
* Button

### Variant options
* This pattern can be shown with [no image](./?p=molecules-stacked-promo-no-image) 

### Variables
~~~
stackedPromo: {
  image: (optional) {  
    type: image / optional
  },
  title: {
    type: string / optional
  },
  description: {
    type: richText / optional
  },
  link: {
    type: button / optional
  }
}
~~~