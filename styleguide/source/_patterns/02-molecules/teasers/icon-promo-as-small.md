### Description
This is a variant of the [Icon Promo](./?p=molecules-icon-promo) pattern showing a small sized variant.

### How to generate
* set the `theme` variable to "small"
