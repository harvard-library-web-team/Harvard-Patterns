import React, { Component }   from "react";
import $                      from "jquery";

import { string, arrayOf, shape, bool } from "prop-types";

export default class LibraryCard extends Component {

  static propTypes = {
    address: shape({
      address_line1: string,
      address_line2: string,
      locality: string,
      administrative_area: string,
      postal_code: string
    }),
    all_times: arrayOf(shape({
      label: string,
      rendered: string
    })),
    directions_link: string,
    hours_link: string.isRequired,
    href: string.isRequired,
    id: string.isRequired,
    image: shape({
      src: string.isRequired,
      alt: string,
      height: string,
      width: string
    }),
    name: string.isRequired,
    subTitle: string,
    timeLabel: string,
    today_hours: shape({
      status: string.isRequired,
      rendered: string.isRequired,
      currently_open: bool,
      department_hours: arrayOf(shape({
        label: string,
        rendered: string
      }))
    }),
    external: bool.isRequired
  };

  renderHours(hours) {
    return hours.map((h, i) => {
      return (
        <li key={ i }>
          <span className="hl__library-card__hours-label">{ h.label }</span>
          <span className="hl__library-card__hours-value">{ h.rendered }</span>
        </li>
      );
    });
  }

  currentStatus(today_hours) {
    return today_hours.currently_open ? "Open Now" : "Currently Closed";
  }

  buildAddress(a) {
    return a.address_line2 ? `${a.address_line1}<br />${a.address_line2}` : a.address_line1;
  }

  render() {

    const {
      address,
      directions_link,
      hours_link,
      href,
      id,
      image,
      name,
      subTitle,
      timeLabel,
      today_hours,
      external } = this.props;

    // const { expanded } = this.state;

    // department hours is the new API property (06-2018)
    const all_times = (today_hours && today_hours.department_hours) || this.props.all_times;

    // const accordionLabel = expanded ? "Hide Detailed Hours" : "Show Detailed Hours";

    return (
      <section className="hl__library-card">
        <div className="hl__library-card__container-top">
          { today_hours && today_hours.currently_open && (
            <div className="hl__library-card__status">
              { this.currentStatus(today_hours) }
            </div>
          )}
          <a className="hl__library-card__image" href={ href } aria-label={ `${name } details` } aria-hidden="true"><img
            src={ image.src }
            alt={ image.alt }
            height={ image.height }
            width={ image.width } /></a>
          <div className="hl__library-card__header">
            <h2 className="hl__library-card__title">
              <a href={ href } aria-label={ `${name } details` }>
                { name }
                { external == true && (
                  <span className="hl__link-tag__external-icon">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" width="20" height="20" viewBox="0 0 20 20">
                      <g>
                        <g transform="translate(-906 -1005)">
                          <path d="M923.78 1022.78V1015H926v7.78c0 1.22-1 2.22-2.22 2.22h-15.56c-1.23 0-2.22-1-2.22-2.22v-15.56c0-1.22.99-2.22 2.22-2.22H916v2.22h-7.78v15.56zm-5.71-17.78H926v7.93h-2.27v-4.07L912.6 1020l-1.6-1.6 11.14-11.13h-4.07z"/>
                        </g>
                      </g>
                    </svg>
                  </span>
                )}
              </a>
            </h2>
            { subTitle.length !== 0 && (
              <p className="hl__library-card__sub-title">{ subTitle }</p>
            )}
          </div>
          { address && (
          <div className="hl__library-card__address-container">
            <span className="hl__library-card__map-icon"><svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" width="12" height="17" viewBox="0 0 12 17"><g><g transform="translate(-988 -1676)"><path fill="silver" d="M994 1676c3.32 0 6 2.66 6 5.95 0 4.46-6 11.05-6 11.05s-6-6.59-6-11.05a5.97 5.97 0 0 1 6-5.95zm0 8a2 2 0 1 0 0-4 2 2 0 0 0 0 4z"/></g></g></svg></span>

            { address && directions_link && (
              <a
                href={ directions_link }
                aria-label={ `get directions to ${ name }` }
                className="hl__library-card__address-text hl__link-tag">
                { this.buildAddress(address) }<br />
                { address.locality}, { address.administrative_area } { address.postal_code }
              </a>
            )}
            { address && !directions_link && (
              <span className="hl__library-card__address-text">
                { this.buildAddress(address) }<br />
                { address.locality}, { address.administrative_area } { address.postal_code }
              </span>
            )}
          </div>
          )}
        </div>
        <div className="hl__library-card__container-bottom">
          { today_hours && today_hours.rendered && (
            <div className="hl__library-card__hours-container">
              <span className="hl__library-card__clock-icon"><svg height="31" viewBox="0 0 30 31" width="30" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink"><g><g transform="translate(-188 -510)"><g><path fill="#fad154" d="M202.99 512.5a12.5 12.5 0 1 1 0 25.01 12.5 12.5 0 0 1 0-25.01zM203 535a10 10 0 1 0 0-20 10 10 0 0 0 0 20z"></path></g><g><path fill="#fad154" d="M203.63 518.75h-1.88v7.5l6.56 3.94.94-1.54-5.63-3.34z"></path></g></g></g></svg></span>
              <span className="hl__library-card__hours-text">
                Today<br />{ today_hours.rendered }
              </span>
            </div>
          )}
          { all_times && all_times.length > 0 && (
            <div className="hl__library-card__more-hours">
              <div
                ref={el => this.accordionContent = el }
                className="hl__library-card__hours-content"
                id={ `hours-content-${ id }` }>
                  <p><span className="hl__library-card__today">{ timeLabel }</span></p>
                  <ul className="hl__library-card__hours-list">
                    { this.renderHours(all_times) }
                  </ul>
              </div>
              <div className="hl__library-card__hours-link">
                <a className="hl__link-tag hl__link-tag--with-arrow"
                  href={ hours_link }
                  aria-label={ `see all hours for ${ name }` }>
                  <span>See All Hours</span>
                </a>
              </div>
            </div>
          )}
        </div>
      </section>
    );
  }
}
