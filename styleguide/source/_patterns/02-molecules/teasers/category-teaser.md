### Description
This pattern is a link to another page and includes an icon based on the type of the page with some content from the page.

### Status
* Stable as of 1.0.0

### Variant options
* This pattern can also be rendered with an [external icon](./?p=molecules-category-teaser-external-icon) or [condensed card for hollis results](./?p=molecules-category-teaser-hollis-results)

### Pattern Contains
* Rich Text

### Variables
~~~
categoryTeaser: {
  level:
    type: number / optional
  icon:
    type: string (path to icon) / optional,
  eyebrow:
    type: string / optional,
  title:
    type: string / required,
  external:
    type: bool / optional
  description: {
    type: richText / optional
  }
  hollisResults:
    type: bool / optional
}
~~~
