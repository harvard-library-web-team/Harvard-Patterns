### Description
This Pattern shows an optional image, category, title, description wrapped in a link

### Status
* Stable as of 1.0.0

### Pattern Contains
* Image
* Rich Text
* Link Tag

### Variables
~~~
teaserLink: {
  image: (optional) {  
    src:
      type: string (image path) / required
    alt:
      type: string / required - describes the image
    href:
      type: string (url) / optional,
    width:
      type: string (number) / optional
    height:
      type: string (number) / optional
  },
  subject: {
    text:
      type: string / required,
    href:
      type: string(url) / optional
    info:
      type: string / optional
  },
  date:
    type: string / optional,
  title: {
    text:
      type: string / required,
    href:
      type: string(url) / optional
    info:
      type: string / optional
  },
  subText: {
    type: string / optional
  },
  description: {
    type: richText / optional
  },
  link: {
    type: linkTag / optional
  }
}
~~~
