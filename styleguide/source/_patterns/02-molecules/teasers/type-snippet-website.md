### Description
This is a variant of the [Type Snippet](./?p=molecules-type-snippet) pattern showing an example of it with an associated image

### How to generate
* include `snippetImage` variable
* set `externalLink` to `null`
* set `subject` to `website`
