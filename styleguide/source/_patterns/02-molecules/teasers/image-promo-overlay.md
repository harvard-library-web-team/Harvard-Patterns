### Description
This is a variant of the [Image Promo](./?p=molecules-image-promo) pattern showing an overlay over the image.

### How to generate
* set the `overlayPattern` variable to true
