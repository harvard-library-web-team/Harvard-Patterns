import React      from "react";
import Image      from "../../01-atoms/media/image.jsx";

import { string, shape, number, object } from "prop-types";

const propTypes = {
  title: shape({
    text: string.isRequired
  }).isRequired,
  level: number,
  collection_type: string.isRequired,
  subText: string,
  description: string,
  image: object,
  link: shape({
    href: string.isRequired,
    info: string
  }).isRequired,
  id: string
};

const createMarkup = ( rawText ) => {
  return {__html: rawText };
};

const teaserLink = (props) => {
  const { link, image, title, collection_type, description, subText, id } = props;
  const level = props.level ? props.level : 2;

  return (
    <a className="hl__teaser-link hl__teaser-link--link" href={ link.href } title={ link.info }>
      <div className="hl__teaser-link__container">
        { image && image.src && (
          <div className="hl__teaser-link__image">
            <Image { ...image } />
          </div>
        )}
        <div className="hl__teaser-link__details">
          <div className="hl__teaser-link__eyebrow">{ collection_type }</div>
          { level === 2 && (
            <h2 className="hl__teaser-link__title">
              { title.text }
              { collection_type === "website" && (
                <span className="hl__link-tag__external-icon">
                  <svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" width="20" height="20" viewBox="0 0 20 20">
                    <g>
                      <g transform="translate(-906 -1005)">
                        <path d="M923.78 1022.78V1015H926v7.78c0 1.22-1 2.22-2.22 2.22h-15.56c-1.23 0-2.22-1-2.22-2.22v-15.56c0-1.22.99-2.22 2.22-2.22H916v2.22h-7.78v15.56zm-5.71-17.78H926v7.93h-2.27v-4.07L912.6 1020l-1.6-1.6 11.14-11.13h-4.07z"/>
                      </g>
                    </g>
                  </svg>
                </span>
              )}
            </h2>
          )}
          { level === 3 && (
            <h3 className="hl__teaser-link__title">
              { title.text }
              { collection_type === "website" && (
                <span className="hl__link-tag__external-icon">
                  <svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" width="20" height="20" viewBox="0 0 20 20">
                    <g>
                      <g transform="translate(-906 -1005)">
                        <path d="M923.78 1022.78V1015H926v7.78c0 1.22-1 2.22-2.22 2.22h-15.56c-1.23 0-2.22-1-2.22-2.22v-15.56c0-1.22.99-2.22 2.22-2.22H916v2.22h-7.78v15.56zm-5.71-17.78H926v7.93h-2.27v-4.07L912.6 1020l-1.6-1.6 11.14-11.13h-4.07z"/>
                      </g>
                    </g>
                  </svg>
                </span>
              )}
            </h3>
          )}
          { subText && (
            <div className="hl__teaser-link__sub-text">{ subText }</div>
          )}
          { description && (
            <div
              className="hl__teaser-link__description"
              dangerouslySetInnerHTML={ createMarkup( description ) } />
          )}
        </div>
      </div>
    </a>
  );
};

teaserLink.propTypes = propTypes; // defined at the top of the file

export default teaserLink;
