### Description
This is a variant of the [Story Meta](./?p=molecules-story-meta) pattern showing an example with a subject instead of an author.

### How to generate
* polulate the `subject` variable and leave `authorName` blank
