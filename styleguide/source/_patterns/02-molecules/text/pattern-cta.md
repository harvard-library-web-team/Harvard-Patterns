### Description
This patterns is a Call to Action that can show icon links and or buttons

### Status
* Stable as of 1.0.0

### Pattern Contains
* Button
* Icon Link

### Variant options
* This pattern can also be rendered with [icons](./?p=molecules-pattern-cta-with-icons)
* This pattern can also be rendered in a [smaller format](./?p=molecules-pattern-cta-as-small)

### Variables
~~~
patternCta: {
  title:
    type: string / required
  description: 
    type: string / optional
  links: [{
    href:
      type: string (url) / optional
    info: 
      type: string / optional
    text: 
      type: string / required
    icon:
      type: string (path to icon)
  }]
}
~~~
