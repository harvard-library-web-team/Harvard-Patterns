### Description
This pattern shows the author's name and a publish date.

### Status
* Stable as of 1.0.0

### Variant options
* This pattern can be rendered with a [subject](./?p=molecules-story-meta-with-subject)

### Usage Guidelines
* either `authorName` or `subject` have to been populated.

### Variables
~~~
storyMeta: {
  authorName: 
    type: string / optional
  subject:
    type: string / optional
  date: 
    type: string / required
}
~~~
