### Description
This patterns shows quoted text along with a citation.

### Status
* Stable as of 1.0.0

### Variant options
* This pattern can also be rendered [without an author](./?p=molecules-quote-without-author)


### Variables
~~~
quote: {
  align:
    type: string ("left","right","") / optional
  text: 
    type: string / required
  author: 
    type: string / optional
  title:     
    type: string / optional
}
~~~
