### Description
This pattern show a round image followed by a link.

### Status
* Stable as of 1.0.0

### Pattern Contains
* Image
* Link Tag

### Variables
~~~
userLink: {
  image: {
    type: image / required
  }
  link: {
    type: linkTag / required
  }
}
~~~
