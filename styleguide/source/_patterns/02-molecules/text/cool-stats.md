### Description
This pattern is used to display statistic atoms in a highly stylized manner.

### Status
* Stable as of 1.0.0

### Pattern Contains
* Comp Heading
* Rich Text
* Statistic

### Variables
~~~
"coolStats": {
  compHeading: {
    type: compHeading / optional
  },
  richText: {
    type: richText / optional
  },
  items:
    type: array of Statistic / required
}
~~~
