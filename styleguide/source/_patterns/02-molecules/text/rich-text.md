### Description
Displays the contents of a Rich Text field.

### Status
* Alpha

### Pattern Contains
* Comp Heading
* Acceptable patterns for Rich Text
  * Paragraph
  * Headings 2-6
  * List
  * Table
  * Image
* Any pattern can be rendered in this pattern by setting the 'path' variable to the location of the pattern and setting the 'data' variable to container of the data object of that pattern.  
  * {% include element.path with element.data %}

### Variant options
* This pattern can be rendered as [small](./?p=molecules-rich-text-as-small)

### Variables:
~~~
richText: {
  compHeading: {
    type: compHeading / optional
  }
  seeAllLink: {
    type: linkTag / optional
  }
  theme: {
    type: string ("small", "") / optional  
  }
  align: {
    type: string ("center", "left-center", "") / optional
  }
  elements: [{
    path:
      type: string / required,
    data: {
      type: object of pattern path above / required
    }
  }]
}
~~~
