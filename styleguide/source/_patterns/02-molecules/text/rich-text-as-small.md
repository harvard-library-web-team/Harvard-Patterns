### Description
This is a variant of the [Rich Text](./?p=molecules-rich-text) pattern showing the small theme version.

### How to generate
* set the `theme` variable to `small`
