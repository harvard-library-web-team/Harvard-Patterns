import React      from "react";

import Button     from "../../01-atoms/buttons/button.jsx";

import { string, shape } from "prop-types";

const propTypes = {
  title: string.isRequired,
  size: string.isRequired,
  description: string,
  description_line2: string,
  link: shape({
    href: string.isRequired,
    info: string.isRequired,
    text: string.isRequired
  }).isRequired
};

// PatternCta is a stateless functional component.
const PatternCta = (props) => {

  const { title, size, description, description_line2, link } = props;

  const patternCtaSize = size ? `hl__pattern-cta--${ size }` : "";

  const button = {
    "href": link.href,
    "info": link.info,
    "text": link.text,
    "type": "button",
    "size": "small"
  };

  return (
    <section className={ `hl__pattern-cta ${ patternCtaSize }` }>
      <div className="hl__pattern-cta__container">

        <div className="hl__pattern-cta__text">
          <div className="hl__pattern-cta__title">{ title }</div>
          { description && (
            <div className="hl__pattern-cta__description">{ description }</div>
          )}
          { description_line2 && (
            <div className="hl__pattern-cta__description-line2">{ description_line2 }</div>
          )}
        </div>
        <div className="hl__pattern-cta__links">
          <Button { ...button } />
        </div>
      </div>
    </section>
  );
};

PatternCta.propTypes = propTypes; // defined at the top of the file

export default PatternCta;
