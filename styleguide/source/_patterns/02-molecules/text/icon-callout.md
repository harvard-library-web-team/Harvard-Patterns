### Description
This pattern shows an important message preceded by an optional icon.

### Status
* Stable as of 1.0.0

### Variables
~~~
iconCallout: {
  light: 
    type: boolean
  icon: 
    type: string (path) / optional
  message: 
    type: string / required
}
~~~
