### Description
This pattern shows a page title with a background image.

### Status
* Stable as of 1.0.0

### Pattern Contains
* Page Title

### Variables
~~~
illustratedTitle: {
  icon: 
    type: string (url) / required
  pageTitle: {
    type: pageTitle / required
  }
}
~~~
