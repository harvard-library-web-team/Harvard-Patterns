### Description
This pattern shows the file's name with a link and its metadata.

### Status
* Stable as of 1.0.0

### Pattern Contains
* [Link Tag](./?p=atoms-link-tag)

### Variables
~~~
fileDownload: {
  linkTag:
    type: Link Tag / required
  meta: 
    type: string / optional
  info
    type: string / optional
}
~~~
