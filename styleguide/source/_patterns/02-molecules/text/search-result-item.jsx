import React      from "react";
import { string, shape } from "prop-types";

const propTypes = {
  title: string.isRequired,
  href: string.isRequired,
  type: string,
  publisher: string,
  author: string,
  isPartOf: string,
  link: shape({
    href: string.isRequired,
    text: string.isRequired
  }).isRequired,
  additionaLink: shape({
    href: string.isRequired,
    text: string.isRequired
  })
};

// SearchResultItem is a stateless functional component.
const SearchResultItem = (props) => {

  const renderTitle = (title, link) => {
    if(link) {
      return (
        <a href={ link }>{ title }</a>
      );
    } else {
      return title;
    }
  };

  const { title, href, type, publisher, author, isPartOf, link, additionaLink } = props;

  return (
    <section className="hl__search-result-item">

      <div className="hl__search-result-item__text">
        <div className="hl__search-result-item__type">{ type }</div>
        <h4 className="hl__search-result-item__title">{ renderTitle(title, href)}</h4>
        { author && (
          <div className="hl__search-result-item__author">{ author }</div>
        )}
        { publisher && (
          <div className="hl__search-result-item__publisher">{ publisher }</div>
        )}
        { isPartOf && (
          <div className="hl__search-result-item__is-part-of">{ isPartOf }</div>
        )}
        {/* link && link.href && (
          <div className="hl__search-result-item__link">
            <a href={ link.href } className="hl__link-tag">{ link.text }</a>
          </div>
        )*/}
        { additionaLink && (
          <div className="hl__search-result-item__link">
            <a href={ additionaLink.href } className="hl__link-tag">{ additionaLink.text }</a>
          </div>
        )}
      </div>
    </section>
  );
};

SearchResultItem.propTypes = propTypes; // defined at the top of the file

export default SearchResultItem;
