### Description
This pattern is used to show contact info (email addresses or phone numbers) for a department 

### Status
* Stable as of 1.0.0

### Pattern Contains
* Link Tag

### Variables
~~~
contactInfo: {
  compHeading: {
    type: compHeading / optional
  }
  department:
    type: string / optional
  helpButton: {
    type: button / optional
  }
  address: 
    type: string / optional,

  directions: {
    type: linkTag / optional
  },

  website: {
    type: linkTag / optional
    internal: 
      type: boolean
  },
  emails: [{
    label: 
      type: string / optional
    link: {
      type: linkTag / optional
    }
  }]
  phones: [{
    label: 
      type: string / optional
    href: {
      type: string / optional
    text: {
      type: string / optional
    }
  }],
  socialLinks: [{
    type: arrayOf linkTag / optional
    icon:
      type: string (path to svg) / optional
  }]
}
~~~
