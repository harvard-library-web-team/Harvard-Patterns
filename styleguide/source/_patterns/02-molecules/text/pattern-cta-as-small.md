### Description
This is a variant of the [Pattern CTA](./?p=molecules-pattern-cta) pattern showing an example with a smaller format.

### How to generate
* populate the `size` variable to `small`
