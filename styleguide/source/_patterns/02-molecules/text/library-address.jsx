import React        from "react";
import ReactSVG     from "react-inlinesvg";
import currentDate  from "../../../js/helpers/currentDate.js";

import { string, shape, number } from "prop-types";

const propTypes = {
  assetsUrl: string.isRequired,
  name: string.isRequired,
  href: string.isRequired,
  coordinates: shape({
    latitude: number,
    longitude: number
  }),
  address: shape({
    address_line1: string.isRequired,
    address_line2: string,
    administrative_area: string.isRequired,
    locality: string.isRequired,
    organization: string,
    postal_code: string.isRequired
  }).isRequired,
  times: shape({
    rendered: string.isRequired
  }).isRequired
};

const LibraryAddress = (props) => {
  
  const { 
    assetsUrl,
    name,
    href,
    coordinates,
    times,
    address
  } = props;

  return (
    <div className="hl__library-address">
      <h2 className="hl__library-address__title">{ name }</h2>
      <div className="hl__library-address__hours">
        <mark className="hl__library-address__current-date">
          { currentDate() }
        </mark>
        { times.rendered }
      </div>
      <div className="hl__library-address__address">
        <div>{ address.organization ? `${ address.organization }, ` : "" }{ address.address_line1 }</div>
        { address.address_line2 && (
          <div>{ address.address_line2 }</div>
        )}
        <div>{ address.locality }, { address.administrative_area } { address.postal_code }</div>  
      </div>
      <div className="hl__library-address__links">
        { coordinates && coordinates.latitude && coordinates.longitude && (
          <div className="hl__library-address__link">
            <ReactSVG src={ `${ assetsUrl }/assets/images/svg-icons/map-marker.svg` } className="hl__library-address__icon"/>
            <a 
              className="hl__library-address__directions" 
              href={ `https://www.google.com/maps/dir/?api=1&destination=${ coordinates.latitude },${ coordinates.longitude }` }>Directions</a>
          </div>
        )}
        <div className="hl__library-address__link">
          <ReactSVG src={ `${ assetsUrl }/assets/images/svg-icons/web-link.svg` } className="hl__library-address__icon"/>
          <a className="hl__library-address__detail-page" href={ href }>More about { name }</a>
        </div>
      </div>
    </div>
  );
};

LibraryAddress.propTypes = propTypes; // defined at the top of the file

export default LibraryAddress;
