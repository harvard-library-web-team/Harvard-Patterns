import $ from "jquery";
import slick from "slick-carousel";

//PATCH for BUG:
//https://github.com/kenwheeler/slick/issues/2055
$(window).resize(function() {
  clearTimeout(window.galleryUpdate);
  let galleryUpdate = setTimeout(function() {
    $(".js-image-gallery").each(function(index, el) {
      $(el).find('.js-image-gallery__main-slider').slick('slickGoTo', 0);
    });
  }, 100);
});

export default function (window = window, document = document) {

  $(".js-image-gallery").each(function(index, el) {
    const $gallery = $(el).find('.js-image-gallery__main-slider');
    const $navigation = $(el).find('.js-image-gallery__nav-slider');

    const slickGallerySettings = {
      accessibility: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: true,
      fade: true,
      asNavFor: $navigation,
      responsive: [
        {
          breakpoint: 781,
          settings: {
            fade: false
          }
        }
      ]
    };

    const slickNavigationSettings = {
      accessibility: true,
      slidesToShow: 3,
      arrows: true,
      slidesToScroll: 1,
      focusOnSelect: true,
      asNavFor: $gallery
    };

    $gallery.slick(slickGallerySettings);
    $navigation.slick(slickNavigationSettings);
  });
};
