### Description
This pattern is used to give credit to an image.

### Status
* Stable as of 1.0.0

### Variant options
* The credit can also be render in a [light](./?p=molecules-image-credit-light) theme

### Variables
~~~
imageCredit: {
  theme: 
    type: string ("light", "") / optional
  href: 
    type: string (url) / required,
  text: 
    type: string / required,
  icon: 
    type: string (icon path) / required,
  library: {
    href: 
      type: string (url) / required,
    text: 
      type: string / required,
  } / optional
}
~~~
