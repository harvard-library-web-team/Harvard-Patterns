### Description
This is a variant of the [Image Credit](./?p=molecules-image-credit) pattern showing an example with a light theme.

### How to generate
* set the `theme` variable to `light`
