### Description
This pattern show a featured video or image.

### Status
* Stable as of 1.0.0

### Pattern Contains
* Comp Heading
* Rich Text
* YouTube Video
* Image
* Link Tag

### Variant options
* Can also show an [image](./?p=molecules-featured-media-with-image)

### Variables
~~~
featuredMedia: {
  compHeading: {
    type: compHeading / optional
  },
  description: {
    type: richText / optional
  },
  video: {
    type: youtubeVideo / optional with image
  },
  image:{
    type: image / optional with video
  },
  caption: 
    type: string / optional,
  transcript: {
    type: linkTag / optional
  }
}
~~~
