### Description
This is a variant of the [Featured Media](./?p=molecules-featured-media) pattern showing an example with an image instead of a video.

### How to generate
* populate the `image` variable 
* set the `video` variable to null
* set the `transcript` variable to null

