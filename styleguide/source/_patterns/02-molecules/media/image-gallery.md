### Description
This pattern shows a set of figures in an image carousel configuration, and
a set of images in a separate image carousel. The image carousel serves as navigation for the primary figure carousel. 
For large screen sizes the width of the carousel is capped to prevent having to scroll in order to interact with the navigation.
For medium screen sizes the width of the carousel will fill the container it
is placed in.
For small screen sizes the navigation carousel disappears, leaving only the 
primary figure carousel. On small screens the primary figure carousel can be controlled by swiping or with navigation buttons.

### Status
* Stable as of 1.0.0

### Pattern Contains
* Figure
* Image

### Variables
~~~
"imageGallery": {
  "items": 
    type: array of figure / required
}
~~~
