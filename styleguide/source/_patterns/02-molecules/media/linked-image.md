### Description
This is a sample of an image wrapped in an optional link.

### Status
* Stable as of 1.0.0

### Pattern Contains
* image


### Variables
~~~
linkedImage: {
  href: 
    type: string / optional,
  info: {
    type: string / optional
  },
  image: {
    type: image / required
  }
}
~~~
