import React, {Component}   from "react";
import Slider               from "react-slick";

import { string, object, arrayOf, shape, func } from "prop-types";

export default class ImageCarousel extends Component {

  static propTypes = {
    images: arrayOf(shape({
      src: string.isRequired
    })).isRequired
  };

  renderImages = (images) => {
    return images.map((image, i) => {
      const style= {
        backgroundImage: `url("${ image.src }")`
      };
      return (
        <div key={ image.src } className="hl__image-carousel__image" style={ style } />
      );
    });
  }

  render() {
    const { images } = this.props;

    return (
      <section className="hl__image-carousel">
        { images.length > 1 && (
          <Slider className="hl__image-carousel__slider">
            { this.renderImages(images) }
          </Slider>
        )}
        { images.length == 1 && (
          this.renderImages(images)
        )}
      </section>
    );
  }
}
