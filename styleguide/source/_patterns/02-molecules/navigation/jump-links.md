### Description
This pattern shows a list of links used to navigate to specific areas of the page.  Includes an optional title and page type.

### Status
* Stable as of 1.0.0

### Pattern Contains
* Eyebrow
* Linked List

### Variant options
* [Red (primary)](./?p=molecules-jump-links-red-color)
* [Gray (secondary)](./?p=molecules-jump-links-gray-color)
* [White](./?p=molecules-jump-links-white-color)

### JavaScript Used
* This pattern uses JavaScript for the sticky and scroll features

### Variables
~~~
jumpLinks: {
  theme:
    type: string ("", "primary", "secondary", "white") / optional,
  eyebrow: {
    type: eyebrow / optional
  },
  title:
    type: string / optional,
  links: [{
    type: arrayOf linkTag / required
  }]
}
~~~
