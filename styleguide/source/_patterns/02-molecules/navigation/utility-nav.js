import $ from "jquery";

export default function (window = window, document = document) {

  // utility nav buttons to open/close dropdown windows
  $(".js-utility-nav").each(function(){
    const $el = $(this);
    const $utilityNav = $el.find(".js-utility-nav-item");
    const $utilityNavAll = $el.find(".hl__utility-nav__item"); //finds all buttons AND links
    const $utilityButtons = $el.find(".js-utility-nav-button");
    const keyCodes = {
      tab: 9,
      up: 38,
      down: 40,
      left: 37,
      right: 39,
      enter: 13,
      space: 32,
      escape: 27
    };

    let utilityIndex = 1;
    let dropdownIndex = 2;
    let currentView = updateView();
    let debounce = null;
    let focusing = null;
    let documentWidth = $(document).width();

    $(".js-utility-nav-back-button").attr({tabIndex:"-1"});

    $(window).resize(() => {
      if(documentWidth === $(document).width()) {
        return;
      } else {
        documentWidth = $(document).width();
      }

      clearTimeout(debounce);
      debounce = setTimeout(() => {
        closeAllDropdowns();
        currentView = updateView();
        // if(currentView === "narrow") {
        //   $utilityButtons.attr({tabIndex:"-1"});
        // } else {
        //   $utilityButtons.removeAttr("tabIndex");
        // }
      },300);
    });

    // close all dropdowns when click outside of utility nav area
    $(document).on("click", function(e) {
      if ($el.has(e.target).length === 0) {
        closeAllDropdowns();
      }
    });

    $utilityNav.each(function(index){
      const $utilityNavItem = $(this);
      const $dropdownMenu = $utilityNavItem.find(".js-utility-nav-dropdown");
      const $dropdownLinks = $utilityNavItem.find("input, a, button");
      const $utilityButton = $utilityNavItem.find(".js-utility-nav-button");
      const $backButton = $utilityNavItem.find(".js-utility-nav-back-button");

      // $dropdownLinks.attr({tabIndex:"-1"});

      $utilityButton.on("click",function(e){
        e.preventDefault();

        if(currentView === "narrow") {
          if($utilityNavItem.hasClass("is-open")){
            closeThisDropdown();
          } else {
            openThisDropdown();
          }
        } else {
          if ($utilityNavItem.hasClass("is-open")){
            closeAllDropdowns();
          } else {
            closeAllDropdowns();
            openThisDropdown();
          }
        }
      });

      $utilityButton.on("keydown", function(e) {
        switch (e.keyCode) {
          case keyCodes.tab:
            if(currentView === "narrow"){
              e.preventDefault();
              if(utilityIndex === $utilityNavAll.length - 1){
                utilityIndex = 1;
                closeHamburgerMenu($(".hl__home-heading__search-input input"));
              } else {
                utilityIndex++;
                focusOnThis($utilityNavAll[utilityIndex],10);
              }
            }
            break;
          case keyCodes.left:
          case keyCodes.escape:
            if(currentView === "narrow"){
              e.preventDefault();
              closeHamburgerMenu($(".js-hamburger-button"));
            }
            break;
          case keyCodes.down:
            e.preventDefault();
            openThisDropdown();
            break;
          default:
        }
      });

      // navigating through a dropdown tray (not searchbox)
      $dropdownMenu.on("keydown", (e) => {
        switch (e.keyCode) {
          case keyCodes.tab:
            if($(this).hasClass("js-utility-nav-item--ask")){
              // hack for dropdown to close after chat button is tabbed because it isn't being found/recognized as a $dropdownLink
              if(dropdownIndex === $dropdownLinks.length){
                closeThisDropdown();
                utilityIndex++;
              } else {
                dropdownIndex++;
              }
            } else {
              if(dropdownIndex === $dropdownLinks.length - 1){
                closeThisDropdown();
                utilityIndex++;
              } else {
                dropdownIndex++;
              }
            }
            break;
          case keyCodes.left:
            e.preventDefault();
            if(currentView === "narrow"){
              closeThisDropdown();
              focusOnThis($utilityButton, 10);
            }
            break;
          case keyCodes.escape:
            e.preventDefault();
            closeThisDropdown();
            focusOnThis($utilityButton, 10);
            break;
          default:
        }
      });

      $backButton.on("click", function(e) {
        e.preventDefault();
        closeThisDropdown();
        focusOnThis($utilityButton);
      });

      // if click on escape from utility nav LINK
      $(".js-utility-nav-item--hollis").on("keydown", function(e) {
        switch (e.keyCode) {
          case keyCodes.escape:
            if(currentView === "narrow"){
              e.preventDefault();
              closeHamburgerMenu($(".js-hamburger-button"));
            }
            break;
          default:
        }
      });

      function focusOnThis($el, time = 600) {
        // wait for css animation to finish (.5 secs)
        clearTimeout(focusing);
        focusing = setTimeout(() => {
          $el.focus();
        }, time);
      }

      function openThisDropdown() {
        $utilityNavItem.addClass("is-open");
        $utilityButton.attr("aria-expanded", "true");

        if(currentView === "wide"){
          $dropdownMenu.slideDown(function(){
            focusOnThis($dropdownLinks[2], 10);
          });
        } else {
          focusOnThis($dropdownLinks[2]);
        }
      }

      function closeThisDropdown() {
        $utilityNavItem.removeClass("is-open");
        $utilityButton.attr("aria-expanded", "false");
        dropdownIndex = 2;

        if(currentView === "wide") {
          $dropdownMenu.slideUp();
        }
      }

      function closeHamburgerMenu($el) {
        $(".js-hamburger-button").removeClass("is-open");
        $(".js-hamburger-button").attr("aria-expanded", "false");
        utilityIndex = 1;
        focusOnThis($el, 10);
      }
    });

    function closeAllDropdowns() {
      if(currentView === "wide") {
        $el.find(".is-open .js-utility-nav-dropdown").stop(true, true).slideUp("fast", function(){
          $(this).removeAttr("style");
        });
      }

      $utilityNav.removeClass("is-open");
      $utilityButtons.attr("aria-expanded", "false");
      dropdownIndex = 2;
      utilityIndex = 1;
    }

    function updateView() {
      try {
        return window.getComputedStyle($el[0], ':before').getPropertyValue('content').replace(/\"/g, '');
      } catch(err) {
        return "narrow";
      }
    }
  });

  // if homepage searchbox is in focus, close search dropdown
  $(".hl__home-heading__search-input input").on("focus",function(){
    const $searchItem = $(".js-utility-nav-item--search");
    const $searchButton = $(".js-utility-nav-button");

    if ($searchItem.hasClass("is-open")){
      $searchItem.removeClass("is-open");
      $searchButton.attr("aria-expanded", "false");

      $searchItem.find(".js-utility-nav-dropdown").stop(true,true).slideUp("fast",function(){
        $(this).removeAttr("style");
      });

      $(".js-hamburger-button").removeClass("is-open");
      $(".js-hamburger-button").attr("aria-expanded", "false");
    }
  });

  // search bar dropdown
  $(".hl__utility-nav__dropdown .js-search-dropdown").each(function(){
    const $el = $(this);
    const dropdownUtilityNav = $el.find(".hl__single-input-form__dropdown");
    const searchWebsite = $el.find(".hl__single-input-form__search-term")[0];
    const searchHollis = $el.find(".hl__single-input-form__search-term")[1];
    const searchHollisLink = $el.find("#search-hollis-link");
    const searchWebsiteLink = $el.find("#search-website-link");
    const keyCodesUtilityNavs = {
      up: 38,
      down: 40
    };
    const optionsUtilityNav = {
      input: $el.find("input"),
      website: $el.find(".hl__single-input-form__dropdown-item a")[0],
      hollis: $el.find(".hl__single-input-form__dropdown-item a")[1]
    };

    // on focus and entering search term
    $("input#utility-nav-search-input").on("focusin keyup keydown change", function(){
      var searchTerm = this.value;
      var searchTermFull = this.value;
      const searchLength = searchTerm.length;
      const searchMaxLength = 20;

      // truncates long searches with an ellipsis
      if (searchLength > searchMaxLength) {
        searchTerm = searchTerm.substring(0,searchMaxLength)+'...';
      }

      // open/close search box
      if (searchLength === 0) {
        closeSearchBox();
      } else if (searchLength > 0){
        openSearchBox();
        searchHollis.innerHTML = `"${searchTerm}"`;
        searchWebsite.innerHTML = `"${searchTerm}"`;
        searchHollisLink.attr("href", "https://hollis.harvard.edu/primo-explore/search?query=any,contains,"+`${searchTermFull}`+"&tab=everything&search_scope=everything&vid=HVD2&lang=en_US&offset=0");
        searchWebsiteLink.attr("href", "/search?keywords="+`${searchTermFull}`);
      }
    });

    // navigating up/down from input field
    $(optionsUtilityNav.input).on("keydown focusout", function(e){
      dropdown(e, optionsUtilityNav.hollis, optionsUtilityNav.website);
      checkFocus();
    });

    // navigating up/down from hollis dropdown item
    $(optionsUtilityNav.hollis).on("keydown focusout",function(e){
      dropdown(e, optionsUtilityNav.website, optionsUtilityNav.input);
      checkFocus();
    });

    // navigating up/down from website dropdown item
    $(optionsUtilityNav.website).on("keydown focusout",function(e){
      dropdown(e, optionsUtilityNav.input, optionsUtilityNav.hollis);
      checkFocus();
    });

    function checkFocus(){
      setTimeout(function(){
        if (!(($(optionsUtilityNav.hollis).is(":focus")) || ($(optionsUtilityNav.input).is(":focus")) || ($(optionsUtilityNav.website).is(":focus")))) {
          closeSearchBox();
        }
      }, 300);
    }

    function openSearchBox(){
      $(optionsUtilityNav.input).addClass("is-visible");
      dropdownUtilityNav.slideDown().addClass("is-visible");
    }

    function closeSearchBox(){
      $(optionsUtilityNav.input).removeClass("is-visible");
      dropdownUtilityNav.slideUp().removeClass("is-visible");
    };

    function dropdown(e, keyUp, keyDown){
      if (e.keyCode === 38){
        focusItem(e, keyUp);
      } else if (e.keyCode === 40){
        focusItem(e, keyDown);
      }
    }

    function focusItem(e, item) {
      e.preventDefault();
      $(item).focus().select();
    }

    // turn off autocomplete
    $("input").attr("autocomplete", "off");
    $("form").attr("autocomplete", "off");

  });

}
