import checkMobile from "../../../js/helpers/cssControlCode.js";
import $ from "jquery";

export default function (window = window, document = document) {
  $(".js-jump-links").each(function() {
    const $el = $(this);
    const $links = $el.find('.js-jump-links-link');
    const activeClass = "is-active";

    // wrapper is used to take up the space when the links become sticky
    $el.wrap('<div class="hl__jump-links__wrapper" />');
    // once the wrapper exist, record it.
    const $wrapper = $el.parent();
    const headerBuffer = 90;

    let upperLimit;
    let debounceTimer;
    let activeAnchorIndex = -1;
    let anchors = [];
    let numAnchors = 0;
    let isMobile = false;
    let linkScrolling = false;

    $links.first().addClass(activeClass);

    setVariables();

    // default assumption as to where the screen will load
    $el.attr('data-sticky','static');

    $el.addClass("is-ready");

    // update variables one more time to catch any post page load changes
    window.setTimeout(function(){
      setVariables();
      setPosition();
      activateLink();
    },1000);

    $links.on('click',function(e) {
      e.preventDefault();

      let $link = $(this);

      // is the menu closed on mobile
      if(!$el.hasClass('is-open') && isMobile) {     
        // just show the menu
        $el.addClass('is-open');
        return;
      }
       
      activeAnchorIndex = $(this).data('index');
      // find the location of the desired link and scroll the page
      let position = anchors[activeAnchorIndex].position;
      // close the menu
      $el.removeClass('is-open');
      // prevent the scroll event from updating active links
      linkScrolling = true;

      $("html,body").stop(true,true).animate({scrollTop:position}, '750', function(){
        linkScrolling = false;
        // Get the link hash target so we can bring focus to it
        let hash = anchors[activeAnchorIndex].hash;
        // bring focus to the item we just scrolled to
        $(hash).focus();
        // set the URL to the clicked hash
        history.replaceState(null, null, hash);
        // timing issue with window.scroll event firing.
        setTimeout(function(){
          // set this link as active.
          $el.find('.' + activeClass).removeClass(activeClass);
          $link.addClass(activeClass);
        },30);
      });
    });

    // if the content contains any accordions, 
    // readjust settings when there state changes.
    $(".js-accordion-link, .js-show-more-text").on('click',function() {
      if(typeof debounceTimer === "number") {
        window.clearTimeout(debounceTimer);
      }
      debounceTimer = window.setTimeout(function(){
        setVariables();
        setPosition();
        activateLink();
      },1000);
    });

    // make the links sticky
    $(window).resize(function() {
      if(typeof debounceTimer === "number") {
        window.clearTimeout(debounceTimer);
      }
      debounceTimer = window.setTimeout(function(){
        setVariables();
        setPosition();
        activateLink();
      },300);
    });

    $(window).scroll(function () {
      setPosition();

      if(!linkScrolling){
        activateLink();
      }
    });

    function setVariables() {
      const elHeight = $el.outerHeight(true) || 0;

      upperLimit = $wrapper.offset().top - headerBuffer;
      isMobile = checkMobile($el[0]);
     
      // locate the position of all of the anchor targets
      anchors = new Array;
      $links.each(function(i,e){
        const $thisLink = $(this),
          $link = $thisLink.is('a') ? $thisLink : $thisLink.find('a'),
          hash = $link[0].hash,
          position = $(hash).offset() ? $(hash).offset().top - elHeight - headerBuffer : upperLimit;

        anchors[i] = { hash, position };

        $thisLink.data('index',i);
      });

      // record the number of anchors for performance
      numAnchors = anchors.length;
    }

    function setPosition() {
      const windowTop = $(window).scrollTop();
      const attr = $el.attr('data-sticky');
      const makeStatic = attr === 'sticky' && windowTop <= upperLimit; 
      const makeSticky = attr !== 'sticky' && windowTop > upperLimit;

      // switch to one of these states or stay the same
      if(makeStatic) {
        $el.attr('data-sticky','static');
        $wrapper.height("auto");
      } 
      if (makeSticky) {
        $el.attr('data-sticky','sticky');
        $wrapper.height($el.height());
      }
    }

    function activateLink() {
      // do we have more than one anchor
      if(numAnchors < 2 || linkScrolling) {
        return;
      }

      // get the current scroll position and offset by half the view port
      const windowMiddle = $(window).scrollTop() + (window.innerHeight/2);
      const currentAnchor = activeAnchorIndex;

      // is there a prev target
      // and 
      // is the current scroll position above the current target
      if(currentAnchor > 0 && windowMiddle < anchors[activeAnchorIndex].position) { 
        // make the prev link active
        --activeAnchorIndex;
      }

      // is there a next target
      // and
      // is the current scroll position below the next target
      else if(currentAnchor < numAnchors-1 && windowMiddle > anchors[activeAnchorIndex+1].position) { 
        // make the next link active
        ++activeAnchorIndex;
      } 

      if (currentAnchor !== activeAnchorIndex) {
        // move the active flag
        $el.find('.' + activeClass).removeClass(activeClass);
        $links.eq(activeAnchorIndex).addClass(activeClass);
      }
    }
  });
}
