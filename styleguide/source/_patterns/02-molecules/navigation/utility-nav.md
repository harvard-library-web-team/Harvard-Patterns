### Description
This pattern shows a list of icon links with an optional message.

### Status
* Stable as of 1.0.0

### Pattern Contains
* Single Input Form

### Variables
~~~
utilityNav: {
  items: [{
    name:
      type: string / required,
    icon:
      type: string (icon path) / required,
    href:
      type: string (url) / required,
    text:
      type: string / optional,
    info:
      type: string / required
  },
  singleInputForm: {
    type: singleInputForm / required
  }
}
~~~
