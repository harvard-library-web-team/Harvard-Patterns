### Description
This pattern shows a set of navigation links and a logo
For large screen size it will print a left navigation links, a centered logo and a right navigation links.
For medium screen size it will show the logo centered at the top of the section followed by two columns of links, left and right navigation.
For small screen size it will display the logo centered at the top of the section and the links stacked starting with the left navigation.

### Status
* Stable as of 1.0.0

### Pattern Contains
* Link Tag

### Variables
~~~
footerNav: {
  items:
    type: array of linkTag / required
}
~~~
