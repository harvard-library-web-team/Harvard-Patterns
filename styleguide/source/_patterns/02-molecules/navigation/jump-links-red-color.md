### Description
This is a variant of the [Jump Links](./?p=molecules-jump-links) pattern showing an example in red with header removed.

### How to generate
* set the `eyebrow` and `title` variables to `null`
* set the `theme` variable to `primary`
