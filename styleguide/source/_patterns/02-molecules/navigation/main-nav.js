import $ from "jquery";

export default function (window = window, document = document) {

  // toggle the open class on the button
  $(".js-main-nav").each(function() {
    const $el = $(this);
    const $menus = $el.find(".js-main-nav-menu");
    const $topLinks = $el.find(".js-main-nav-top-link");
    const keyCodes = {
      tab: 9,
      up: 38,
      down: 40,
      left: 37,
      right: 39,
      enter: 13,
      space: 32,
      escape: 27
    };

    let currentView = updateView();
    let debounce = null;
    let focusing = null;
    let documentWidth = $(document).width();

    if(currentView === "narrow") {
      $topLinks.attr({tabIndex:"-1"});
    } else {
      $topLinks.removeAttr("tabIndex");
    }

    $(".js-main-nav-back-button").attr({tabIndex:"-1"});

    $(window).resize(() => {
      if(documentWidth === $(document).width()) {
        return;
      } else {
        documentWidth = $(document).width();
      }

      clearTimeout(debounce);
      debounce = setTimeout(() => {
        closeAllMenus();
        currentView = updateView();
        if(currentView === "narrow") {
          $topLinks.attr({tabIndex:"-1"});
        } else {
          $topLinks.removeAttr("tabIndex");
        }
      },300);
    });

    // close all menus when click outside of main nav area
    $(document).on("click", function(e) {
      if ($menus.has(e.target).length === 0) {
        closeAllMenus();
      }
    });

    $menus.each(function(index){
      const $menu = $(this);
      const $submenu = $menu.find(".js-main-nav-sub-menu");
      const $subLinks = $submenu.find("a");
      const $topLink = $menu.find(".js-main-nav-top-link");
      const $backButton = $menu.find(".js-main-nav-back-button");

      let subMenuIndex = 0;

      $subLinks.attr({tabIndex: "-1" });

      $topLink.on("click", function(e){
        e.preventDefault();

        if(currentView === "narrow") {
          if($menu.hasClass("is-open")) {
            closeThisMenu();
          } else {
            openThisMenu();
          }
        } else {
          if($menu.hasClass("is-open")) {
            closeAllMenus();
          } else {
            closeAllMenus();
            openThisMenu();
          }
        }
      });

      $topLink.on("keydown", function(e) {
        switch (e.keyCode) {
          case keyCodes.tab:
            // if(currentView === "narrow") {
            //   e.preventDefault();
            // }
            break;
          case keyCodes.escape:
            e.preventDefault();
            closeThisMenu();
            break;
          case keyCodes.enter:
          case keyCodes.space:
            e.preventDefault();
            openThisMenu();
            break;
          case keyCodes.down:
            e.preventDefault();
            if(currentView === "wide") {
              openThisMenu();
            } else {
              if(index === $menus.length - 1) {
                $menus.first().find(".js-main-nav-top-link").focus();
              } else {
                $($menus[index + 1]).find(".js-main-nav-top-link").focus();
              }
            }
            break;
          case keyCodes.up:
            e.preventDefault();
            if(currentView === "wide") {
              closeThisMenu();
            } else {
              if(index === 0) {
                $menus.last().find(".js-main-nav-top-link").focus();
              } else {
                $($menus[index - 1]).find(".js-main-nav-top-link").focus();
              }
            }
            break;
          case keyCodes.right:
            e.preventDefault();
            if(currentView === "narrow") {
              openThisMenu();
            }
            break;
          default:
        }
      });

      $submenu.on("keydown", (e) => {
        switch (e.keyCode) {
          case keyCodes.tab:
            e.preventDefault();
            closeThisMenu();
            focusOnThis($topLink, 10);
            break;
          case keyCodes.up:
            e.preventDefault();
            if(subMenuIndex === 0) {
              subMenuIndex = $subLinks.length - 1;
            } else {
              subMenuIndex--;
            }
            focusOnThis($subLinks[subMenuIndex], 10);
            break;
          case keyCodes.down:
            e.preventDefault();
            if(subMenuIndex === $subLinks.length - 1) {
              subMenuIndex = 0;
            } else {
              subMenuIndex++;
            }
            focusOnThis($subLinks[subMenuIndex], 10);
            break;
          case keyCodes.left:
          case keyCodes.right:
            e.preventDefault();
            if(currentView === "narrow") {
              closeThisMenu();
              focusOnThis($topLink, 10);
            }
            break;
          case keyCodes.escape:
            e.preventDefault();
            closeThisMenu();
            focusOnThis($topLink, 10);
            break;
          default:
        }
      });

      $backButton.on("click", function(e) {
        e.preventDefault();
        closeThisMenu();
        focusOnThis($topLink);
      });

      // Chrome hates me
      // setting focus scrolls the window
      // if the element isn't visible on the screen
      function focusOnThis($el, time = 600) {
        // wait for css animation to finish (.5 secs)
        clearTimeout(focusing);
        focusing = setTimeout(() => {
          $el.focus();
        }, time);
      }

      function openThisMenu() {
        $menu.addClass("is-open");
        $topLink.attr("aria-expanded", "true");
        if(currentView === "wide") {
          $submenu.slideDown(function(){
            focusOnThis($subLinks.first(), 10);
          });
        } else {
          focusOnThis($subLinks.first());
        }
      }

      function closeThisMenu() {
        $menu.removeClass("is-open");
        $topLink.attr("aria-expanded", "false");
        if(currentView === "wide") {
          $submenu.slideUp();
        }
      }
    });

    function closeAllMenus() {
      if(currentView === "wide") {
        $el.find(".is-open .js-main-nav-sub-menu").stop(true, true).slideUp("fast", function(){
          $(this).removeAttr("style");
        });
      }
      $menus.removeClass("is-open");
      $topLinks.attr("aria-expanded", "false");
    }

    function updateView() {
      try {
        return window.getComputedStyle($el[0], ':before').getPropertyValue('content').replace(/\"/g, '');
      } catch(err) {
        return "narrow";
      }
    }
  });
}
