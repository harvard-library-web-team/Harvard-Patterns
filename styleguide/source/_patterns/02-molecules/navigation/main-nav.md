### Description
This pattern provides site wide navigation and shows the Harvard Library logo on wide screens.

### Status
* Stable as of 1.0.0

### Pattern Contains
* Link Tag
* Linked Image
* Button
* Quick Links

### JavaScript Used
* This pattern uses JavaScript to control the menus

### Variables
~~~
mainNav: {
  backButton: {
    type: button / required
  },
  menus: {
    topLink:
      type: string / required
    submenu: [{
      link: {
        type: linkTag / required
      }
    }]
  },
  utilityNav: {
    STUFF
  }
}
~~~
