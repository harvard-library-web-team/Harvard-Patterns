import React   from "react";

import { string, func } from "prop-types";

const propTypes = {
  label: string.isRequired,
  onChange: func.isRequired,
  placeholder: string,
  theme: string,
  value: string
};

const SearchInputRow = (props) => {

  const { value, theme, placeholder, label, onChange } = props;

  const themeClass = theme ? `hl__search-input-row--${theme}` : ""; 

  return (
    <section className={ `hl__search-input-row ${ themeClass }` }>
      <div className="hl__search-input-row__container">
        <label className="hl__search-input-row__label">
          <span className="hl__search-input-row__text">{ label }</span>
          <input 
            className="hl__input hl__search-input-row__input" 
            name="keyword" 
            type="text" 
            placeholder={ placeholder }
            value={ value }
            onChange={ onChange } />
        </label>
      </div>
    </section>
  );
};

SearchInputRow.propTypes = propTypes; // defined at the top of the file

export default SearchInputRow;
