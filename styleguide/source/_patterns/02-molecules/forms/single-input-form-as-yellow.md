### Description
This is a variant of the [Single input form](./?p=molecules-single-input-form) pattern to display it with the yellow theme.

### How to generate
* set the `theme` variable to `yellow`
