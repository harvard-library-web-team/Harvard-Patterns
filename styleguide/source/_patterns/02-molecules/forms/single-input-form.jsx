import React, { Component }   from "react";
import Button                 from "../../01-atoms/buttons/button.jsx";

import { string, object, func } from "prop-types";

const propTypes = {
  action: string.isRequired,
  method: string,
  theme: string.isRequired,
  helperText: string,
  button: object.isRequired,
  onSubmit: func.isRequired,
  onChange: func.isRequired,
  searchTerm: string
};

const SingleInputForm = (props) => {

  const handleSubmit = (event) => {
    event.preventDefault();
    props.onSubmit();
  };

  const handleChange = (event) => {
    props.onChange(event.target.value);
  };

  const { searchTerm, action, method, theme, helperText, button } = props;

  const themeClass = theme ? `hl__single-input-form--${theme}` : "";

  return (
    <section className={"hl__single-input-form " + themeClass}>
      <form
        className="hl__single-input-form__form"
        action={ action }
        method={ method }
        onSubmit={ handleSubmit }>

        <label className="hl__label hl__label--hidden" htmlFor="search-page-keyword">Enter a keyword to search</label>
        { helperText && (
          <div className="hl__single-input-form__helper-text">{ helperText }</div>
        )}
        <div className="hl__single-input-form__input-group">
        <input
          className="hl__input hl__input--inline"
          name="keyword"
          id="search-page-keyword"
          type="text"
          placeholder="Keyword Search"
          value={ searchTerm }
          onChange={ handleChange } />
        <Button { ...button } />
        </div>
      </form>
    </section>
  );
};

SingleInputForm.propTypes = propTypes; // defined at the top of the file

export default SingleInputForm;
