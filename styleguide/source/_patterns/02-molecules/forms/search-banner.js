import $ from "jquery";

export default function (window = window, document = document) {

  $(".js-search-banner").each(function(){
    const $el = $(this);
    const $form = $el.find(".js-search-banner-form");

    // on focus 
    $form.on("focusin", function() {
      $(this).addClass("is-active");
    });
    // on blur 
    $form.on("focusout", function() {
      $(this).removeClass("is-active");
    });
  });
}
