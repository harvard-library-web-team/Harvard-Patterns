### Description
A pattern to display a single input form.

### Status
* Stable as of 1.0.0

### Pattern Contains
* Input
* Button

### Variant options
This pattern, in addition to blue, can also be rendered in [red](/?p=molecules-single-input-form-as-red), [crimson](/?p=molecules-single-input-form-as-crimson), or [yellow](/?p=molecules-single-input-form-as-yellow), or as a [dropdown](/?p=molecules-single-input-form-as-dropdown).

### Usage Guidelines
* The input `theme` variable need to be set as `inline`
* The button `theme` variable need to be set as `inline` and `size` as `small`

### Variables
~~~
singleInputForm: {
  action:
    type: string / required
  method:
    type: string ("", "GET", "POST") / optional
  theme:
    type: string / optional / ("", "red", "yellow", "clear")
  label:
    type: String / required
  helperText:
    type: String / optional
  input:
    type: Input / required
  button:
    type: Button / required
  dropdown:
    type: boolean / optional
}
~~~
