import $                      from "jquery";
import React, { Component }   from "react";

import CheckboxFilter         from "../../01-atoms/forms/checkbox-filter.jsx";
import WeekPicker             from "../../02-molecules/forms/week-picker.jsx";
import Picky from 'react-picky';

import { array, string, bool, func } from "prop-types";

export default class FilterBarGroup extends Component {

  static propTypes = {
    accordionChange: func,
    assetsUrl: string,
    expanded: bool.isRequired,
    filterType: string.isRequired,
    hideAccordion: bool,
    items: array.isRequired,
    label: string,
    onChange: func.isRequired,
    param: string.isRequired,
    placeholder: string,
    selected: array // for select type
  };

  componentWillReceiveProps(nextProps) {
    if(this.props.expanded == nextProps.expanded) {
      return;
    }
    const $content = $(this.accordionContent);

    if(nextProps.expanded) {
      $content.slideDown();
    } else {
      $content.slideUp();
    }
  }

  handleAccordion = (e) => {
    e.preventDefault();
    this.props.accordionChange(this.props.param,!this.props.expanded);
  }

  handleSelectChange = (value) => {
    const { param, onChange } = this.props;
    onChange(param,value);
  }

  renderFilters() {
    const {
      assetsUrl= "../..",
      param,
      onChange,
      filterType,
      selected = [],
      items,
      placeholder,
      label } = this.props;

    switch (filterType) {
      case "checkbox":
        return items.map((item, index) => {
          return (
            <CheckboxFilter
              key={ param + item.value }
              onChange={ onChange }
              name= { param }
              { ...item } />
          );
        });
        break;
      case "select":
        return (
          <Picky
            value={selected}
            name={param}
            options={items}
            multiple={true}
            includeSelectAll={false}
            includeFilter={false}
            onChange={this.handleSelectChange}
            dropdownHeight={200}
            valueKey={'value'}
            labelKey={'label'}
            numberDisplayed={0}
            placeholder={placeholder}
            manySelectedPlaceholder={placeholder + " (%s)"}
          />
        );
        break;
      case "keyword":
        return (
          <input
            className="hl__input"
            aria-label={ label }
            name={ param }
            id={ param }
            type="search"
            placeholder={ placeholder }
            value={ items.length && items[0].value ? items[0].value : "" }
            onChange={ onChange } />
        );
        break;
      case "weekPicker":
        return items.map((item, index) => {
          return (
            <WeekPicker
              key={ `single_date_${ index }` }
              assetsUrl={ assetsUrl }
              onDateChange={ onChange }
              { ...item } />
          );
        });
        break;
      default:
        return null;
    }
  }

  render() {
    const { label, expanded, filterType, hideAccordion, accordionChange } = this.props;

    const themeClass = filterType === "inline-checkbox" ? `hl__filter-bar-group--inline` : "";

    return (
        <fieldset className={ `hl__filter-bar-group ${ themeClass }` }>
          { !hideAccordion && accordionChange && (
            <label><button className="hl__filter-bar-group__label" type="button" aria-expanded={ expanded } onClick={ this.handleAccordion }>{ label }</button></label>
          )}
          { hideAccordion && (
            <label><div className="hl__filter-bar-group__label">{ label }</div></label>
          )}
          <div
            className="hl__filter-bar-group__items"
            ref={el => this.accordionContent = el }>
            { this.renderFilters() }
          </div>
        </fieldset>
    );
  }
}
