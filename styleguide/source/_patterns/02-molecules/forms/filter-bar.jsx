import React            from "react";
import FilterBarGroup    from "./filter-bar-group.jsx";

import { shape, string, func, bool, arrayOf, array } from "prop-types";

const propTypes = {
  assetsUrl: string.isRequired,
  message: string,
  groups: arrayOf(shape({
    param: string.isRequired,
    expanded: bool.isRequired,
    items: array
  })).isRequired,
  onClearAll: func,
  title: string.isRequired,
  theme: string
};

const FilterBar = (props) => {

  const renderGroups = (groups) => {
    const { assetsUrl } = props;

    return groups.filter(group => group.items && group.items.length).map(group => {
      return (
        <div
          className="hl__filter-bar__group"
          key={ group.param }>
          <FilterBarGroup
            assetsUrl={ assetsUrl }
            { ...group } />
        </div>
      );
    });
  };

  const {
    groups,
    title,
    onClearAll,
    assetsUrl,
    message,
    theme } = props;

  const themeColor = theme ? `hl__filter-bar--${theme}` : "";

  return (
    <div className={ `hl__filter-bar ${themeColor}`}>
      <div className="hl__filter-bar__container">
      <header className="hl__filter-bar__header">
        <h2 className="hl__filter-bar__title">{ title }</h2>
      </header>
      <div className="hl__filter-bar__filters">
        { renderGroups(groups) }
        { message && (
          <div className="hl__filter-bar__message">
            { message }
          </div>
        )}
      </div>
      { onClearAll && (
        <div className="hl__filter-bar__clear">
          <button
            className="hl__filter-bar__clear-button"
            type="button"
            aria-label="Clear all filters"
            onClick={ onClearAll }>Clear All</button>
        </div>
      )}

      </div>
    </div>
  );
};

FilterBar.propTypes = propTypes; // defined at the top of the file

export default FilterBar;
