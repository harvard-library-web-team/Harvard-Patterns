import React   from "react";

import { shape, arrayOf, string, bool, func } from "prop-types";

const propTypes = {
  label: string,
  onChange: func.isRequired,
  activeFilter: string,
  items: arrayOf(shape({
    name: string.isRequired,
    value: string.isRequired,
    label: string.isRequired
  })).isRequired
};

const FilterByType = (props) => {

  const handleChange = (event) => {
    props.onChange(event.target.value);
  };

  const renderFilters = (items) => {
    return items.map((item) => {
      return (
        <label key={ item.value } className="hl__radio-input">
          <input
            name={ item.name }
            aria-label={ item.name }
            type="radio"
            value={ item.value }
            checked= { item.value === props.activeFilter }
            onChange= { handleChange } />
          <span className="hl__radio-input__label">{ item.label }</span>
        </label>
      );
    });
  };

  const { label, items } = props;

  return (
    <div className="hl__filter-by-type">
      <fieldset>
        <legend>{ label }</legend>
        <span className="hl__filter-by-type__label" aria-hidden>{ label }</span>
        { renderFilters(items) }
      </fieldset>
    </div>
  );
};

FilterByType.propTypes = propTypes; // defined at the top of the file

export default FilterByType;
