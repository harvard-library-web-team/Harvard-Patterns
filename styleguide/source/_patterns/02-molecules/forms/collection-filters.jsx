import React                from "react";
import MasonrySVG   from "../../01-atoms/svg-icons/svg-masonry-view.jsx";
import ListSVG      from "../../01-atoms/svg-icons/svg-list-view.jsx";

import { func, bool, string } from "prop-types";

const propTypes = {
  onKeywordChange: func.isRequired,
  onViewChange: func.isRequired,
  keyword: string.isRequired,
  listView: bool.isRequired
};

// CollectionFilters is a stateless functional component.
const CollectionFilters = (props) => {

  const { keyword, onViewChange, onKeywordChange, listView } = props;

  const viewClass = listView ? "hl__collection-filters--list" : "hl__collection-filters--masonry";

  return (
    <section className={ `hl__collection-filters ${ viewClass }` }>
      <div className="hl__collection-filters__container">
        <div className="hl__collection-filters__keyword">
          <label className="hl__label">Filter</label>
          <input
              className="hl__input"
              type="text"
              placeholder='Try "Audubon" or "digital"'
              value={ keyword }
              onChange={ onKeywordChange } />
        </div>
        <div className="hl__collection-filters__toggles">
          <button
            type="button"
            aria-label="Show the results in a non-accessible masonry view"
            onClick={ onViewChange }
            disabled={ !listView }>
            <MasonrySVG />
          </button>
          <button
            type="button"
            aria-label="Show the result in an accessible list view"
            onClick={ onViewChange }
            disabled={ listView }>
            <ListSVG />
          </button>
        </div>
      </div>
    </section>
  );
};

CollectionFilters.propTypes = propTypes; // defined at the top of the file

export default CollectionFilters;
