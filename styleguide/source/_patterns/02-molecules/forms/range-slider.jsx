import React, { Component }   from "react";
// https://github.com/davidchin/react-input-range
import InputRange             from "react-input-range";


import { number, object, func } from "prop-types";

const propTypes = {
  min: number.isRequired,
  max: number.isRequired,
  value: object.isRequired,
  onChange: func.isRequired
};

const RangeSlider = (props) => {

  const { min, max, value, onChange } = props;

  return (
    <div className="hl__range-slider">
      <InputRange 
        maxValue={ max }
        minValue={ min }
        value={ value }
        onChange={ onChange } />
    </div>
  );
};

RangeSlider.propTypes = propTypes; // defined at the top of the file

export default RangeSlider;

