import React    from "react";
import Select   from "react-select";
import R        from "ramda";

import { shape, array, func, arrayOf, string } from "prop-types";

const propTypes = {
  libraries: arrayOf(shape({
    amenities: array.isRequired,
    name: string.isRequired,
    id: string.isRequired,
    subjects: array.isRequired
  })),
  filterValues: shape({
    amenities: array.isRequired,
    names: array.isRequired,
    subjects: array.isRequired
  }),
  filterChange: func.isRequired
};

// LibraryFilters is a stateless functional component.
const LibraryFilters = (props) => {

  const filterOptions = (libraries) => {
    let filters = {};

    filters.names = libraries.map(library => { 
      return {
        label: library.name,
        value: library.id
      };
    });

    filters.amenities = R.compose(
      R.sortBy(R.prop("label")),
      R.uniqBy(R.prop("label")),
      R.flatten,
      R.map(R.prop("amenities"))
    )(libraries);

    filters.subjects = R.compose(
      R.sortBy(R.prop("label")),
      R.uniqBy(R.prop("label")),
      R.flatten,
      R.map(R.prop("subjects"))
    )(libraries);

    return filters;
  };

  const { filterValues, filterChange, libraries } = props;

  const filters = filterOptions(libraries);

  return (
    <section className="hl__library-filters">
      <div className="hl__library-filters__container">
        <div className="hl__library-filters__menus">
          <div className="hl__library-filters__menu">
            <label id="filter-by-name">By Name</label>
            <Select
              name="filter-by-name"
              value={ filterValues.names }
              multi={true}
              onChange={ filterChange.bind(null,"filter-by-name") }
              options={ filters.names }
              aria-labelledby="filter-by-name"
              clearable={false}
            />
          </div>
          <div className="hl__library-filters__menu">
            <label id="filter-by-amenity">By Feature</label>
            <Select
              name="filter-by-amenity"
              value={ filterValues.amenities }
              multi={true}
              onChange={ filterChange.bind(null,"filter-by-amenity") }
              options={ filters.amenities }
              aria-labelledby="filter-by-amenity"
              clearable={false}
            />
          </div>
          <div className="hl__library-filters__menu">
            <label id="filter-by-subject">By Subject</label>
            <Select
              name="filter-by-subject"
              value={ filterValues.subjects }
              multi={true}
              onChange={ filterChange.bind(null,"filter-by-subject") }
              options={ filters.subjects }
              aria-labelledby="filter-by-subject"
              clearable={false}
            />
          </div>
        </div>
        <div className="hl__library-filters__toggles">

        </div>
      </div>
    </section>
  );
};

LibraryFilters.propTypes = propTypes; // defined at the top of the file

export default LibraryFilters;
