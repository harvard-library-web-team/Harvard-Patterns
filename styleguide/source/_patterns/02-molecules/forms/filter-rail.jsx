import React            from "react";
import FilterGroup    from "./filter-group.jsx";

import { shape, string, func, bool, arrayOf, array } from "prop-types";

const propTypes = {
  assetsUrl: string.isRequired,
  hideFilters: bool.isRequired,
  message: string,
  groups: arrayOf(shape({
    hideAccordion: bool,
    param: string.isRequired,
    expanded: bool.isRequired,
    items: array
  })).isRequired,
  onAccordionChange: func,
  onHideFilters: func.isRequired,
  onClearAll: func.isRequired,
  title: string.isRequired
};

const FilterRail = (props) => {

  // if all accordions are collapsed
  // expand all accordions
  // otherwise collapse all accordions
  const handleCollapseAll = (event) => {
    const { groups, onAccordionChange } = props;
    const expandedState = accordionsCollapsed();

    groups.forEach(group => {
      onAccordionChange(group.param, expandedState);
    });
  };

  const handleHide = (event) => {
    props.onHideFilters(!props.hideFilters);
  };

  const accordionsCollapsed = () => {
    return props.groups.every(group => !group.expanded);
  };

  const renderGroups = (groups) => {
    const { assetsUrl, onAccordionChange } = props;

    return groups.filter(group => group.items && group.items.length).map(group => {
      return (
        <div 
          className="hl__filter-rail__group"
          key={ group.param }>
          <FilterGroup 
            accordionChange= { onAccordionChange }
            assetsUrl={ assetsUrl }
            hideAccordion={ group.hideAccordion }
            { ...group } />
        </div> 
      );
    });
  };

  const { 
    groups,
    hideFilters, 
    title, 
    onAccordionChange,
    onClearAll,
    assetsUrl,
    message } = props;

  const expandedClass = hideFilters ? "hl__filter-rail--collapsed" : " ";

  return (
    <div className={ `hl__filter-rail ${expandedClass}` }>
      <header className="hl__filter-rail__header">
        <h2 className="hl__filter-rail__title">{ title }</h2>
        <button 
          className="hl__filter-rail__hide-filters"
          type="button"
          aria-label="Hide/Show Filters"
          aria-expanded={ !hideFilters }
          onClick={ handleHide }><span className="hl__filter-rail__spacer"></span><img alt="" src={ `${ assetsUrl }/assets/images/svg-icons/collapse-icon.svg` } /></button>
        <div className="hl__filter-rail__header-controls">
          <button 
            className="hl__filter-rail__clear-button"
            type="button"
            aria-label="Clear all filters"
            onClick={ onClearAll }>Clear All</button>
          { onAccordionChange && (
            <div className="hl__filter-rail__collapse-row">
              <button 
                className="hl__filter-rail__collapse-all"
                type="button"
                aria-label="Toggle all filter groups"
                aria-expanded={ !accordionsCollapsed() }
                onClick={ handleCollapseAll }><span>Collapse All</span><span>Expand All</span></button>
            </div>
          )}
        </div>
      </header>
      <div className="hl__filter-rail__filters">
        { renderGroups(groups) }
        { message && (
          <div className="hl__filter-rail__message">
            { message }
          </div>
        )}
      </div>
    </div>  
  );
};

FilterRail.propTypes = propTypes; // defined at the top of the file

export default FilterRail;
