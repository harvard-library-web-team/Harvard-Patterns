import React                from "react";

import { func, bool, string } from "prop-types";

const propTypes = {
  onKeywordChange: func.isRequired,
  keyword: string.isRequired,
};

// NewsFilters is a stateless functional component.
const NewsFilters = (props) => {

  const { keyword, onKeywordChange} = props;

  return (
    <section className={ `hl__news-filters` }>
      <div className="hl__news-filters__container">
        <div className="hl__news-filters__keyword">
          <label className="hl__label">Search</label>
          <input
            className="hl__input"
            type="text"
            placeholder="Try 'keyword'"
            value={ keyword }
            onChange={ onKeywordChange } />
        </div>
      </div>
    </section>
  );
};

NewsFilters.propTypes = propTypes; // defined at the top of the file

export default NewsFilters;
