import React        from "react";
import ListSVG      from "../../01-atoms/svg-icons/svg-list-view.jsx";
import SingleListSVG      from "../../01-atoms/svg-icons/svg-single-list-view.jsx";
import MasonrySVG   from "../../01-atoms/svg-icons/svg-masonry-view.jsx";
import GridSVG      from "../../01-atoms/svg-icons/svg-card-view.jsx";

import { shape, arrayOf, string, bool, func } from "prop-types";

const propTypes = {
  label: string,
  onChange: func.isRequired,
  activeFilter: string,
  items: arrayOf(shape({
    name: string.isRequired,
    value: string.isRequired,
    label: string.isRequired,
    icon: string
  })).isRequired
};

const FilterToggles = (props) => {

  const handleChange = (event) => {
    props.onChange(event.target.value);
  };
  
  const renderFilters = (items) => {
    return items.map((item) => {
      return (
        <label
          key={ item.value }
          tabIndex="0"
          className={`hl__radio-input hl__radio-input--${ props.activeFilter }`}>

          <input
            name="toggle"
            aria-label={ item.name }
            type="radio"
            value={ item.value }
            checked= { item.value === props.activeFilter }
            onChange= { handleChange } />

          { item.icon && item.icon == "list" && (
            <ListSVG />
          )}
          { item.icon && item.icon == "masonry" && (
            <MasonrySVG />
          )}
          { item.icon && item.icon == "single" && (
            <SingleListSVG />
          )}
          { item.icon && item.icon == "grid" && (
            <GridSVG />
          )}
          <span className="hl__filter-toggles__label" aria-hidden>{ label }</span>
        </label>
      );
    });
  };

  const { label, items } = props;

  return (
    <div className="hl__filter-toggles__container">
      <div className="hl__filter-toggles">
          { renderFilters(items) }
      </div>
    </div>
  );
};

FilterToggles.propTypes = propTypes; // defined at the top of the file

export default FilterToggles;
