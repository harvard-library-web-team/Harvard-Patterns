import $                      from "jquery";
import React, { Component }   from "react";

import CheckboxFilter         from "../../01-atoms/forms/checkbox-filter.jsx";
import RangeSlider            from "../../02-molecules/forms/range-slider.jsx";
import WeekPicker             from "../../02-molecules/forms/week-picker.jsx";
import Select                 from "react-select";

import { array, string, bool, func } from "prop-types";

export default class FilterGroup extends Component {

  static propTypes = {
    accordionChange: func,
    assetsUrl: string,
    expanded: bool.isRequired,
    filterType: string.isRequired,
    hideAccordion: bool,
    items: array.isRequired,
    label: string.isRequired,
    onChange: func.isRequired,
    param: string.isRequired,
    placeholder: string,
    selected: array // for select type
  };

  componentWillReceiveProps(nextProps) {
    if(this.props.expanded == nextProps.expanded) {
      return;
    }

    const $content = $(this.accordionContent);

    if(nextProps.expanded) {
      $content.slideDown();
    } else {
      $content.slideUp();
    }
  }

  handleAccordion = (e) => {
    e.preventDefault();
    this.props.accordionChange(this.props.param,!this.props.expanded);
  }

  handleSelectChange = (value) => {
    const { param, onChange } = this.props;
    onChange(param,value);
  }

  renderFilters() {
    const {
      assetsUrl= "../..",
      param,
      onChange,
      filterType,
      selected = [],
      items,
      placeholder } = this.props;

    switch (filterType) {
      case "range_slider":
        return items.map((item, index) => {
          return (
            <RangeSlider
              key={ param + item.value }
              onChange={ onChange }
              { ...item } />
          );
        });
        break;
      case "checkbox":
        return items.map((item, index) => {
          return (
            <CheckboxFilter
              key={ param + item.value }
              onChange={ onChange }
              name= { param }
              { ...item } />
          );
        });
        break;
      case "inline-checkbox":
        return items.map((item, index) => {
          return (
            <CheckboxFilter
              key={ param + item.value }
              onChange={ onChange }
              name= { param }
              inline={ true }
              { ...item } />
          );
        });
        break;
      case "select":
        return (
          <Select
            name={ param }
            value={ selected }
            multi={ true }
            onChange={ this.handleSelectChange }
            options={ items }
            clearable={ false }
            placeholder={ placeholder }/>
        );
        break;
      case "keyword":
        return (
          <input
            className="hl__input"
            name={ param }
            id={ param }
            type="text"
            placeholder={ placeholder }
            value={ items.length && items[0].value ? items[0].value : "" }
            onChange={ onChange } />
        );
        break;
      case "weekPicker":
        return items.map((item, index) => {
          return (
            <WeekPicker
              key={ `single_date_${ index }` }
              assetsUrl={ assetsUrl }
              onDateChange={ onChange }
              { ...item } />
          );
        });
        break;
      default:
        return null;
    }
  }

  render() {
    const { label, expanded, filterType, hideAccordion, accordionChange, param } = this.props;

    const themeClass = filterType === "inline-checkbox" ? `hl__filter-group--inline` : "";

    const filterName = `hl__filter-group--${param}`;

    return (
      <fieldset className={ `hl__filter-group ${ themeClass } ${ filterName }` }>
        { !hideAccordion && accordionChange && (
          <legend><button className="hl__filter-group__label" type="button" aria-expanded={ expanded } onClick={ this.handleAccordion }>{ label }</button></legend>
        )}
        { hideAccordion && (
          <legend><div className="hl__filter-group__label">{ label }</div></legend>
        )}
        <div
          className="hl__filter-group__items"
          ref={el => this.accordionContent = el }>
          { this.renderFilters() }
        </div>
      </fieldset>
    );
  }
}
