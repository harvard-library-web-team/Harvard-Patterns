import React   from "react";
import GiftSVG      from "../../01-atoms/svg-icons/svg-gift.jsx";

import { shape, arrayOf, string, bool, func } from "prop-types";

const propTypes = {
  label: string,
  onChange: func.isRequired,
  activeFilter: string,
  items: arrayOf(shape({
    name: string.isRequired,
    value: string.isRequired,
    label: string.isRequired,
    icon: string
  })).isRequired
};

const SortByType = (props) => {

  const handleChange = (event) => {
    props.onChange(event.target.value);
  };

  const renderFilters = (items) => {
    return items.map((item) => {
      return (
        <label key={ item.value } className="hl__radio-input">
          <input
            name={ item.name }
            type="radio"
            value={ item.value }
            checked= { item.value === props.activeFilter }
            onChange= { handleChange } />
          <span className="hl__radio-input__label">
            { item.icon == "gift" && (
              <GiftSVG />
            )}
            { item.label }
          </span>
        </label>
      );
    });
  };

  const { label, items } = props;

  return (
    <div className="hl__sort-by-type">
      <fieldset>
        <legend>{ label }</legend>
        <span className="hl__sort-by-type__label" aria-hidden>{ label }</span>
        <div>
          { renderFilters(items) }
        </div>
      </fieldset>
    </div>
  );
};

SortByType.propTypes = propTypes; // defined at the top of the file

export default SortByType;
