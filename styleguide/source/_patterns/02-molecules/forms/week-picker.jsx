import React                  from "react";

// https://github.com/airbnb/react-dates
import "react-dates/initialize";
import { SingleDatePicker }   from "react-dates";
import moment                 from "moment";

import { object, func, bool, string, node } from "prop-types";
import { momentObj }          from "react-moment-proptypes";

const propTypes = {
  assetsUrl: string.isRequired,
  date: momentObj.isRequired,
  focused: bool.isRequired,
  onFocusChange: func.isRequired,
  onDateChange: func.isRequired,
  properties: object, // optional settings
  navPrev: node,
  navNext: node
};

const WeekPicker = (props) => {

  const onChange = (date) => {
    if(date.day() !== 0) {
      date.subtract(date.day(), "days");
    }

    props.onDateChange(date);
  };

  const onPreviousWeek = () => {
    props.onDateChange(props.date.subtract(7, "days"));
  };

  const onNextWeek = () => {
    props.onDateChange(props.date.add(7, "days"));
  };

  const {
    assetsUrl,
    properties,
    focused,
    date,
    onFocusChange } = props;

  const endDate = moment(date).add(6, "days").format("MMM DD, YYYY");

  const navPrev = (
    <svg width="9px" height="14px" viewBox="0 0 9 14" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
      <g>
        <g transform="translate(-733.000000, -834.000000)" fill="#1E1E1E">
          <g transform="translate(690.000000, 790.000000)">
            <polygon transform="translate(47.264520, 51.000000) scale(-1, 1) translate(-47.264520, -51.000000) " points="44.476406 58 51.2020202 51.0008077 44.476406 44 43.3270202 45.9416176 48.1873394 51.0008077 43.3270202 56.0583824"></polygon>
          </g>
        </g>
      </g>
    </svg>
  );

  const navNext = (
    <svg width="8px" height="14px" viewBox="0 0 8 14" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
      <g>
        <g transform="translate(-960.000000, -834.000000)" fill="#1E1E1E">
          <g transform="translate(690.000000, 790.000000)">
            <polygon points="271.149386 58 277.875 51.0008077 271.149386 44 270 45.9416176 274.860319 51.0008077 270 56.0583824"></polygon>
          </g>
        </g>
      </g>
    </svg>
  );

  const downArrow = (
    <svg width="11px" height="8px" viewBox="0 0 11 8" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
      <g fillOpacity="0.6">
        <g transform="translate(-806.000000, -764.000000)" fill="#1E1E1E">
          <g transform="translate(0.000000, 597.000000)">
            <g transform="translate(595.000000, 150.000000)">
              <polygon transform="translate(216.500000, 20.839286) rotate(90.000000) translate(-216.500000, -20.839286) " points="213.160714 15.3392857 213.160714 26.3392857 219.839286 20.839744"></polygon>
              </g>
            </g>
        </g>
      </g>
    </svg>)

  return (
    <div className="hl__week-picker">
      <div className="hl__week-picker__date-range">
        <div className="hl__week-picker__control">
          <button
          disabled={ date < moment() }
          onClick={ onPreviousWeek }
          type="button"
          aria-label="Previous week"
          className="hl__week-picker__control--previous">
          { navPrev }
          </button>
      </div>
        <SingleDatePicker
          date={ date }
          focused={ focused }
          onFocusChange={ onFocusChange }
          onDateChange={ onChange }
          navPrev={ navPrev }
          navNext={ navNext }
          showDefaultInputIcon
          inputIconPosition="after"
          customInputIcon={ downArrow }
          { ...properties } />
        <span className="hl__week-picker__to-divider">to</span>
        <div className="hl__week-picker__end-date__group">
        <span className="hl__week-picker__end-date">
          { endDate }
        </span>
        <div className="hl__week-picker__control">
          <button
          onClick={ onNextWeek }
          type="button"
          aria-label="Next week"
          className="hl__week-picker__control--next">
            { navNext }
          </button>
        </div>
      </div>
      </div>
    </div>
  );
};

WeekPicker.propTypes = propTypes; // defined at the top of the file

export default WeekPicker;
