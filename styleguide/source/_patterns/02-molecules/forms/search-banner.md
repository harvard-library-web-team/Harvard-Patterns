### Description
This pattern shows a page title and text along with a search input.

### Status
* Stable as of 1.0.0

### JavaScript Used
* This pattern uses JavaScript to watch for when the inputs are in focus to show a background color

### Pattern Contains
* Rich Text
* Single Input Form

### Variables
~~~
"searchBanner": {
  "bgNarrow": 
    type: string (url) / required
  "bgWide":  
    type: string (url) / required
  "title": 
    type: string (with html) / required
  "description": {
    type: richText / optional
  },
  "singleInputForm": {
    type: singleInputForm / required
  },
  "helperText": {
    type: richText / optional
  }
}
~~~
