### Description
This pattern shows a list of items with an optional icon displayed in columns.  A set number of featured items can be display with an accordion of the rest below.

### Status
* Stable as of 1.0.0

### Pattern Contains
* Comp Heading
* Link Tag
* Image

### Variant options
* This pattern can also be rendered in a [small](./?p=molecules-illustrated-list-as-small) variant

### Usage Guides
* setting the `numFeatured` variable will show that many items above the accordion
* You can elimate the accordion if you set the `numFeatured` variable greater than or equal to the length of featured items.

### JavaScript Used
* This pattern uses JavaScript for the accordions (js/helpers/accordions.js)

### Variables
~~~
illustratedList: {
  compHeading: {
    type: compHeading / optional
  },
  numFeatured: 
    type: number (0, 2, 4, 6 or items.length) / required,
  theme:
    type: string ("small", "") / optional
  seeAll: {
    type: linkTag / required
  },
  items: [{
    icon: {
      type: image / optional
    },
    text: 
      type: string / required
  }]
}
~~~
