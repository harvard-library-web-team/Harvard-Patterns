import React        from "react";
import $            from "jquery";

import { string, arrayOf, shape, number, bool } from "prop-types";

const propTypes = {
  name: string.isRequired,
  subTitle: string,
  href: string.isRequired,
  directions_link: string,
  spaces_link: string,
  weeks_hours: shape({
    lid: number,
    title: string,
    departments_hours: arrayOf(shape({
      lid: number,
      title: string,
      weeks: arrayOf(shape({
        date: string,
        rendered: string
      }))
    })),
    weeks: arrayOf(shape({
      date: string,
      rendered: string
    }))
  }),
  headerDates: arrayOf(shape({
    day: string.isRequired,
    date: string.isRequired
  })),
  mark: number,
  external: bool.isRequired
};

const LibraryHoursTable = (props) => {

  const renderHead = () => {
    const { headerDates } = props;

    return headerDates.map(d => {
      return (
        <th key={ d.day }>
          <span className="hl__library-hours-table__day">{ d.day }</span>
          <span className="hl__library-hours-table__date">{ d.date }</span>
        </th>
      );
    });
  };

  const renderDays = (dates) => {
    const { headerDates } = props;

    if(dates === null || !dates.weeks) {
      return;
    }

    return dates.weeks.map((day,i) => {
      return (
        <td
          key={ day.date }
          data-label={ `${headerDates[i].day} ${headerDates[i].date}` }>{ day.rendered }</td>
      );
    });
  };

  const renderDepartmentRows = () => {
    const { weeks_hours } = props;

    if(weeks_hours === null || typeof weeks_hours.department_hours === "undefined") {
      return;
    }

    return (weeks_hours.department_hours.map((dept) => {
      return (
        <tr key={ dept.lid } className="department-hours">
          <th>{ dept.title }</th>
          { renderDays(dept) }
        </tr>
      );
    }));
  };

  const {
    name,
    subTitle,
    href,
    directions_link,
    spaces_link,
    mark,
    weeks_hours,
    external } = props;

  const markClass = mark ? `hl__library-hours-table--mark-${ mark + 1 }` : "";
  const libraryName = `hl__library-hours-table__${ weeks_hours.lid }`;

  const handleAccordion = (e) => {
    e.preventDefault();

    const $table = $(`.${libraryName} .hl__library-hours-table__table`);
    const $generalHours = $table.find(".general-hours");

    $table.toggleClass("detailed-hours--open");
    $generalHours.toggleClass("detailed-hours--open");

    const $link = $(e.currentTarget);
    const $linkText = $link.find("span");

    if($link.attr("aria-expanded") === "true") {
      $link.attr("aria-expanded", "false");
      $link.attr("aria-label", `show detailed hours for ${ name }`);
      $linkText.text("Show Detailed Hours");
    } else {
      $link.attr("aria-expanded", "true");
      $link.attr("aria-label", `hide detailed hours for ${ name }`);
      $linkText.text("Hide Detailed Hours");
    }
  }

  return (
    <section
      className={ `hl__library-hours-table ${ markClass } ${ libraryName }` }>

      <div className="hl__library-hours-table__container">
        <div className="hl__library-hours-table__header">
          <h2 className="hl__library-hours-table__title">
            <a href={ href } aria-label={ `${name} details`}>
              { name }
              { external == true && (
                <span className="hl__link-tag__external-icon">
                  <svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" width="20" height="20" viewBox="0 0 20 20">
                    <g>
                      <g transform="translate(-906 -1005)">
                        <path d="M923.78 1022.78V1015H926v7.78c0 1.22-1 2.22-2.22 2.22h-15.56c-1.23 0-2.22-1-2.22-2.22v-15.56c0-1.22.99-2.22 2.22-2.22H916v2.22h-7.78v15.56zm-5.71-17.78H926v7.93h-2.27v-4.07L912.6 1020l-1.6-1.6 11.14-11.13h-4.07z"/>
                      </g>
                    </g>
                  </svg>
                </span>
              )}
            </a>
          </h2>
          { subTitle && (
            <p className="hl__library-hours-table__subtitle">{ subTitle }</p>
          )}
          { directions_link && (
            <div className="hl__library-hours-table__links">
              <div className="hl__library-hours-table__address">
                <span className="hl__library-hours-table__map-icon"><svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" width="12" height="17" viewBox="0 0 12 17"><g><g transform="translate(-988 -1676)"><path fill="silver" d="M994 1676c3.32 0 6 2.66 6 5.95 0 4.46-6 11.05-6 11.05s-6-6.59-6-11.05a5.97 5.97 0 0 1 6-5.95zm0 8a2 2 0 1 0 0-4 2 2 0 0 0 0 4z"/></g></g></svg></span>
                <a
                  href={ directions_link }
                  aria-label={ `get directions to ${ name }`}
                  className="hl__library-hours-table__address-text hl__link-tag">Map</a>
              </div>

              { spaces_link && (
                <div className="hl__library-hours-table__spaces">
                  <span className="hl__library-hours-table__spaces-icon">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" width="17" height="20" viewBox="0 0 17 20"><g><g transform="translate(-1233 -33)"><path fill="silver" d="M1241.5 50.59v-.27c0-.25-.1-.49-.27-.66l-5.23-5.23 6.03-6.03.32.32c-.41 1.58.23 3.61 1.79 5.17l5.65-5.65c-1.56-1.56-3.59-2.2-5.17-1.8l-3.45-3.44-2.27 2.27 1.55 1.55.25.26-6.69 6.69a.94.94 0 0 0 0 1.32l5.5 5.5c-2.27.22-3.98 1.22-3.98 2.41h9.93c0-1.19-1.7-2.18-3.95-2.41"/></g></g></svg>
                  </span>
                  <a
                    href={ spaces_link }
                    aria-label={ `explore spaces at ${ name }`}
                    className="hl__library-hours-table__spaces-text hl__link-tag">Explore Spaces</a>
                </div>
              )}
            </div>
          )}
      </div>

        <table className="hl__library-hours-table__table">
        <caption className="hl__visually-hidden">{ name } hours</caption>
        <thead>
          <tr>
            <th>departments</th>
            { renderHead() }
          </tr>
        </thead>
        <tbody>
          <tr className="general-hours">
            <th>General</th>
            { renderDays(weeks_hours) }
          </tr>
          { renderDepartmentRows() }
        </tbody>
      </table>
      </div>

      { weeks_hours.department_hours && weeks_hours.department_hours.length > 0 && (
        <div className="hl__library-hours-table__hours-link">
          <a href="#"
            aria-label={`show detailed hours for ${ name }`}
            className="hl__link-tag hl__link-tag--with-arrow"
            onClick={ handleAccordion }
            aria-expanded="false">
            <span>Show Detailed Hours</span>
          </a>
        </div>
      )}
    </section>
  );
};

LibraryHoursTable.propTypes = propTypes; // defined at the top of the file

export default LibraryHoursTable;
