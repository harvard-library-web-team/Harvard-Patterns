import React      from "react";

import { string, number, arrayOf, shape } from "prop-types";

const propTypes = {
  label: string,
  columns: number,
  items: arrayOf(shape({
    id: string,
    title: string.isRequired,
    description: string,
    external_link: string,
    internal_link: string.isRequired
  })).isRequired
};

const GroupedList = (props) => {

  const renderExternalTitle = (item) => {
    return(
      <a
        className="hl__link-tag hl__grouped-list__title hl__grouped-list__title--external"
        href={ item.external_link }>
        <span>{ item.title }</span>
        <span class="hl__link-tag__external-icon hl__link-tag__external-icon--blue">
          <svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" width="20" height="20" viewBox="0 0 20 20">
            <g>
              <g transform="translate(-906 -1005)">
                <path d="M923.78 1022.78V1015H926v7.78c0 1.22-1 2.22-2.22 2.22h-15.56c-1.23 0-2.22-1-2.22-2.22v-15.56c0-1.22.99-2.22 2.22-2.22H916v2.22h-7.78v15.56zm-5.71-17.78H926v7.93h-2.27v-4.07L912.6 1020l-1.6-1.6 11.14-11.13h-4.07z"/>
              </g>
            </g>
          </svg>
        </span>
      </a>
    );
  };

  const renderInternalTitle = (item) => {
    return(
      <a
        className="hl__link-tag hl__grouped-list__title"
        href={ item.internal_link }>{ item.title }</a>
    );
  };

  const renderExternalDescription = (item) => {
    return(
      <p className="hl__grouped-list__description">
        { item.description } (<a
          className="hl__link-tag"
          href={ item.internal_link }
          title={ `More information about ${ item.title }` }>more info...</a>)</p>
    );
  };

  const renderInternalDescription = (item) => {
    return(
      <p className="hl__grouped-list__description">
        { item.description }
      </p>
    );
  };

  const renderDD = (items) => {
    return items.map(item => {
      return (
        <dd
          className="hl__grouped-list__item"
          key={ item.id }>
          { item.external_link && (
            renderExternalTitle(item)
          )}
          { !item.external_link && (
            renderInternalTitle(item)
          )}
          { item.description && item.external_link && (
            renderExternalDescription(item)
          )}
          { item.description && !item.external_link && (
            renderInternalDescription(item)
          )}
        </dd>
      );
    });
  };

  const renderLI = (items) => {
    return items.map(item => {
      return (
        <li
          className="hl__grouped-list__item"
          key={ item.id }>
          { item.external_link && (
            renderExternalTitle(item)
          )}
          { !item.external_link && (
            renderInternalTitle(item)
          )}
          { item.description && item.external_link && (
            renderExternalDescription(item)
          )}
          { item.description && !item.external_link && (
            renderInternalDescription(item)
          )}
        </li>
      );
    });
  };

  const { columns = 1, items, label = "" } = props;
  const columnClass = `hl__grouped-list--${ columns }`;

  return (

    <section className={ `hl__grouped-list ${ columnClass }` }>
      <div className="hl__grouped-list__container">
        { label && (
          <dl className="hl__grouped-list__items hl__grouped-list__items--dl">
            <dt className="hl__grouped-list__label">{ label }</dt>
            { renderDD(items) }
          </dl>
        )}
        { !label && (
          <ul className="hl__grouped-list__items hl__grouped-list__items--ul">
            { renderLI(items) }
          </ul>
        )}
      </div>
    </section>
  );
};

GroupedList.propTypes = propTypes; // defined at the top of the file

export default GroupedList;
