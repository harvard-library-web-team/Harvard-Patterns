### Description
This is a variant of the [Illustrated List](./?p=molecules-illustrated-list) pattern showing an example with small icons and text.

### How to generate
* set the `theme` variable to `small`
