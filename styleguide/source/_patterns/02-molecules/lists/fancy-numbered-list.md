### Description
This pattern shows a list of plain text items with a stylized numbered list treatment.

### Status
* Stable as of 1.0.0

### Pattern Contains
* Comp Heading
* Rich Text

### Variables
~~~
fancyNumberedList: {
  bgNarrow: 
    type: string (url) / optional
  bgWide: 
    type: string (url) / optional
  id: 
    type: string (unique per page) / required with backgrounds
  compHeading: {
    type: compHeading / optional
  },
  centered: 
    type: boolean
  richText: {
    type: richText / optional
  },
  items:[{
    type: array of raw input / required
  }]
}
~~~
