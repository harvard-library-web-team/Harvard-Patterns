### Description
This pattern shows a list of links or plain text with a label.

### Status
* Stable as of 1.0.0

### Pattern Contains
* Link Tag

### Variables
~~~
labeledList: {
  id:
    type: text / GUID / required
  label: 
    type: text / required
  items:[{
    type: array of Link Tag / required
  }]
}
~~~
