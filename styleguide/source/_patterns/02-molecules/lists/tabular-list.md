### Description
This pattern shows a two column list of data rendered in a table.

### Status
* Stable as of 1.0.0

### Variables
~~~
tabularList: {
  rows: [{
    label: 
      type: string / optional
    text:  
      type: string / required
  }]
}
~~~
