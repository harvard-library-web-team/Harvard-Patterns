### Description
This pattern shows a list of icons with link.

### Status
* Alpha

### Pattern Contains
* Icon Link

### Variables
~~~
iconList: {
  label: 
    type: text / optional
  iconLinks: [{
    href: 
      type: string / required,
    info:
      type: string / required
    icon: 
      type: string (path to SVG twig file) / required
  }] 
}
~~~
