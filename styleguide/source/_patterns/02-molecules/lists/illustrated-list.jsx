import React      from "react";
import Image      from "../../01-atoms/media/image.jsx";

import { string, arrayOf, shape } from "prop-types";

const propTypes = {
  items: arrayOf(shape({ 
    text: string.isRequired,
    icon: shape
  })).isRequired,
  theme: string
};

const IllustratedList = (props) => {

  const renderItems = (items) => {
    return items.map(item => {
      return (
        <div 
          className="hl__illustrated-list__list-item"
          key={ item.text.replace(/\s/g, "") }>
          { item.icon && (
            <span className="hl__illustrated-list__image-wrapper">
              <Image { ...item.icon } />
            </span>
          )}
          <span>{ item.text }</span>
        </div>
      );
    });
  };

  const { theme, items } = props;

  const themeClass = theme ? `hl__illustrated-list--${theme}` : "";
  
  return (

    <section className={`hl__illustrated-list ${ themeClass }`}>
      <div className="hl__illustrated-list__featured-items">
        { renderItems(items) }
      </div>
    </section>
  );
};

IllustratedList.propTypes = propTypes; // defined at the top of the file

export default IllustratedList;
