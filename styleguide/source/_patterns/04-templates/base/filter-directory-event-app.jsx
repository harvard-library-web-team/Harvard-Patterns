import $                      from "jquery";
import React, { Component }   from "react";
import R                      from "ramda";
import queryStringApp         from "query-string";

import FilterRail             from "../../02-molecules/forms/filter-rail.jsx";
import FilterByType           from "../../02-molecules/forms/filter-by-type.jsx";
import EventGrid              from "../../03-organisms/lists/event-grid.jsx";
import StaffGrid              from "../../03-organisms/lists/staff-grid.jsx";
import Button                 from "../../01-atoms/buttons/button.jsx";

import { string } from "prop-types";

export default class EventDirectoryApp extends Component {
  static propTypes = {
    requestUrl: string.isRequired,
    assetsUrl: string.isRequired
  }

  constructor(props) {
    super(props);

    this.page = 0;
    this.$ajaxResults = $.ajax();
    this.queryString = queryStringApp.parse(location.search); // params in the url
    this.emptyMessage = "Loading...";

    this.state = {
      hideFilters: document.documentElement.clientWidth <= 1170,
      filters: {
        dates: {
          min: "",
          max: "",
          label: "Show Me:",
          activeFilter: "0",
          items: [{
            name: "event-listing-tabs",
            value: "0",
            label: "All",
            checked: true
          },{
            name: "event-listing-tabs",
            value: "1",
            label: "Today",
            checked: false
          },{
            name: "event-listing-tabs",
            value: "2",
            label: "This Week",
            checked: false
          },{
            name: "event-listing-tabs",
            value: "3",
            label: "This Month",
            checked: false
          }]
        },

        library: {
          label: "Library",
          placeholder: "All Libraries",
          param: "library_verf",
          expanded: true,
          hideAccordion: true,
          filterType: "select",
          onChange: this.onSelectChange, 
          selected: [],
          items: []
        },
        subject: {
          label: "Subject",
          placeholder: "All Subjects",
          param: "subject",
          expanded: true,
          hideAccordion: true,
          filterType: "select",
          onChange: this.onSelectChange, 
          selected: [],
          items: []
        },
        eventTypes: {
          label: "Event Type",
          param: "event_type",
          expanded: true,
          hideAccordion: true,
          filterType: "checkbox",
          onChange: this.onFilterChange, 
          items: []
        },
        admittance: {
          label: "Admittance",
          param: "restricted",
          expanded: true,
          hideAccordion: true,
          filterType: "checkbox",
          onChange: this.onFilterChange, 
          items: [{
            label: "Open to the Public",
            checked: this.queryString.restricted === "0",
            value: "0",
            icon: ""
          }]

        }
      },
      results: {
        totalCount: 0,
        items: []
      }
    };
  }

  componentDidMount() {
    // fetch all expertise / subject
    const getSubject = this.fetchSubject();
    // fetch all expertise / subject
    const getEventTypes = this.fetchEventTypes();
    // fetch all Libraries
    const getLibraries = this.fetchLibraries().then(data => {    
      const filters = data.map(library => {
        return {
          label: library.name,
          value: library.id
        };
      });
      
      return {
        filters,
        libraries: data
      };
    });

    Promise
    .all([getSubject,getLibraries,getEventTypes])
    .then(values => {
      let filters = Object.assign({}, this.state.filters);

      filters.subject.items = filters.subject.items.concat(values[0]);
      filters.library.items = filters.library.items.concat(values[1].filters);
      filters.eventTypes.items = filters.eventTypes.items.concat(values[2]);

      // mark filters found in the URL
      filters = this.initializeFilters(filters);

      // update state and render filters
      this.setState({filters, libraries: values[1].libraries});

      // fetch results
      this.fetchResults();

      this.emptyMessage = "No Results";
    });
  }

  /********** Data Fetching ***************/

  // returns an array of properly formated event types
  fetchEventTypes = () => {
    return fetch(`${ this.props.requestUrl }/api/v1/taxonomy/event_types?_format=json`)
    .then(response => response.json())
    .then(data => {
      let checked = [];
      const events = data.map(eventType => {
        eventType.value = eventType.tid;
        if(eventType.checked === "1") {
          checked.push(eventType.tid);
        }
        eventType.checked = eventType.checked === "1" && !window.location.search;
        return eventType;
      });
      // Only update the URL with the checked filters if params do not already exist in the URL.
      if (!window.location.search) {
        this.updateUrl("event_type",checked);
      }
      return events;
    })      
    .catch(error => {
      console.warn("failed to fetch event types:", error);
      return [];
    });
  }
  // returns an array of properly formated filters
  fetchSubject = () => {
    return fetch(`${ this.props.requestUrl }/api/v1/taxonomy/subject?_format=json`)
    .then(response => response.json())
    .then(data => {
      return data.map(d => {
        return {
          label: d.label,
          value: d.tid
        };
      });
    })      
    .catch(error => {
      console.warn("failed to fetch expertise:", error);
      return [];
    });
  }
  // returns an array of properly formated filters
  fetchLibraries = () => {
    return fetch(`${ this.props.requestUrl }/api/v1/library?_format=json`)
    .then(response => response.json())
    .then(data => data)      
    .catch(error => {
      console.warn("failed to fetch libraries:", error);
      return [];
    });
  }

  // optional loadMore boolean is used to append or replace the existing results
  fetchResults = (loadMore = false) => {   
    if(loadMore) {
      this.page++;
    } else {
      this.page = 0;
    }

    let activeFilters = Object.assign({
      page: this.page,
      _format: "json"
    }, this.queryString);

    this.$ajaxResults.abort();

    //The restricted filter is not an array, so convert to string if it exists
    //This way the Drupal View can use it correctly
    if (activeFilters.hasOwnProperty("restricted")) {
      activeFilters.restricted = activeFilters.restricted[0];
    }

    // The library filter should search both library and sponsored library fields
    // So add the library data to a new sponsored library array
    if (activeFilters.hasOwnProperty("library_verf")) {
      activeFilters.sponsoring_library_verf = new Array();
      if (Array.isArray(activeFilters.library_verf)) {
        Array.from(activeFilters.library_verf).forEach(function (verf) {
          activeFilters.sponsoring_library_verf.push(verf);
        });
      }
      else {
        activeFilters.sponsoring_library_verf.push(activeFilters.library_verf);
      }
    }

    this.$ajaxResults = $.ajax({
      url: `${ this.props.requestUrl }/api/v1/events`,
      // cache: false,
      data: activeFilters
    });

    this.$ajaxResults.done(data => {
      const currentResults = this.state.results;
      let newResults = {
        totalCount: data.results.totalCount || 0,
        items: []
      };

      if (typeof data.results.items === "undefined" || data.results.items.length === 0) {
        
        if(!loadMore) {
          this.setState({ results: newResults });
        }
        return;
      }

      const newItems = this.parseResultItems(data.results.items,true);

      if(loadMore) {
        newResults.items = currentResults.items.concat(newItems);
      } else {
        newResults.items = newItems;
      }

      this.setState({ results: newResults });
    }).fail((xhr, status, error)=> {
      console.warn("failed to fetch results");
      
      const results = {
        totalCount: 0,
        items: []
      };

      this.setState({ results });
    });
  }
  /********** End Data Fetching ***************/

  /********** Data Manipulation ***************/
  parseResultItems = (data) => {
    const { filters } = this.state;

    return data.map((event, i) => {
      const eventTypeData = R.find(R.propEq("tid", event.event_type))(filters.eventTypes.items);
      let location = [];

      if( event.location_org ) {
        location.push(event.location_org);
      }

      if( event.location_address1 ) {
        location.push(event.location_address1);
      }

      if( event.location_address2 ) {
        location.push(event.location_address2);
      }

      if( event.location_name ) {
        location.push(event.location_name);
      }

      return {
        id: event.id || `${i}`,
        row_index: event.row_index,
        title: event.title,
        type: eventTypeData ? eventTypeData.label : "",
        subText: event.subtitle || "",
        date: event.date,
        time: event.hours,
        location: location.join(", "),
        href: event.url,
        info: `See details about "${ event.title }"`
      };
    });
  }

  initializeFilters = (filters) => {
    const params = this.queryString;
    if(params.subject && filters.subject.items.length) {
      if(typeof params.subject === "string") {
        params.subject = [params.subject];
      }
      filters.subject.selected = filters.subject.items.filter(a => params.subject.some((b) => a.value === b ) );
    }

    if(params.library_verf && filters.library.items.length) {
      if(typeof params.library_verf === "string") {
        params.library_verf = [params.library_verf];
      }
      filters.library.selected = filters.library.items.filter(a => params.library_verf.some((b) => a.value === b ) );
    }

    if(params.event_type && filters.eventTypes.items.length) {
      if(typeof params.event_type === "string") {
        params.event_type = [params.event_type];
      }
      filters.eventTypes.items = markItem(params.event_type, filters.eventTypes.items);
    }
    
    return filters;

    function markItem(param, filterItems) {
      param.forEach(paramValue => {
        const filterIndex = filterItems.findIndex(item => {
          return item.value === paramValue;
        });
        if(filterItems[filterIndex]){
          filterItems[filterIndex].checked = true;
        }
      });

      return filterItems;
    }
  }

  // function takes two optional values 
  // the url parameter and the value it should equal
  // if neither a parameter or value are passed, 
  // we assume the queryString has been updated else where
  // and we just need to reset the url
  updateUrl = (param, value = null) => {
    // if a parameter and a value are passed add the new value
    if(param && value) {
      this.queryString[param] = value;
    }
    // if a parameter is passed with a value remove the parameter
    if(param && (value === null || value.length === 0)) {
      delete this.queryString[param];
    }

    const stringifiedQueries = queryStringApp.stringify(this.queryString);
    const newUrl = window.location.origin + window.location.pathname + "?" + stringifiedQueries;

    window.history.replaceState({path: newUrl}, "", newUrl);
  }

  /********** End Data Manipulation ***************/

  /********** Event Handlers ***************/

  handleLoadMore = () => {
    this.fetchResults(true);
  }

  onDateChange = (value) => {
    // if the user selects the active filter
    if (value === this.state.filters.dates.activeFilter) {
      // do nothing
      return;
    }

    // get the current filter values
    let filters = Object.assign({}, this.state.filters);    

    const currentTime = new Date(Date.now());
    const endDate = new Date();
    
    filters.dates.activeFilter = value;
    this.currentPage = 0;

    switch(value) {
      case "1": // Today
        filters.dates.min = returnDate(currentTime);
        filters.dates.max = returnDate(currentTime, 'end');
        break;
      case "2": // Week
        filters.dates.min = returnDate(currentTime); 
        filters.dates.max = returnDate(new Date(endDate.setDate(currentTime.getDate() + 7)), 'end');
        break;
      case "3": // month
        filters.dates.min = returnDate(currentTime);
        filters.dates.max = returnDate(new Date(endDate.setDate(currentTime.getDate() + 30)), 'end');
        break;
      case "4": // custom range - TBD

        break;
      default: //all
        filters.dates.min = ""; 
        filters.dates.max = "";
    }

    if(filters.dates.min !== "" && filters.dates.max !== "") {
      this.queryString.min_date = filters.dates.min;
      this.queryString.max_date = filters.dates.max;
    } else {
      delete this.queryString["min_date"];
      delete this.queryString["max_date"];
    }

    this.updateUrl();

    this.fetchResults();
    this.setState({filters});

    function returnDate(date, dateType) {
      const monthAdjusted = date.getMonth() + 1;
      const month = monthAdjusted < 10 ? `0${ monthAdjusted }` : `${monthAdjusted}`;  
      const year = date.getFullYear();
      const rawDay = date.getDate();
      const day = rawDay < 10 ? `0${ rawDay }` : `${rawDay}`;
      var myTime = '00:00';
      if (dateType === 'end') {
       myTime = '23:59';
      }
      return `${year}-${month}-${day} ${myTime}`;

    }
  }

  // when a filter is clicked
  // recieves the event from the checkbox
  onFilterChange = (event) => {
    // loops through the array looking for the checked property as true
    const allCheckedValues = R.compose(
      R.map(R.prop("value")),
      R.filter(R.prop("checked"))
    );

    const filterValue = event.target.value;
    const filterName = event.target.name;

    let filters = Object.assign({}, this.state.filters);

    for (var filterObj in filters) {
      if(filterName === filters[filterObj].param) {
        const filterIndex = filters[filterObj].items.findIndex(item => {
          return item.value === filterValue;
        });

        filters[filterObj].items[filterIndex].checked = !filters[filterObj].items[filterIndex].checked;
        this.updateUrl(filters[filterObj].param, allCheckedValues(filters[filterObj].items));

        // fetch new results
        this.fetchResults();

        // update the state of the filters
        this.setState({ filters });

        break;
      }
    }
  }

  onSelectChange = (filterName, value) => {
    let filters = Object.assign({}, this.state.filters);

    for (var filterObj in filters) {
      if(filterName === filters[filterObj].param) {
        filters[filterObj].selected = value;

        this.updateUrl(filterName, value.map(v => v.value));
        break;
      }
    }

    this.setState({ filters });

    // fetch results
    this.fetchResults();   
  }

  // when an filter accordion expands or collapses
  // receives the key value of the filter and a boolean state
  onAccordionChange = (filterName, value) => {
    let filters = Object.assign({}, this.state.filters);

    for (var filterObj in filters) {
      if(filterName === filters[filterObj].param) {
        filters[filterObj].expanded = value;
        break;
      }
    }

    this.setState({ filters });   
  }

  // when the clear all button is clicked
  // receives the event from the button
  onClearAll = (event) => {
    event.preventDefault();
    let oldFilters = this.state.filters;

    const filters = R.map((filter) => {
      if(filter.selected) {
        filter.selected = [];
      }
      if (filter.filterType === 'checkbox') {
        for (let item of filter.items){
          if(item.checked) {
            item.checked = false;
          }
        }
      }
      return filter;
    },oldFilters);

    // clear the parameters in the url
    this.queryString = {};
    this.updateUrl();

    this.fetchResults();

    this.setState({filters});
  }

  // When the user clicks the >> or << button to show or hide the filters
  // receives the desired state as a boolean
  // true == hidden
  // false == visible
  onHideFilters = (value) => {
    this.setState({ hideFilters: value });     
    
    if(this.state.showDetails) {
      this.onHideDetail();
    }
  }


  /********** End Event Handlers ***************/


  render() {
    const { assetsUrl } = this.props;
    const { filters, results, hideFilters } = this.state;
    const button = {
      text: "Load More",
      theme: "secondary"
    };

    const gridModifier = hideFilters 
        ? "hl__filter-directory__container--no-filters" 
        : " ";
    const viewModifier = hideFilters 
        ? "hl__filter-directory__views-container--no-filters" 
        : " ";

    const filterArray = [
      filters.library,
      filters.subject,
      filters.eventTypes,
      filters.admittance
    ];

    return(
      <div className="hl__filter-directory__app">
        <section className={ `hl__filter-directory__views ${ viewModifier }` }>
          <div className={ `hl__filter-directory__views-container ${ viewModifier }` }>
            <FilterByType
              onChange= { this.onDateChange }
              { ...filters.dates } />
          </div>
        </section>
        <div className={`hl__filter-directory__container ${ gridModifier }` }>
          <section className="hl__filter-directory__filters">
            <FilterRail 
              onHideFilters={ this.onHideFilters }
              onClearAll={ this.onClearAll }
              assetsUrl={ assetsUrl } 
              title="Filter Events"
              hideFilters={ hideFilters }
              groups= { filterArray } />
          </section>

          <section className="hl__filter-directory__results">
            <div className="hl__filter-directory__result-details">
              Showing <b>{ results.items.length }</b> of <b>{ results.totalCount }</b>
            </div>

            { results.items.length > 0 && (
              <EventGrid events={ results.items } />
            )}
            { results.items.length === 0 && (
              <span className="hl__filter-directory__no-results">{ this.emptyMessage }</span>
            )}
            { results.items && results.items.length < results.totalCount && (
              <div className="hl__filter-directory__load-more">
                <Button 
                  handleClick = { this.handleLoadMore }
                  { ...button } />
              </div>
            )}
          </section>
        </div>
      </div>
    );
  }
}
