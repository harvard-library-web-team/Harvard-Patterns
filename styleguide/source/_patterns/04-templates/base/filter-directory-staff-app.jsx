import $                      from "jquery";
import React, { Component }   from "react";
import R                      from "ramda";
import queryStringApp         from "query-string";

import FilterRail             from "../../02-molecules/forms/filter-rail.jsx";
import FilterByType           from "../../02-molecules/forms/filter-by-type.jsx";
import StaffGrid              from "../../03-organisms/lists/staff-grid.jsx";
import Button                 from "../../01-atoms/buttons/button.jsx";

import { string } from "prop-types";

export default class StaffDirectoryApp extends Component {
  static propTypes = {
    requestUrl: string.isRequired,
    assetsUrl: string.isRequired
  }

  constructor(props) {
    super(props);

    this.state = {
      hideFilters: document.documentElement.clientWidth <= 1170,
      staffType: {
        label: "Show Me:",
        param: "view",
        activeFilter: "0",
        items: [{
          name: "staff-listing-tabs",
          value: "0",
          label: "Library Specialists",
          checked: true
        },{
          name: "staff-listing-tabs",
          value: "1",
          label: "All Staff",
          checked: false
        }]
      },
      filters: {
        name: {
          label: "Name",
          placeholder: "All Names",
          param: "name",
          expanded: true,
          hideAccordion: true,
          filterType: "keyword",
          onChange: this.onKeywordChange,
          items:[{
            value: ""
          }]
        },
        subject: {
          label: "Area of Expertise",
          placeholder: "All Subjects",
          param: "area_of_expertise",
          expanded: true,
          hideAccordion: true,
          filterType: "select",
          onChange: this.onSelectChange, 
          selected: [],
          items: []
        },
        languages: {
          label: "Languages",
          placeholder: "All Languages",
          param: "language",
          expanded: true,
          hideAccordion: true,
          filterType: "select",
          onChange: this.onSelectChange, 
          selected: [],
          items: []
        },
        department: {
          label: "Affiliation",
          placeholder: "All Affiliations",
          param: "department",
          expanded: true,
          hideAccordion: true,
          filterType: "select",
          onChange: this.onSelectChange, 
          selected: [],
          items: []
        },
        role: {
          label: "Role",
          placeholder: "All Roles",
          param: "role",
          expanded: true,
          hideAccordion: true,
          filterType: "select",
          onChange: this.onSelectChange, 
          selected: [],
          items: []
        },
        library: {
          label: "Library",
          placeholder: "All Libraries",
          param: "library",
          expanded: true,
          hideAccordion: true,
          filterType: "select",
          onChange: this.onSelectChange, 
          selected: [],
          items: []
        }
      },
      specialistResults: {
        totalCount: 0,
        items: []
      },
      generalResults: {
        totalCount: 0,
        items: []
      }
    };

    this.currentSpecialPage = 0;
    this.currentGeneralPage = 0;
    this.$ajaxSpecialResults = $.ajax();
    this.$ajaxGeneralResults = $.ajax();
    this.queryString = queryStringApp.parse(location.search); // params in the url
    this.countLabels = ["library specialists","library staff"];
    this.emptyMessage = "Loading...";
  }

  componentDidMount() {
    // show the correct view
    if(this.queryString.active_filter) {
      this.handleViewChange(this.queryString.active_filter);
    }
    // fetch all expertise
    const getExpertise = this.fetchExpertise();
    // fetch all languages
    const getLanguages = this.fetchLanguages();
    // fetch all departments
    const getDepartments = this.fetchDepartments();
    // fetch all roles
    const getRoles = this.fetchRoles();
    // fetch all Libraries
    const getLibraries = this.fetchLibraries().then(data => {    
      const filters = data.map(library => {
        return {
          label: library.name,
          value: library.id
        };
      });
      
      return {
        filters,
        libraries: data
      };
    });

    Promise
    .all([getExpertise,getLanguages,getDepartments,getRoles,getLibraries])
    .then(values => {
      let filters = Object.assign({}, this.state.filters);

      filters.subject.items = filters.subject.items.concat(values[0]);
      filters.languages.items = filters.languages.items.concat(values[1]);
      filters.department.items = filters.department.items.concat(values[2]);
      filters.role.items = filters.role.items.concat(values[3]);
      filters.library.items = filters.library.items.concat(values[4].filters);

      // mark filters found in the URL 
      filters = this.initializeFilters(filters);

      // update state and render filters
      this.setState({filters, libraries: values[4].buildings});

      // fetch results
      this.fetchResults();

      this.emptyMessage = "No Results";
    });
  }

  /********** Data Fetching ***************/

  // returns an array of properly formated filters
  fetchExpertise = () => {
    return fetch(`${ this.props.requestUrl }/api/v1/taxonomy/subject?_format=json`)
    .then(response => response.json())
    .then(data => {
      return data.map(d => {
        return {
          label: d.label,
          value: d.tid
        };
      });
    })      
    .catch(error => {
      console.warn("failed to fetch expertise:", error);
      return [];
    });
  }
  // returns an array of properly formated filters
  fetchLanguages = () => {
    return fetch(`${ this.props.requestUrl }/api/v1/taxonomy/language?_format=json`)
    .then(response => response.json())
    .then(data => {
      return data.map(d => {
        return {
          label: d.label,
          value: d.tid
        };
      });
    })      
    .catch(error => {
      console.warn("failed to fetch languages:", error);
      return [];
    });
  }
  // returns an array of properly formated filters
  fetchDepartments = () => {
    // departments are coming later
    return fetch(`${ this.props.requestUrl }/api/v1/taxonomy/department?_format=json`)
    .then(response => response.json())
    .then(data => {
      return data.map(d => {
        return {
          label: d.label,
          value: d.tid
        };
      });
    })      
    .catch(error => {
      console.warn("failed to fetch departments:", error);
      return [];
    });
  }
  // returns an array of properly formated filters
  fetchRoles = () => {
    return fetch(`${ this.props.requestUrl }/api/v1/taxonomy/role?_format=json`)
    .then(response => response.json())
    .then(data => {
      return data.map(d => {
        return {
          label: d.label,
          value: d.tid
        };
      });
    })      
    .catch(error => {
      console.warn("failed to fetch Roles:", error);
      return [];
    });
  }
  // returns an array of properly formated filters
  fetchLibraries = () => {
    return fetch(`${ this.props.requestUrl }/api/v1/library?_format=json`)
    .then(response => response.json())
    .then(data => data)      
    .catch(error => {
      console.warn("failed to fetch libraries:", error);
      return [];
    });
  }

  // fetches results from the api based on the filter values passed
  fetchResults(loadMore = false) {
    this.fetchGeneralResults(loadMore);
    this.fetchSpecialResults(loadMore);
  }

  // optional loadMore boolean is used to append or replace the existing results
  fetchSpecialResults = (loadMore = false) => {   
    if(loadMore) {
      this.currentSpecialPage++;
    } else {
      this.currentSpecialPage = 0;
    }

    let activeFilters = Object.assign({
      page: this.currentSpecialPage,
      _format: "json",
      is_specialist: 1
    }, this.queryString);

    this.$ajaxSpecialResults.abort();

    this.$ajaxSpecialResults = $.ajax({
      url: `${ this.props.requestUrl }/api/v1/staff`,
      // cache: false,
      data: activeFilters
    });

    this.$ajaxSpecialResults.done(data => {
      const currentResults = this.state.specialistResults;
      let newResults = {
        totalCount: data.results.totalCount || 0,
        items: []
      };

      if (typeof data.results.items === "undefined" || data.results.items.length === 0) {
        
        if(!loadMore) {
          this.setState({ specialistResults: newResults });
        }
        return;
      }

      const newItems = this.parseResultItems(data.results.items,true);

      if(loadMore) {
        newResults.items = currentResults.items.concat(newItems);
      } else {
        newResults.items = newItems;
      }

      this.setState({ specialistResults: newResults });
    }).fail((xhr, status, error)=> {
      console.warn("failed to fetch special results");
      
      let { specialistResults } = this.state;
      specialistResults.totalCount = 0;
      specialistResults.items = [];

      this.setState({ specialistResults });
    });
  }
  // optional loadMore boolean is used to append or replace the existing results
  fetchGeneralResults = (loadMore = false) => {   
    if(loadMore) {
      this.currentGeneralPage++;
    } else {
      this.currentGeneralPage = 0;
    }

    let activeFilters = Object.assign({
      page: this.currentGeneralPage,
      _format: "json"
    }, this.queryString);

    this.$ajaxGeneralResults.abort();

    // The field of expertise filter should search both primary and additional expertise fields
    // So add the field of expertise data to a new additional expertise array
    if (activeFilters.hasOwnProperty("area_of_expertise")) {
      activeFilters.additional_expertise = new Array();
      if (Array.isArray(activeFilters.area_of_expertise)) {
        Array.from(activeFilters.area_of_expertise).forEach(function (verf) {
          activeFilters.additional_expertise.push(verf);
        });
      }
      else {
        activeFilters.additional_expertise.push(activeFilters.area_of_expertise);
      }
    }

    this.$ajaxGeneralResults = $.ajax({
      url: `${ this.props.requestUrl }/api/v1/staff`,
      // cache: false,
      data: activeFilters
    });

    this.$ajaxGeneralResults.done(data => {
      const currentResults = this.state.generalResults;
      let newResults = {
        totalCount: data.results.totalCount || 0,
        items: []
      };
      if (typeof data.results.items === "undefined" || data.results.items.length === 0) {
        
        if(!loadMore) {
          this.setState({ generalResults: newResults });
        }
        return;
      }

      const newItems = this.parseResultItems(data.results.items);

      if(loadMore) {
        newResults.items = currentResults.items.concat(newItems);
      } else {
        newResults.items = newItems;
      }

      this.setState({ generalResults: newResults });
    }).fail((xhr, status, error)=> {
      console.warn("failed to fetch general results");
      
      let { generalResults } = this.state;
      generalResults.totalCount = 0;
      generalResults.items = [];

      this.setState({ generalResults });
    });
  }
  /********** End Data Fetching ***************/

  /********** Data Manipulation ***************/
  parseResultItems = (data, special = false) => {
    if(special) {
      return data.map((item, i) => {
        return {
          id: item.id,
          href: item.profile_link,
          firstName: item.first_name,
          lastName: item.last_name,
          img: item.image,
          jobTitle: "",
          email: item.email_link,
          phone: item.phone,
          expertise: item.area_of_expertise,
          liaison: item.is_liaison,
          role: ""
        };
      });
    } else {
      return data.map((item, i) => {
        return {
          id: item.id,
          href: item.profile_link,
          firstName: item.first_name,
          lastName: item.last_name,
          img: null,
          jobTitle: item.job_title,
          email: item.email_link,
          phone: item.phone,
          expertise: "",
          liaison: false,
          role: item.role
        };
      });
    }
  }

  initializeFilters = (filters) => {
    const params = this.queryString;

    if(params.name && filters.name.items.length) {
      filters.name.items[0].value = params.name;
    }

    if(params.area_of_expertise && filters.subject.items.length) {
      if(typeof params.area_of_expertise === "string") {
        params.area_of_expertise = [params.area_of_expertise];
      }
      filters.subject.selected = filters.subject.items.filter(a => params.area_of_expertise.some((b) => a.value === b ) );
    }

    if(params.department && filters.department.items.length) {
      if(typeof params.department === "string") {
        params.department = [params.department];
      }
      filters.department.selected = filters.department.items.filter(a => params.department.some((b) => a.value === b ) );
    }

    if(params.language && filters.languages.items.length) {
      if(typeof params.language === "string") {
        params.language = [params.language];
      }
      filters.languages.selected = filters.languages.items.filter(a => params.language.some((b) => a.value === b ) );
    }

    if(params.role && filters.role.items.length) {
      if(typeof params.role === "string") {
        params.role = [params.role];
      }
      filters.role.selected = filters.role.items.filter(a => params.role.some((b) => a.value === b ) );
    }

    if(params.library && filters.library.items.length) {
      if(typeof params.library === "string") {
        params.library = [params.library];
      }
      filters.library.selected = filters.library.items.filter(a => params.library.some((b) => a.value === b ) );
    }
    
    return filters;
  }

  // function takes two optional values 
  // the url parameter and the value it should equal
  // if neither a parameter or value are passed, 
  // we assume the queryString has been updated else where
  // and we just need to reset the url
  updateUrl = (param, value = null) => {
    // if a parameter and a value are passed add the new value
    if(param && value) {
      this.queryString[param] = value;
    }
    // if a parameter is passed with a value remove the parameter
    if(param && (value === null || value.length === 0)) {
      delete this.queryString[param];
    }

    const stringifiedQueries = queryStringApp.stringify(this.queryString);
    const newUrl = window.location.origin + window.location.pathname + "?" + stringifiedQueries;

    window.history.replaceState({path: newUrl}, "", newUrl);
  }

  /********** End Data Manipulation ***************/

  /********** Event Handlers ***************/

  handleViewChange = (index) => {
    let staffType = Object.assign({}, this.state.staffType);
    staffType.activeFilter = index;

    this.updateUrl("active_filter", index);
    this.setState({staffType});
  }

  handleLoadMoreSpecial = () => {
    this.fetchSpecialResults(true);
  }

  handleLoadMoreAll = () => {
    this.fetchGeneralResults(true);
  }

  onKeywordChange = (event) => {
    let filters = Object.assign({}, this.state.filters);
    let value = event.target.value;

    filters.name.items[0].value = value;

    this.updateUrl(filters.name.param, value);
    this.setState({ filters });

    if(value.length > 1 || value.length === 0) {
      // fetch results
      this.fetchResults();
    }   
  }

  onSelectChange = (filterName, value) => {
    let filters = Object.assign({}, this.state.filters);

    for (var filterObj in filters) {
      if(filterName === filters[filterObj].param) {
        filters[filterObj].selected = value;

        this.updateUrl(filterName, value.map(v => v.value));
        break;
      }
    }

    this.setState({ filters });

    // fetch results
    this.fetchResults();   
  }

  // when an filter accordion expands or collapses
  // receives the key value of the filter and a boolean state
  onAccordionChange = (filterName, value) => {
    let filters = Object.assign({}, this.state.filters);

    for (var filterObj in filters) {
      if(filterName === filters[filterObj].param) {
        filters[filterObj].expanded = value;
        break;
      }
    }

    this.setState({ filters });   
  }

  // when the clear all button is clicked
  // receives the event from the button
  onClearAll = (event) => {
    event.preventDefault();
    let oldFilters = this.state.filters;

    const filters = R.map((filter) => {
      if(filter.selected) {
        filter.selected = [];
      }
      return filter;
    },oldFilters);

    filters.name.items[0].value = "";

    // clear the parameters in the url
    this.queryString = {};
    this.updateUrl();

    this.fetchResults();

    this.setState({filters});
  }

  // When the user clicks the >> or << button to show or hide the filters
  // receives the desired state as a boolean
  // true == hidden
  // false == visible
  onHideFilters = (value) => {
    this.setState({ hideFilters: value });     
    
    if(this.state.showDetails) {
      this.onHideDetail();
    }
  }


  /********** End Event Handlers ***************/


  render() {
    const { assetsUrl } = this.props;
    const { filters, specialistResults, generalResults, hideFilters, staffType } = this.state;
    const button = {
      text: "Load More",
      theme: "secondary"
    };

    const gridModifier = hideFilters 
        ? "hl__filter-directory__container--no-filters" 
        : " ";
    const viewModifier = hideFilters 
        ? "hl__filter-directory__views-container--no-filters" 
        : " ";

    const filterArray = [
      filters.name,
      filters.subject,
      filters.languages,
      filters.department,
      filters.role,
      filters.library
    ];

    const activeFilter = staffType.activeFilter;
    const currentResults = activeFilter === "0" ? specialistResults : generalResults;
    const currentLoadMore = activeFilter === "0" ? this.handleLoadMoreSpecial : this.handleLoadMoreAll;

    return(
      <div className="hl__filter-directory__app">
        <section className={ `hl__filter-directory__views ${ viewModifier }` }>
          <div className={ `hl__filter-directory__views-container ${ viewModifier }` }>
            <FilterByType
              onChange= { this.handleViewChange }
              { ...staffType } />
          </div>
        </section>
        <div className={`hl__filter-directory__container ${ gridModifier }` }>
          <section className="hl__filter-directory__filters">
            <FilterRail 
              onHideFilters={ this.onHideFilters }
              onClearAll={ this.onClearAll }
              assetsUrl={ assetsUrl } 
              title="Filter Staff"
              hideFilters={ hideFilters }
              groups= { filterArray } />
          </section>

          <section className="hl__filter-directory__results">
            <div className="hl__filter-directory__result-details">
              Showing <b>{ currentResults.items.length }</b> of <b>{ currentResults.totalCount }</b> { this.countLabels[activeFilter] }
            </div>

            { currentResults.items.length > 0 && (
              <StaffGrid 
                assetsUrl={ assetsUrl }
                genericView={ activeFilter === "1" }
                results={ currentResults.items } />
            )}
            { currentResults.items.length === 0 && (
              <span className="hl__filter-directory__no-results">{ this.emptyMessage }</span>
            )}
            { currentResults.items && currentResults.items.length < currentResults.totalCount && (
              <div className="hl__filter-directory__load-more">
                <Button 
                  handleClick = { currentLoadMore }
                  { ...button } />
              </div>
            )}
          </section>
        </div>
      </div>
    );
  }
}
