import $                      from "jquery";
import React, { Component }   from "react";
import R                      from "ramda";
import queryStringApp         from "query-string";

import FilterBar              from "../../02-molecules/forms/filter-bar.jsx";
import StaffGrid              from "../../03-organisms/lists/staff-grid.jsx";
import Button                 from "../../01-atoms/buttons/button.jsx";
import { StickyContainer, Sticky } from 'react-sticky';

import { string } from "prop-types";

export default class StaffDirectoryApp extends Component {
  static propTypes = {
    requestUrl: string.isRequired,
    assetsUrl: string.isRequired
  }

  constructor(props) {
    super(props);

    this.state = {
      filters: {
        name: {
          label: "Filter Staff",
          placeholder: "Try last name or first name",
          param: "name",
          expanded: true,
          hideAccordion: true,
          filterType: "keyword",
          onChange: this.onKeywordChange,
          items:[{
            value: ""
          }]
        },
        languages: {
          label: "Languages",
          placeholder: "Language",
          param: "language",
          expanded: true,
          hideAccordion: true,
          filterType: "select",
          onChange: this.onSelectChange,
          selected: [],
          items: []
        },
        department: {
          label: "Affiliation",
          placeholder: "Affiliation",
          param: "department",
          expanded: true,
          hideAccordion: true,
          filterType: "select",
          onChange: this.onSelectChange,
          selected: [],
          items: []
        },
        role: {
          label: "Role",
          placeholder: "Role",
          param: "role",
          expanded: true,
          hideAccordion: true,
          filterType: "select",
          onChange: this.onSelectChange,
          selected: [],
          items: []
        },
        library: {
          label: "Library",
          placeholder: "Library",
          param: "library",
          expanded: true,
          hideAccordion: true,
          filterType: "select",
          onChange: this.onSelectChange,
          selected: [],
          items: []
        }
      },
      generalResults: {
        totalCount: 0,
        items: []
      }
    };

    this.currentGeneralPage = 0;
    this.$ajaxGeneralResults = $.ajax();
    this.queryString = queryStringApp.parse(location.search); // params in the url
    this.emptyMessage = "Loading...";
  }

  componentDidMount() {
    // fetch all languages
    const getLanguages = this.fetchLanguages();
    // fetch all departments
    const getDepartments = this.fetchDepartments();
    // fetch all roles
    const getRoles = this.fetchRoles();
    // fetch all Libraries
    const getLibraries = this.fetchLibraries().then(data => {
      const filters = data.map(library => {
        return {
          label: library.name,
          value: library.id
        };
      });

      return {
        filters,
        libraries: data
      };
    });

    Promise
    .all([getLanguages,getDepartments,getRoles,getLibraries])
    .then(values => {
      let filters = Object.assign({}, this.state.filters);

      filters.languages.items = filters.languages.items.concat(values[0]);
      filters.department.items = filters.department.items.concat(values[1]);
      filters.role.items = filters.role.items.concat(values[2]);
      filters.library.items = filters.library.items.concat(values[3].filters);

      // mark filters found in the URL
      filters = this.initializeFilters(filters);

      // update state and render filters
      this.setState({filters, libraries: values[3].buildings});

      // fetch results
      this.fetchResults();

      this.emptyMessage = "No Results";
    });
  }

  /********** Data Fetching ***************/

  // returns an array of properly formated filters
  fetchLanguages = () => {
    return fetch(`${ this.props.requestUrl }/api/v1/taxonomy/language?_format=json`)
    .then(response => response.json())
    .then(data => {
      return data.map(d => {
        return {
          label: d.label,
          value: d.tid
        };
      });
    })
    .catch(error => {
      console.warn("failed to fetch languages:", error);
      return [];
    });
  }
  // returns an array of properly formated filters
  fetchDepartments = () => {
    // departments are coming later
    return fetch(`${ this.props.requestUrl }/api/v1/taxonomy/department?_format=json`)
    .then(response => response.json())
    .then(data => {
      return data.map(d => {
        return {
          label: d.label,
          value: d.tid
        };
      });
    })
    .catch(error => {
      console.warn("failed to fetch departments:", error);
      return [];
    });
  }
  // returns an array of properly formated filters
  fetchRoles = () => {
    return fetch(`${ this.props.requestUrl }/api/v1/taxonomy/role?_format=json`)
    .then(response => response.json())
    .then(data => {
      return data.map(d => {
        return {
          label: d.label,
          value: d.tid
        };
      });
    })
    .catch(error => {
      console.warn("failed to fetch Roles:", error);
      return [];
    });
  }
  // returns an array of properly formated filters
  fetchLibraries = () => {
    return fetch(`${ this.props.requestUrl }/api/v1/library?_format=json`)
    .then(response => response.json())
    .then(data => data)
    .catch(error => {
      console.warn("failed to fetch libraries:", error);
      return [];
    });
  }

  // fetches results from the api based on the filter values passed
  fetchResults(loadMore = false) {
    this.fetchGeneralResults(loadMore);
  }

  // optional loadMore boolean is used to append or replace the existing results
  fetchGeneralResults = (loadMore = false) => {
    if(loadMore) {
      this.currentGeneralPage++;
    } else {
      this.currentGeneralPage = 0;
    }

    let activeFilters = Object.assign({
      page: this.currentGeneralPage,
      _format: "json"
    }, this.queryString);

    this.$ajaxGeneralResults.abort();

    this.$ajaxGeneralResults = $.ajax({
      url: `${ this.props.requestUrl }/api/v1/staff`,
      data: activeFilters
    });

    this.$ajaxGeneralResults.done(data => {
      const currentResults = this.state.generalResults;
      let newResults = {
        totalCount: data.results.totalCount || 0,
        items: []
      };
      if (typeof data.results.items === "undefined" || data.results.items.length === 0) {

        if(!loadMore) {
          this.setState({ generalResults: newResults });
        }
        return;
      }

      const newItems = this.parseResultItems(data.results.items);

      if(loadMore) {
        newResults.items = currentResults.items.concat(newItems);
      } else {
        newResults.items = newItems;
      }

      this.setState({ generalResults: newResults });
    }).fail((xhr, status, error)=> {
      console.warn("failed to fetch general results");

      let { generalResults } = this.state;
      generalResults.totalCount = 0;
      generalResults.items = [];

      this.setState({ generalResults });
    });
  }
  /********** End Data Fetching ***************/

  /********** Data Manipulation ***************/
  parseResultItems = (data) => {
    return data.map((item, i) => {
      return {
        id: item.id,
        href: item.profile_link,
        firstName: item.first_name,
        lastName: item.last_name,
        img: item.image,
        jobTitle: item.job_title,
        email: item.email_link,
        role: item.role
      };
    });
  }

  initializeFilters = (filters) => {
    const params = this.queryString;

    if(params.name && filters.name.items.length) {
      filters.name.items[0].value = params.name;
    }

    if(params.department && filters.department.items.length) {
      if(typeof params.department === "string") {
        params.department = [params.department];
      }
      filters.department.selected = filters.department.items.filter(a => params.department.some((b) => a.value === b ) );
    }

    if(params.language && filters.languages.items.length) {
      if(typeof params.language === "string") {
        params.language = [params.language];
      }
      filters.languages.selected = filters.languages.items.filter(a => params.language.some((b) => a.value === b ) );
    }

    if(params.role && filters.role.items.length) {
      if(typeof params.role === "string") {
        params.role = [params.role];
      }
      filters.role.selected = filters.role.items.filter(a => params.role.some((b) => a.value === b ) );
    }

    if(params.library && filters.library.items.length) {
      if(typeof params.library === "string") {
        params.library = [params.library];
      }
      filters.library.selected = filters.library.items.filter(a => params.library.some((b) => a.value === b ) );
    }

    return filters;
  }

  // function takes two optional values
  // the url parameter and the value it should equal
  // if neither a parameter or value are passed,
  // we assume the queryString has been updated elsewhere
  // and we just need to reset the url
  updateUrl = (param, value = null) => {
    // if a parameter and a value are passed add the new value
    if(param && value) {
      this.queryString[param] = value;
    }
    // if a parameter is passed with a value remove the parameter
    if(param && (value === null || value.length === 0)) {
      delete this.queryString[param];
    }

    const stringifiedQueries = queryStringApp.stringify(this.queryString);
    const newUrl = window.location.origin + window.location.pathname + "?" + stringifiedQueries;

    window.history.replaceState({path: newUrl}, "", newUrl);
  }

  /********** End Data Manipulation ***************/

  /********** Event Handlers ***************/

  handleLoadMoreAll = () => {
    this.fetchGeneralResults(true);
  }

  onKeywordChange = (event) => {
    let filters = Object.assign({}, this.state.filters);
    let value = event.target.value;

    filters.name.items[0].value = value;

    this.updateUrl(filters.name.param, value);
    this.setState({ filters });

    if(value.length > 1 || value.length === 0) {
      // fetch results
      this.fetchResults();
    }
  }

  onSelectChange = (filterName, value) => {
    let filters = Object.assign({}, this.state.filters);

    for (var filterObj in filters) {
      if(filterName === filters[filterObj].param) {
        filters[filterObj].selected = value;

        this.updateUrl(filterName, value.map(v => v.value));
        break;
      }
    }

    this.setState({ filters });

    // fetch results
    this.fetchResults();
  }

  // when an filter accordion expands or collapses
  // receives the key value of the filter and a boolean state
  onAccordionChange = (filterName, value) => {
    let filters = Object.assign({}, this.state.filters);

    for (var filterObj in filters) {
      if(filterName === filters[filterObj].param) {
        filters[filterObj].expanded = value;
        break;
      }
    }

    this.setState({ filters });
  }

  // when the clear all button is clicked
  // receives the event from the button
  onClearAll = (event) => {
    event.preventDefault();
    let oldFilters = this.state.filters;

    const filters = R.map((filter) => {
      if(filter.selected) {
        filter.selected = [];
      }
      return filter;
    },oldFilters);

    filters.name.items[0].value = "";

    // clear the parameters in the url
    this.queryString = {};
    this.updateUrl();

    this.fetchResults();

    this.setState({filters});
  }


  /********** End Event Handlers ***************/


  render() {
    const { assetsUrl } = this.props;
    const { filters, generalResults } = this.state;
    const button = {
      text: "Load More",
      theme: "secondary"
    };

    const filterArray = [
      filters.name,
      filters.department,
      filters.library,
      filters.role,
      filters.languages
    ];

    const currentResults = generalResults;
    const currentLoadMore = this.handleLoadMoreAll;

    return(
      <StickyContainer>
      <div className="hl__filter-bar-directory__app">
          <Sticky topOffset={-53}>
            {({ style, isSticky }) =>
              <section style={{ ...style, marginTop: isSticky ? '53px' : '0px' }} className="hl__filter-bar-directory__filters"
                data-sticky={ isSticky ? 'sticky' : 'static' }>
              <FilterBar
              onClearAll={ this.onClearAll }
              assetsUrl={ assetsUrl }
              title="Filter Staff"
              groups= { filterArray } />
              </section>
            }
          </Sticky>
        <div className="hl__filter-bar-directory__container">
          <section className="hl__filter-bar-directory__results">
            { currentResults.items.length > 0 && (
              <StaffGrid
                assetsUrl={ assetsUrl }
                results={ currentResults.items } />
            )}
            { currentResults.items.length === 0 && (
              <span className="hl__filter-bar-directory__no-results">{ this.emptyMessage }</span>
            )}
            { currentResults.items && currentResults.items.length < currentResults.totalCount && (
              <div className="hl__filter-bar-directory__load-more">
                <Button
                  handleClick = { currentLoadMore }
                  { ...button } />
              </div>
            )}
          </section>
        </div>
      </div>
      </StickyContainer>
    );
  }
}
