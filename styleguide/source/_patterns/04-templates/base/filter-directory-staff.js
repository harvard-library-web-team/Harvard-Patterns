import React     from "react";
import {render}  from "react-dom";
import App       from "./filter-directory-staff-app.jsx";

const el = document.querySelector(".js-staff-directory-app");

if(el) {
  const props = {
    requestUrl: el.getAttribute("data-request-url"),
    assetsUrl: el.getAttribute("data-assets-url")
  };

  render(
    <App { ...props} />,
    el
  );
}
