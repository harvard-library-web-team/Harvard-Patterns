import $                      from "jquery";
import React, { Component }   from "react";
import R                      from "ramda";
import queryStringApp         from "query-string";

import NewsGrid               from "../../03-organisms/lists/news-grid.jsx";
import Button                 from "../../01-atoms/buttons/button.jsx";
import FilterBar              from "../../02-molecules/forms/filter-bar.jsx";
import { StickyContainer, Sticky } from 'react-sticky';

import { string } from "prop-types";

export default class NewsListingApp extends Component {
  static propTypes = {
    requestUrl: string.isRequired,
    assetsUrl: string.isRequired
  }

  constructor(props) {
    super(props);

    this.state = {
      filters: {
        keywords: {
          label: "Search",
          placeholder: "Try 'keyword'",
          param: "keywords",
          expanded: true,
          hideAccordion: true,
          filterType: "keyword",
          onChange: this.handleKeywordChange,
          items:[{
            value: ""
          }]
        },
        library: {
          label: "Library",
          placeholder: "Library",
          param: "library_id",
          expanded: true,
          hideAccordion: true,
          filterType: "select",
          onChange: this.onSelectChange,
          selected: [],
          items: []
        }
      },
      results: {
        totalCount: 0,
        items: []
      }
    };

    this.page = 0;
    this.$ajaxResults = $.ajax();
    this.queryString = queryStringApp.parse(location.search); // params in the url
    this.emptyMessage = "Loading...";
    this.keywordDebounce = null;
  }

  componentDidMount() {

    // fetch all Libraries
    const getLibraries = this.fetchLibraries().then(data => {
      const filters = data.map(library => {
        return {
          label: library.name,
          value: library.id
        };
      });

      return {
        filters,
        libraries: data
      };
    });

    Promise
      .all([getLibraries])
      .then(values => {
        let filters = Object.assign({}, this.state.filters);

        filters.library.items = filters.library.items.concat(values[0].filters);

        // mark filters found in the URL
        filters = this.initializeFilters(filters);

        // update state and render filters
        this.setState({filters});

        // fetch results
        this.fetchResults();

        this.emptyMessage = "No Results";
      });
  }

  /********** Data Fetching ***************/

  // returns an array of properly formated filters
  fetchLibraries = () => {
    return fetch(`${ this.props.requestUrl }/api/v1/library?_format=json`)
    .then(response => response.json())
    .then(data => data)
    .catch(error => {
      console.warn("failed to fetch libraries:", error);
      return [];
    });
  }


  fetchResults = (loadMore = false) => {
    if(loadMore) {
      this.page++;
    } else {
      this.page = 0;
    }

    let activeFilters = Object.assign({
      page: this.page,
      _format: "json"
    }, this.queryString);

    this.$ajaxResults.abort();

    this.$ajaxResults = $.ajax({
      url: `${ this.props.requestUrl }/api/v1/news`,
      // cache: false,
      data: activeFilters
    });

    this.$ajaxResults.done(data => {
      const currentResults = this.state.results;
      let newResults = {
        totalCount: data.results.totalCount || 0,
        items: []
      };

      if (typeof data.results.items === "undefined" || data.results.items.length === 0) {

        if(!loadMore) {
          this.setState({ results: newResults });
        }
        return;
      }

      const newItems = this.parseResultItems(data.results.items);

      if(loadMore) {
        newResults.items = currentResults.items.concat(newItems);
      } else {
        newResults.items = newItems;
      }

      this.setState({ results: newResults });
    }).fail((xhr, status, error)=> {
      console.warn("failed to fetch results");

      const results = {
        totalCount: 0,
        items: []
      };

      this.setState({ results });
    });
  }
  /********** End Data Fetching ***************/

  /********** Data Manipulation ***************/
  parseResultItems = (data) => {
    const { filters } = this.state;

    return data.map((story, i) => {
      if (story.story_type == 'News Mention') {
        var href = story.external_link;
        var href_target = "_blank";
      }
      else {
        var href = story.url;
        var href_target = "";
      }
       return {
        id: story.id || `${i}`,
        index: `${i}`,
        title: story.title,
        type: story.story_type,
        subText: story.summary || "",
        date: story.date,
        href: href,
        href_target: href_target,
        story_author: story.story_author,
        story_subject: story.story_subject,
        story_source: story.story_source,
        info: `See details about "${ story.title }"`
      };
    });
  }

  initializeFilters = (filters) => {
    const params = this.queryString;

    if(params.keywords && filters.keywords.items.length) {
      filters.keywords.items[0].value = params.keywords;
    }

    if(params.library_id && filters.library.items.length) {
      if(typeof params.library_id === "string") {
        params.library_id = [params.library_id];
      }
      filters.library.selected = filters.library.items.filter(a => params.library_id.some((b) => a.value === b ) );
    }

    return filters;
  }

  // function takes two optional values
  // the url parameter and the value it should equal
  // if neither a parameter or value are passed,
  // we assume the queryString has been updated elsewhere
  // and we just need to reset the url
  updateUrl = (param, value = null) => {
    // if a parameter and a value are passed add the new value
    if(param && value) {
      this.queryString[param] = value;
    }
    // if a parameter is passed with a value remove the parameter
    if(param && (value === null || value.length === 0)) {
      delete this.queryString[param];
    }

    const stringifiedQueries = queryStringApp.stringify(this.queryString);
    const newUrl = window.location.origin + window.location.pathname + "?" + stringifiedQueries;

    window.history.replaceState({path: newUrl}, "", newUrl);
  }
  /********** End Data Manipulation ***************/

  /********** Event Handlers ***************/

  // when search terms are changed
  handleKeywordChange = (event) => {
    let filters = Object.assign({}, this.state.filters);
    let value = event.target.value;

    filters.keywords.items[0].value = value;

    this.updateUrl(filters.keywords.param, value);
    this.setState({ filters });

    if(value.length > 1 || value.length === 0) {
      // fetch results
      this.fetchResults();
    }
  }

  handleLoadMore = () => {
    this.fetchResults(true);
  }

  onSelectChange = (filterName, value) => {
    let filters = Object.assign({}, this.state.filters);

    for (var filterObj in filters) {
      if(filterName === filters[filterObj].param) {
        filters[filterObj].selected = value;

        this.updateUrl(filterName, value.map(v => v.value));
        break;
      }
    }

    this.setState({ filters });

    // fetch results
    this.fetchResults();
  }

  // when the clear all button is clicked
  // receives the event from the button
  onClearAll = (event) => {
    event.preventDefault();
    let oldFilters = this.state.filters;

    const filters = R.map((filter) => {
      if(filter.selected) {
        filter.selected = [];
      }
      if (filter.filterType === 'checkbox') {
        for (let item of filter.items){
          if(item.checked) {
            item.checked = false;
          }
        }
      }
      return filter;
    },oldFilters);

    filters.keywords.items[0].value = "";

    // clear the parameters in the url
    this.queryString = {};
    this.updateUrl();

    this.fetchResults();

    this.setState({filters});
  }

  /********** End Event Handlers ***************/

  render() {
    const { assetsUrl } = this.props;
    const { filters, results } = this.state;
    const button = {
      text: "Load More",
      theme: "secondary"
    };

    const filterArray = [
      filters.keywords,
      filters.library
    ];

    return(
      <StickyContainer>
      <div className="hl__filter-directory__app">
        <Sticky topOffset={-53}>
          {({ style, isSticky }) =>
            <section style={{ ...style, marginTop: isSticky ? '53px' : '0px' }} className="hl__filter-bar-directory__filters"
              data-sticky={ isSticky ? 'sticky' : 'static' }>
            <FilterBar
              onClearAll={ this.onClearAll }
              assetsUrl={ assetsUrl }
              title="Search"
              groups= { filterArray }
              theme="subtle" />
            </section>
          }
        </Sticky>
        <section className="hl__filter-directory__results">
          { results.items.length > 0 && (
            <NewsGrid events={ results.items } />
          )}
          { results.items.length === 0 && (
            <span className="hl__filter-directory__no-results">{ this.emptyMessage }</span>
          )}
          { results.items && results.items.length < results.totalCount && (
            <div className="hl__filter-directory__load-more">
              <Button
                handleClick = { this.handleLoadMore }
                { ...button } />
            </div>
          )}
        </section>
      </div>
      </StickyContainer>
    );
  }
}
