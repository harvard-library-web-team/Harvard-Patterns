import $                      from "jquery";
import React, { Component }   from "react";
import R                      from "ramda";
import queryStringApp         from "query-string";
import currentDate            from "../../../js/helpers/currentDate.js";
import moment                 from "moment";
const { DateTime } = require("luxon");

import FilterBar             from "../../02-molecules/forms/filter-bar.jsx";

import WeekPicker             from "../../02-molecules/forms/week-picker.jsx";

import FilterToggles        from "../../02-molecules/forms/filter-toggles.jsx";

import LibraryResultsGrid     from "../../03-organisms/lists/library-grid.jsx";
import LibraryHoursList       from "../../03-organisms/lists/library-hours-list.jsx";
import { StickyContainer, Sticky } from 'react-sticky';

import { string } from "prop-types";

export default class LibraryDirectoryApp extends Component {
  static propTypes = {
    requestUrl: string.isRequired,
    assetsUrl: string.isRequired
  }

  constructor(props) {
    super(props);

    this.todaysDay = moment().day("Sunday");
    this.todaysWeek = moment().day("Sunday").isoWeek();
    this.queryString = queryStringApp.parse(location.search); // params in the url
    this.$ajaxLibraryResults = $.ajax();
    this.$ajaxLibraryHours = $.ajax();
    this.emptyMessage = "Loading...";

    this.queryString.weeks = 1;

    this.state = {
      loading: true,
      libraryView: {
        // label: "Show Me:",
        param: "view",
        activeFilter: "1",
        items: [{
          name: "show results in a list view",
          value: "1",
          label: "Hours View",
          icon: "single",
          checked: true
        },{
          name: "show results in a card view",
          value: "0",
          label: "Library View",
          icon: "grid",
          checked: false
        }]
      },
      filters: {
        name: {
          label: "Search",
          placeholder: "Try a library name or keyword",
          param: "library",
          expanded: true,
          hideAccordion: true,
          filterType: "keyword",
          onChange: this.onKeywordChange,
          items:[{
            value: this.queryString.library || ""
          }]
        },
        selectedDate: {
          label: "Week Of",
          placeholder: "",
          param: "date",
          expanded: true,
          hideAccordion: true,
          filterType: "weekPicker",
          onChange: this.onDateChange,
          week: 0,
          items: [{
            date: moment().day("Sunday"),
            focused: false,
            onFocusChange: this.onFocusChange,
            properties: {
              numberOfMonths: 1,
              displayFormat: "MMM DD, YYYY"
            }
          }]
        }
      },
      libraryResults: {
        totalCount: 0,
        items: []
      }
    };
  }

  componentDidMount() {
    // show the correct view
    if(this.queryString.active_filter) {
      this.handleViewChange(this.queryString.active_filter);
    }

    // fetch all Libraries
    Promise
    .all([])
    .then(values => {
      let filters = Object.assign({}, this.state.filters);

      // mark filters found in the URL
      filters = this.initializeFilters(filters);

      // update state and render filters
      this.setState({filters});

      // fetch results
      this.fetchResults();

      this.emptyMessage = "No Results";
    });
  }

  /********** Data Fetching ***************/

  // fetches results from the api based on the filter values passed
  fetchResults = () => {
    console.log('fetchResults queryString...');
    console.log(this.queryString);
    let activeFilters = Object.assign({
      _format: "json"
    }, this.queryString);

    this.$ajaxLibraryResults.abort();
    this.emptyMessage = "Loading...";

    // hide existing results while loading
    const libraryResults = {
      totalCount: 0,
      items: []
    };

    this.setState({ libraryResults });

    this.$ajaxLibraryResults = $.ajax({
      url: `${ this.props.requestUrl }/api/v1/library`,
      // cache: false,
      data: activeFilters
    });

    this.$ajaxLibraryResults.done(data => {
      const libraryResults = {
        items: []
      };

      if (typeof data === "undefined" || data.length === 0) {
        this.setState({ libraryResults });
        this.emptyMessage = "No Results";
        return;
      }

      libraryResults.items = this.parseResultItems(data);

      this.setState({ libraryResults, loading: false });

    }).fail((xhr, status, error)=> {
      console.warn("failed to fetch library results");

      const libraryResults = {
        totalCount: 0,
        items: []
      };

      this.emptyMessage = "Loading Error";

      this.setState({ libraryResults, loading: false });
    });
  }
  /********** End Data Fetching ***************/

  /********** Data Manipulation ***************/

  // data is a library object array
  parseResultItems = (data) => {
    return data.map(library => {
      const weeks_hours = library.weeks_hours.locations && library.weeks_hours.locations.length ? this.parseLibraryHours(library.weeks_hours.locations[0]) : null;

      return Object.assign({}, library, { weeks_hours });
    });
  }

  parseLibraryHours = (data) => {
    const weekIndex = data.weeks && data.weeks.length ? data.weeks.length - 1 : 0;

    return Object.assign({}, data, {
      department_hours: data.department_hours && Array.isArray(data.department_hours)
      ? data.department_hours.map(department => {
        const deptIndex = department.weeks.length - 1;

        return Object.assign({}, department, {
          weeks: department.weeks && department.weeks.length
            ? [
              department.weeks[deptIndex].Sunday,
              department.weeks[deptIndex].Monday,
              department.weeks[deptIndex].Tuesday,
              department.weeks[deptIndex].Wednesday,
              department.weeks[deptIndex].Thursday,
              department.weeks[deptIndex].Friday,
              department.weeks[deptIndex].Saturday
            ]
            : []
        });
      })
      : [],
      weeks: data.weeks.length
      ? [
        data.weeks[weekIndex].Sunday,
        data.weeks[weekIndex].Monday,
        data.weeks[weekIndex].Tuesday,
        data.weeks[weekIndex].Wednesday,
        data.weeks[weekIndex].Thursday,
        data.weeks[weekIndex].Friday,
        data.weeks[weekIndex].Saturday
      ]
      : []
    });
  }

  initializeFilters = (filters) => {
    const params = this.queryString;

    return filters;
  }

  // function takes two optional values
  // the url parameter and the value it should equal
  // if neither a parameter or value are passed,
  // we assume the queryString has been updated else where
  // and we just need to reset the url
  updateUrl = (param, value = null) => {
    // if a parameter and a value are passed add the new value
    if(param && value) {
      this.queryString[param] = value;
    }
    // if a parameter is passed with a value remove the parameter
    if(param && (value === null || value.length === 0)) {
      delete this.queryString[param];
    }

    const stringifiedQueries = queryStringApp.stringify(this.queryString);
    const newUrl = window.location.origin + window.location.pathname + "?" + stringifiedQueries;

    window.history.replaceState({path: newUrl}, "", newUrl);
  }

  /********** End Data Manipulation ***************/

  /********** Event Handlers ***************/

  handleViewChange = (index) => {
    let libraryView = Object.assign({}, this.state.libraryView);
    libraryView.activeFilter = index;

    this.updateUrl("active_filter", index);
    this.setState({libraryView});
  }

  handleKeywordChange = (event) => {
    // get current filters and update keyword to equal the new value
    const filters = R.assoc('keyword', event.target.value, this.state.filters);

    // clear previous keyword typed
    clearTimeout(this.keywordDebounce);

    // wait for user to stop typing before fetching results
    this.keywordDebounce = setTimeout(() => {
      this.fetchResults( filters );
    },300);

    // update the state with the current keyword
    this.setState({ filters });
  }

  // date should always be a Sunday.
  onDateChange = (date) => {
    const filters = Object.assign({}, this.state.filters);
    let dateISO = date.toISOString();
    let end = DateTime.fromISO(dateISO);
    let startOfWeek = DateTime.local().minus({ days: DateTime.local().weekday }).startOf('day');
    let diffInWeeks = end.diff(startOfWeek, 'weeks');
    let week = parseInt(diffInWeeks.weeks) + 1;

    filters.selectedDate.items[0].date = date;
    filters.selectedDate.week = week > 1 ? week : 1;

    this.queryString.weeks = week > 1 ? week : null;
    console.log(filters);
    this.setState(filters);

    // fetch results
    this.fetchResults();
  }

  onFocusChange = ({focused}) => {
    let filters = Object.assign({}, this.state.filters);

    filters.selectedDate.items[0].focused = focused;

    this.setState(filters);
  }

  onKeywordChange = (event) => {
    let filters = Object.assign({}, this.state.filters);
    let value = event.target.value;

    filters.name.items[0].value = value;

    this.updateUrl(filters.name.param, value);
    this.setState({ filters });

    if(value.length > 1 || value.length === 0) {
      // fetch results
      this.fetchResults();
    }
  }

  renderWeekPicker = () => {
    const {
      assetsUrl="../..",
      onChange,
      items } = this.props;

    return items.map((item, index) => {
      return (
        <WeekPicker
          key={ `single_date_${ index }` }
          assetsUrl={ assetsUrl }
          onDateChange={ onChange }
          { ...item } />
      );
    });
  }


  /********** End Event Handlers ***************/


  render() {
    const { assetsUrl } = this.props;
    const { filters, libraryResults, libraryView } = this.state;
    const startDate = filters.selectedDate.items[0].date;
    const endDate = moment(startDate).add(6, "days");


    const gridModifier =  "hl__filter-bar-directory__container--no-filters";
    const viewModifier = "hl__filter-bar-directory__views-container--no-filters";

    const baseFilters = [
      filters.name
    ];

    const libraryItems = libraryResults.items;

    const hoursResults = libraryItems.filter(library => {
      return library.weeks_hours !== null;
    });

    const activeFilter = libraryView.activeFilter;

    const filterArray = baseFilters;

    return(
      <div className="hl__filter-bar-directory__app">
        <section className="hl__filter-bar-directory__views">
          <div className={ `hl__filter-bar-directory__views-container ${ viewModifier }` }>
            <FilterBar
              assetsUrl={ assetsUrl }
              title="Filter"
              groups= { filterArray } />
            <FilterToggles
              onChange= { this.handleViewChange }
              { ...libraryView } />
          </div>
        </section>

        <StickyContainer>
        { activeFilter === "1" && (
          <Sticky topOffset={-53}>
            {({ style, isSticky }) =>
            <section style={{ ...style, marginTop: isSticky ? '53px' : '0px' }} className="hl__filter-bar-directory__week-picker"
              data-sticky={ isSticky ? 'sticky' : 'static' }>
            <FilterBar
              assetsUrl={ assetsUrl }
              title="Filter by Week"
              groups= { [filters.selectedDate] } />

          </section>
          }
        </Sticky>

        )}

        <div className={`hl__filter-bar-directory__container ${ gridModifier }` }>

          <section className="hl__filter-bar-directory__results" aria-live="polite" aria-atomic="true">
            <div class="hl__visually-hidden">{ libraryItems.length } results</div>

            { activeFilter === "0" && libraryItems.length > 0 && (
              <LibraryResultsGrid libraries={ libraryItems } />
            )}
            { activeFilter === "0" && libraryItems.length === 0 && (
              <span className="hl__filter-bar-directory__no-results">{ this.emptyMessage }</span>
            )}
            { activeFilter === "1" && hoursResults.length > 0 && (
              <LibraryHoursList
                libraries={ hoursResults }
                startDate={ startDate }
                week={ filters.selectedDate.week } />
            )}
            { activeFilter === "1" && hoursResults.length === 0 && (
              <span className="hl__filter-bar-directory__no-results">{ this.emptyMessage }</span>
            )}
          </section>
        </div>
      </StickyContainer>
      </div>
    );
  }
}
