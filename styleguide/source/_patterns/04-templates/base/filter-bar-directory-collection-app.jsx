import $                      from "jquery";
import React, { Component }   from "react";
import R                      from "ramda";
import queryStringApp         from "query-string";

import FilterBar             from "../../02-molecules/forms/filter-bar.jsx";
import FilterToggles        from "../../02-molecules/forms/filter-toggles.jsx";
import SortByType from "../../02-molecules/forms/sort-by-type.jsx";

import TeaserGrid             from "../../03-organisms/lists/teaser-grid.jsx";
import MasonryPromo           from "../../03-organisms/lists/masonry-promo.jsx";
import Button                 from "../../01-atoms/buttons/button.jsx";

import { string } from "prop-types";

export default class CollectionDirectoryApp extends Component {
  static propTypes = {
    requestUrl: string.isRequired,
    assetsUrl: string.isRequired
  }

  constructor(props) {
    super(props);

    this.page = 0;
    this.$ajaxResults = $.ajax();
    this.queryString = queryStringApp.parse(location.search); // params in the url
    this.emptyMessage = "Loading...";

    this.state = {
      toggleView: {
        label:"Toggle Views",
        param:"toggle",
        activeFilter:"0",
        items:[{
          name:"masonry view",
          value:"0",
          label:"masonry",
          icon:"masonry",
          checked: true
        },{
          name:"list view",
          value:"1",
          label:"list",
          icon:"list",
          checked: false
        }]
      },
      sortItems: {
        label:"Sort by type",
        activeFilter:"created",
        items:[{
          name: "sort_by",
          value: "random",
          label: "Surprise Me!",
          checked: false,
          icon: "gift"
          },{
            name: "sort_by",
            value: "created",
            label: "Newest",
            checked: true
          },{
            name: "sort_by",
            value: "title",
            label: "A-Z",
            checked: false
        }]
      },
      filters: {
        collectionName: {
          label: "Filter",
          placeholder: 'Try "Audubon" or "digital"',
          param: "keywords",
          expanded: true,
          hideAccordion: true,
          filterType: "keyword",
          onChange: this.onKeywordChange,
          items:[{
            value: this.queryString.keywords || ""
          }]
        }
      },
      results: {
        totalCount: 0,
        items: []
      }
    };
  }

  componentDidMount() {

    Promise
    .all([])
    .then(values => {
      let filters = Object.assign({}, this.state.filters);

      // mark filters found in the URL
      filters = this.initializeFilters(filters);

      // update state and render filters
      this.setState({filters});

      // fetch results
      this.fetchResults();

      this.emptyMessage = "No Results";
    });
  }

  /********** Data Fetching ***************/

  // optional loadMore boolean is used to append or replace the existing results
  fetchResults = (loadMore = false) => {
    if(loadMore) {
      this.page++;
    } else {
      this.page = 0;
    }

    let activeFilters = Object.assign({
      page: this.page,
      _format: "json"
    }, this.queryString);

    this.$ajaxResults.abort();

    this.$ajaxResults = $.ajax({
      url: `${ this.props.requestUrl }/api/v1/collections`,
      // cache: false,
      data: activeFilters
    });

    this.$ajaxResults.done(data => {
      const currentResults = this.state.results;
      let newResults = {
        totalCount: data.results.totalCount || 0,
        items: []
      };

      if (typeof data.results.items === "undefined" || data.results.items.length === 0) {

        if(!loadMore) {
          this.setState({ results: newResults });
        }
        return;
      }

      const newItems = this.parseResultItems(data.results.items,true);

      if(loadMore) {
        newResults.items = currentResults.items.concat(newItems);
      } else {
        newResults.items = newItems;
      }

      this.setState({ results: newResults });
    }).fail((xhr, status, error)=> {
      console.warn("failed to fetch results");

      const results = {
        totalCount: 0,
        items: []
      };

      this.setState({ results });
    });
  }
  /********** End Data Fetching ***************/

  /********** Data Manipulation ***************/
  parseResultItems = (data) => {
    const { filters } = this.state;

    return data.map((item, i) => {
      return {
        id: item.id || `${i}`,
        cardType: item.card_type,
        title: {
          text: item.title
        },
        collection_type: item.collection_type,
        subText: item.summary,
        subTitle: item.summary,
        description: "",
        link: {
          href: item.url,
          info: `Learn more about the '${ item.title }' collection`
        },
        card_image: item.card_image,
        image: item.list_image
      };
    });
  }

  initializeFilters = (filters) => {
    const params = this.queryString;

    if(params.collectionName && filters.collectionName.items.length) {
      filters.collectionName.items[0].value = params.collectionName;
    }

    return filters;

    function markItem(param, filterItems) {
      param.forEach(paramValue => {
        const filterIndex = filterItems.findIndex(item => {
          return item.value === paramValue;
        });
        if(filterItems[filterIndex]){
          filterItems[filterIndex].checked = true;
        }
      });

      return filterItems;
    }
  }

  // function takes two optional values
  // the url parameter and the value it should equal
  // if neither a parameter or value are passed,
  // we assume the queryString has been updated else where
  // and we just need to reset the url
  updateUrl = (param, value = null) => {
    // if a parameter and a value are passed add the new value
    if(param && value) {
      this.queryString[param] = value;
    }
    // if a parameter is passed with a value remove the parameter
    if(param && (value === null || value.length === 0)) {
      delete this.queryString[param];
    }

    const stringifiedQueries = queryStringApp.stringify(this.queryString);
    const newUrl = window.location.origin + window.location.pathname + "?" + stringifiedQueries;

    window.history.replaceState({path: newUrl}, "", newUrl);
  }

  /********** End Data Manipulation ***************/

  /********** Event Handlers ***************/

  handleLoadMore = () => {
    this.fetchResults(true);
  }

  // when search terms are changed
  onKeywordChange = (event) => {
    let filters = Object.assign({}, this.state.filters);
    let value = event.target.value;

    filters.collectionName.items[0].value = value;

    this.updateUrl(filters.collectionName.param, value);
    this.setState({ filters });

    if(value.length > 1 || value.length === 0) {
      // fetch results
      this.fetchResults();
    }
  }

  //when a toggle is clicked
  handleToggleChange = (index) => {
    let toggleView = Object.assign({}, this.state.toggleView);
    toggleView.activeFilter = index;

    this.updateUrl("active_filter", index);
    this.setState({toggleView});

    // fetch results -- this would cause a new surprise to appear when you toggle between views
    // this.fetchResults();
  }

  //when a sort filter is clicked
  handleSortChange = (index) => {
    let sortItems = Object.assign({}, this.state.sortItems);
    sortItems.activeFilter = index;

    this.updateUrl("sort_by", index);

    // if random clicked
    if (index == "random") {
      const cache = Math.floor((Math.random() * 10000) + 1);
      this.updateUrl("items_per_page", "1");
      this.updateUrl("cache", cache);
      this.updateUrl("sort_order", "");
    }

    // if newest clicked
    if (index == "created") {
      this.updateUrl("items_per_page", "");
      this.updateUrl("cache", "");
      this.updateUrl("sort_order", "DESC");
    }

    // if a-z clicked
    if (index == "title") {
      this.updateUrl("items_per_page", "");
      this.updateUrl("cache", "");
      this.updateUrl("sort_order", "ASC");
    }

    this.setState({sortItems});

    // fetch results
    this.fetchResults();
  }


  /********** End Event Handlers ***************/


  render() {
    const { assetsUrl } = this.props;
    const { filters, results, sortItems, toggleView } = this.state;

    const activeFilter = toggleView.activeFilter;
    const activeView = (activeFilter === "0") ? "hl__filter-bar-directory__views--masonry" : "hl__filter-bar-directory__views--list";

    const button = {
      text: "Load More",
      theme: "secondary"
    };

    const filterArray = [
      filters.collectionName
    ];

    return(
      <div className="hl__filter-bar-directory__app">
        <section className={`hl__filter-bar-directory__views ${activeView}`}>
          <div className="hl__filter-bar-directory__views-container">
            <FilterBar
              assetsUrl={ assetsUrl }
              title="Filter"
              groups= { filterArray } />
            <SortByType
              onChange={ this.handleSortChange }
              { ...sortItems } />
            <FilterToggles
              onChange= { this.handleToggleChange }
              { ...toggleView } />
          </div>
        </section>

        <div className="hl__filter-bar-directory__container">
          <section className="hl__filter-bar-directory__results">
            { activeFilter === "0" && results.items.length > 0 && (
              <MasonryPromo
                results={ results.items } />
            )}
            { activeFilter === "1" && results.items.length > 0 && (
              <TeaserGrid
                results={ results.items } />
            )}
            { results.items.length === 0 && (
              <span className="hl__filter-bar-directory__no-results">{ this.emptyMessage }</span>
            )}
            { results.items && results.items.length < results.totalCount && (
              <div className="hl__filter-bar-directory__load-more">
                <Button
                  handleClick = { this.handleLoadMore }
                  { ...button } />
              </div>
            )}
          </section>
        </div>
      </div>
    );
  }
}
