import $                      from "jquery";
import React, { Component }   from "react";
import R                      from "ramda";
import queryStringApp         from "query-string";
import currentDate            from "../../../js/helpers/currentDate.js";
import moment                 from "moment";

import CheckboxFilter         from "../../01-atoms/forms/checkbox-filter.jsx";
import FilterRail             from "../../02-molecules/forms/filter-rail.jsx";
import FilterByType           from "../../02-molecules/forms/filter-by-type.jsx";
import LibraryResultsGrid     from "../../03-organisms/lists/library-grid.jsx";
import LibraryHoursList       from "../../03-organisms/lists/library-hours-list.jsx";

import { string } from "prop-types";

export default class LibraryDirectoryApp extends Component {
  static propTypes = {
    requestUrl: string.isRequired,
    assetsUrl: string.isRequired
  }

  constructor(props) {
    super(props);

    this.todaysDay = moment().day("Sunday");
    this.queryString = queryStringApp.parse(location.search); // params in the url
    this.$ajaxLibraryResults = $.ajax();
    this.$ajaxLibraryHours = $.ajax();
    this.emptyMessage = "Loading...";

    this.queryString.weeks = 1;

    this.state = {
      hideFilters: document.documentElement.clientWidth <= 1170,
      loading: true,
      libraryView: {
        label: "Show Me:",
        param: "view",
        activeFilter: "0",
        items: [{
          name: "view-tabs",
          value: "0",
          label: "Library View",
          checked: true
        },{
          name: "view-tabs",
          value: "1",
          label: "Hours View",
          checked: false
        }]
      },
      filters: {
        name: {
          label: "Name",
          placeholder: "All locations",
          param: "library",
          expanded: true,
          hideAccordion: true,
          filterType: "keyword",
          onChange: this.onKeywordChange,
          items:[{
            value: this.queryString.library || ""
          }]
        },
        features: {
          label: "Features",
          placeholder: "All features",
          param: "features",
          expanded: true,
          hideAccordion: true,
          filterType: "select",
          onChange: this.onSelectChange,
          selected: [],
          items: []
        },
        topics: {
          label: "Topics",
          placeholder: "All topics",
          param: "topic",
          expanded: true,
          hideAccordion: true,
          filterType: "select",
          onChange: this.onSelectChange,
          selected: [],
          items: []
        },
        selectedDate: {
          label: "Week Of",
          placeholder: "",
          param: "date",
          expanded: true,
          hideAccordion: true,
          filterType: "weekPicker",
          onChange: this.onDateChange,
          week: 0,
          items: [{
            date: moment().day("Sunday"),
            focused: false,
            onFocusChange: this.onFocusChange,
            properties: {
              numberOfMonths: 1,
              displayFormat: "MMM DD, YYYY"
            }
          }]
        },
        openNow: {
          param: "open_only",
          name: "open_now",
          filterType: "checkbox",
          onChange: this.onOpenNowChange,
          label: "Open Now",
          subLabel: currentDate(),
          theme: "base",
          checked: this.queryString.open_only === "1" ? true : false,
          value: "1"
        }
      },
      libraryResults: {
        totalCount: 0,
        items: []
      }
    };
  }

  componentDidMount() {
    // show the correct view
    if(this.queryString.active_filter) {
      this.handleViewChange(this.queryString.active_filter);
    }
    // fetch all expertise
    const getFeatures = this.fetchFeatures();
    // fetch all languages
    const getTopics = this.fetchTopics();

    // fetch all Libraries
    Promise
    .all([getFeatures,getTopics])
    .then(values => {
      let filters = Object.assign({}, this.state.filters);

      filters.features.items = filters.features.items.concat(values[0]);
      filters.topics.items = filters.topics.items.concat(values[1]);

      // mark filters found in the URL
      filters = this.initializeFilters(filters);

      // update state and render filters
      this.setState({filters});

      // fetch results
      this.fetchResults();

      this.emptyMessage = "No Results";
    });
  }

  /********** Data Fetching ***************/

  // returns an array of properly formated filters
  fetchFeatures = () => {
    return fetch(`${ this.props.requestUrl }/api/v1/taxonomy/amenities?_format=json`)
    .then(response => response.json())
    .then(data => {
      return data.filter(d => {
          label: d.label;
          value: d.tid;
          d.excludeFromLibrarydirectory = (d.excludeFromLibrarydirectory === "0") ? false : d.excludeFromLibrarydirectory;
          d.excludeFromLibrarydirectory = (d.excludeFromLibrarydirectory === "1") ? true : d.excludeFromLibrarydirectory;
          return !d.excludeFromLibrarydirectory;
      });
    })
    .catch(error => {
      console.warn("failed to fetch features:", error);
      return [];
    });
  }
  // returns an array of properly formated filters
  fetchTopics = () => {
    return fetch(`${ this.props.requestUrl }/api/v1/taxonomy/subject?_format=json`)
    .then(response => response.json())
    .then(data => {
      return data.map(d => {
        return {
          label: d.label,
          value: d.tid
        };
      });
    })
    .catch(error => {
      console.warn("failed to fetch topics (subjects):", error);
      return [];
    });
  }

  fetchOpen(rawResults) {
    const coeff = 1000 * 60 * 1;
    const date = new Date();
    const rounded = new Date(Math.round(date.getTime() / coeff) * coeff).getTime();

    fetch(`${ this.props.requestUrl }/api/v1/libcal/open-now?_format=json&cache=${ rounded }`)
    .then(response => {
      if (!response.ok) {
        throw Error(response.statusText);
      }
      return response.json();
    })
    .then(data => {
      const libraryResults = {
        items: rawResults.items.map(lib => {
          data.forEach(hour => {
            if(hour.nid === lib.id && lib.today_hours != null) {
              lib.today_hours.currently_open = hour.open;
            }
          });
          return lib;
        })
      };

      this.setState({ libraryResults, loading: false });
    })
    .catch(error => {
      console.warn("failed to fetch open now:", error);
      const libraryResults = {
        items: rawResults.items.map(lib => {
          if(lib.today_hours) {
            lib.today_hours.currently_open = false;
          }
          return lib;
        })
      };

      this.setState({ libraryResults, loading: false });
    });
  }

  // fetches results from the api based on the filter values passed
  fetchResults = () => {

    let activeFilters = Object.assign({
      _format: "json"
    }, this.queryString);

    this.$ajaxLibraryResults.abort();
    this.emptyMessage = "Loading...";

    // hide existing results while loading
    const libraryResults = {
      totalCount: 0,
      items: []
    };

    this.setState({ libraryResults });

// // TODO!!!!
// console.log("turn of cache buster");
    this.$ajaxLibraryResults = $.ajax({
      url: `${ this.props.requestUrl }/api/v1/library`,
      // cache: false,
      data: activeFilters
    });

    this.$ajaxLibraryResults.done(data => {
      const libraryResults = {
        items: []
      };

      if (typeof data === "undefined" || data.length === 0) {
        this.setState({ libraryResults });
        this.emptyMessage = "No Results";
        return;
      }

      libraryResults.items = this.parseResultItems(data);

      // Open Now disabled - 0.34.10
      // removing call to fetch and just setting the state here
      // this.fetchOpen(libraryResults);
      this.setState({ libraryResults, loading: false });

    }).fail((xhr, status, error)=> {
      console.warn("failed to fetch library results");

      const libraryResults = {
        totalCount: 0,
        items: []
      };

      this.emptyMessage = "Loading Error";

      this.setState({ libraryResults, loading: false });
    });
  }
  /********** End Data Fetching ***************/

  /********** Data Manipulation ***************/

  // data is a library object array
  parseResultItems = (data) => {
    return data.map(library => {
      const today_hours = Array.isArray(library.today_hours) ? null : library.today_hours;
      const weeks_hours = library.weeks_hours.locations && library.weeks_hours.locations.length ? this.parseLibraryHours(library.weeks_hours.locations[0]) : null;

      // Open Now disabled - 0.34.10
      // marking all libraries as closed to hide the open now flag
      if(today_hours){
        today_hours.currently_open = false;
      }

      return Object.assign({}, library, { today_hours, weeks_hours });
    });
  }

  parseLibraryHours = (data) => {
    const weekIndex = data.weeks && data.weeks.length ? data.weeks.length - 1 : 0;

    return Object.assign({}, data, {
      department_hours: data.department_hours && Array.isArray(data.department_hours)
      ? data.department_hours.map(department => {
        const deptIndex = department.weeks.length - 1;

        return Object.assign({}, department, {
          weeks: department.weeks && department.weeks.length
            ? [
              department.weeks[deptIndex].Sunday,
              department.weeks[deptIndex].Monday,
              department.weeks[deptIndex].Tuesday,
              department.weeks[deptIndex].Wednesday,
              department.weeks[deptIndex].Thursday,
              department.weeks[deptIndex].Friday,
              department.weeks[deptIndex].Saturday
            ]
            : []
        });
      })
      : [],
      weeks: data.weeks.length
      ? [
        data.weeks[weekIndex].Sunday,
        data.weeks[weekIndex].Monday,
        data.weeks[weekIndex].Tuesday,
        data.weeks[weekIndex].Wednesday,
        data.weeks[weekIndex].Thursday,
        data.weeks[weekIndex].Friday,
        data.weeks[weekIndex].Saturday
      ]
      : []
    });
  }

  initializeFilters = (filters) => {
    const params = this.queryString;

    if(params.features && filters.features.items.length) {
      if(typeof params.features === "string") {
        params.features = [params.features];
      }
      filters.features.selected = filters.features.items.filter(a => params.features.some((b) => a.value === b ) );
    }

    if(params.topic && filters.topics.items.length) {
      if(typeof params.topic === "string") {
        params.topic = [params.topic];
      }
      filters.topics.selected = filters.topics.items.filter(a => params.topic.some((b) => a.value === b ) );
    }

    return filters;
  }

  // function takes two optional values
  // the url parameter and the value it should equal
  // if neither a parameter or value are passed,
  // we assume the queryString has been updated else where
  // and we just need to reset the url
  updateUrl = (param, value = null) => {
    // if a parameter and a value are passed add the new value
    if(param && value) {
      this.queryString[param] = value;
    }
    // if a parameter is passed with a value remove the parameter
    if(param && (value === null || value.length === 0)) {
      delete this.queryString[param];
    }

    const stringifiedQueries = queryStringApp.stringify(this.queryString);
    const newUrl = window.location.origin + window.location.pathname + "?" + stringifiedQueries;

    window.history.replaceState({path: newUrl}, "", newUrl);
  }

  /********** End Data Manipulation ***************/

  /********** Event Handlers ***************/

  handleViewChange = (index) => {
    let libraryView = Object.assign({}, this.state.libraryView);
    libraryView.activeFilter = index;

    this.updateUrl("active_filter", index);
    this.setState({libraryView});
  }

  // date should always be a Sunday.
  onDateChange = (date) => {
    const filters = Object.assign({}, this.state.filters);
    const week = date.diff(this.todaysDay,'weeks') + 1;

    filters.selectedDate.items[0].date = date;
    filters.selectedDate.week = week >= 1 ? week : 1;

    this.queryString.weeks = week > 1 ? week : null;
    this.setState(filters);

    // fetch results
    this.fetchResults();
  }

  onFocusChange = ({focused}) => {
    let filters = Object.assign({}, this.state.filters);

    filters.selectedDate.items[0].focused = focused;

    this.setState(filters);
  }

  onKeywordChange = (event) => {
    let filters = Object.assign({}, this.state.filters);
    let value = event.target.value;

    filters.name.items[0].value = value;

    this.updateUrl(filters.name.param, value);
    this.setState({ filters });

    if(value.length > 1 || value.length === 0) {
      // fetch results
      this.fetchResults();
    }
  }

  onSelectChange = (filterName, value) => {
    let filters = Object.assign({}, this.state.filters);

    for (var filterObj in filters) {
      if(filterName === filters[filterObj].param) {
        filters[filterObj].selected = value;

        this.updateUrl(filterName, value.map(v => v.value));
        break;
      }
    }

    this.setState({ filters });

    // fetch results
    this.fetchResults();
  }

  // when an filter accordion expands or collapses
  // receives the key value of the filter and a boolean state
  onAccordionChange = (filterName, value) => {
    let filters = Object.assign({}, this.state.filters);

    for (var filterObj in filters) {
      if(filterName === filters[filterObj].param) {
        filters[filterObj].expanded = value;
        break;
      }
    }

    this.setState({ filters });
  }

  // when the clear all button is clicked
  // receives the event from the button
  onClearAll = (event) => {
    event.preventDefault();
    let oldFilters = this.state.filters;

    const filters = R.map((filter) => {
      if(filter.selected) {
        filter.selected = [];
      }
      return filter;
    },oldFilters);

    filters.name.items[0].value = "";

    // clear the parameters in the url
    this.queryString = {};
    this.updateUrl();

    this.fetchResults();

    this.setState({filters});
  }

  // When the user clicks the >> or << button to show or hide the filters
  // receives the desired state as a boolean
  // true == hidden
  // false == visible
  onHideFilters = (value) => {
    this.setState({ hideFilters: value });

    if(this.state.showDetails) {
      this.onHideDetail();
    }
  }

  // when the openNow filter is clicked
  // swap the checked status
  onOpenNowChange = () => {
    let filters = Object.assign({}, this.state.filters);

    filters.openNow.checked = !filters.openNow.checked;

    if(filters.openNow.checked){
      this.updateUrl(filters.openNow.param, 1);
    } else {
      this.updateUrl(filters.openNow.param, null);
    }

    this.setState({ filters });
  }


  /********** End Event Handlers ***************/


  render() {
    const { assetsUrl } = this.props;
    const { filters, libraryResults, hideFilters, libraryView } = this.state;
    const startDate = filters.selectedDate.items[0].date;
    const endDate = moment(startDate).add(6, "days");


    const gridModifier = hideFilters
        ? "hl__filter-directory__container--no-filters"
        : " ";
    const viewModifier = hideFilters
        ? "hl__filter-directory__views-container--no-filters"
        : " ";

    const baseFilters = [
      filters.name,
      filters.features,
      filters.topics
    ];

    // Open Now disabled - 0.34.10
    // removing open now filtering and just setting the variable
    // const libraryItems = !filters.openNow.checked
    //   ? libraryResults.items
    //   : libraryResults.items.filter(item => item.times.currently_open);
    const libraryItems = libraryResults.items;

    const hoursResults = libraryItems.filter(library => {
      return library.weeks_hours !== null;
    });

    const activeFilter = libraryView.activeFilter;

    const filterArray = activeFilter === "1"
      ? R.concat([filters.selectedDate], baseFilters)
      : baseFilters;

    return(
      <div className="hl__filter-directory__app">
        <section className="hl__filter-directory__views">
          <div className={ `hl__filter-directory__views-container ${ viewModifier }` }>
            <FilterByType
              onChange= { this.handleViewChange }
              { ...libraryView } />
            { false && (<CheckboxFilter { ...filters.openNow }/>) }
          </div>
        </section>
        <div className={`hl__filter-directory__container ${ gridModifier }` }>
          <section className="hl__filter-directory__filters">
            <FilterRail
              onHideFilters={ this.onHideFilters }
              onClearAll={ this.onClearAll }
              assetsUrl={ assetsUrl }
              title="Filter Location"
              hideFilters={ hideFilters }
              groups= { filterArray } />
          </section>

          <section className="hl__filter-directory__results">
            { activeFilter === "1" && (
              <div className="hl__filter-directory__result-details">
                Showing <b>{ startDate.format("MMMM DD, YYYY") } &mdash; { endDate.format("MMMM DD, YYYY") }</b>
              </div>
            )}

            { activeFilter === "0" && libraryItems.length > 0 && (
              <LibraryResultsGrid libraries={ libraryItems } />
            )}
            { activeFilter === "0" && libraryItems.length === 0 && (
              <span className="hl__filter-directory__no-results">{ this.emptyMessage }</span>
            )}
            { activeFilter === "1" && hoursResults.length > 0 && (
              <LibraryHoursList
                libraries={ hoursResults }
                startDate={ startDate }
                week={ filters.selectedDate.week } />
            )}
            { activeFilter === "1" && hoursResults.length === 0 && (
              <span className="hl__filter-directory__no-results">{ this.emptyMessage }</span>
            )}
          </section>
        </div>
      </div>
    );
  }
}
