import React     from "react";
import {render}  from "react-dom";
import App       from "./search-listing-app.jsx";

const el = document.querySelector(".js-search-listing-app");

if(el) {
  const harvardLibrary = window.harvardLibrary || {};

  const initialProps = {
    searchRequest: {
      page: 1,
      sort: "" //relevance by default
    },
    baseRequestUrl: el.getAttribute("data-request-url"),
    featuredImages: [],
    hollisResults: [],
    onlineArticles: [],
    featuredLibrary: null,
    iconPromos: harvardLibrary.iconPromos || null
  };

  const props = initialProps;

  render(
    <App { ...props} />,
    el
  );
}

