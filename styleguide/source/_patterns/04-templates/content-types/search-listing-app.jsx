import $                      from "jquery";
import React, { Component }   from "react";
import queryString            from "query-string";

import Button                 from "../../01-atoms/buttons/button.jsx";

import SingleInputForm        from "../../02-molecules/forms/single-input-form.jsx";
import FilterByType           from "../../02-molecules/forms/filter-by-type.jsx";
import LibraryCard            from "../../02-molecules/teasers/library-card.jsx";
import IconPromo              from "../../02-molecules/teasers/icon-promo.jsx";

import ResultsList            from "../../03-organisms/lists/results-list.jsx";
import FeaturedImageList      from "../../03-organisms/lists/featured-image-list.jsx";

import { array, string, number, shape, object } from "prop-types";

export default class SearchListingApp extends Component {
  static propTypes = {
    searchRequest: shape({
      page: number.isRequired,
      sort: string.isRequired
    }),
    baseRequestUrl: string.isRequired,
    baseImageRequestUrl: string.isRequired,
    featuredImages: array,
    hollisResults: array,
    onlineArticles: array,
    featuredLibrary: object,
    iconPromos: array
  }

  static defaultProps = {
    searchRequest: {
      page: 1,
      sort: "" //relevance by default
    },
    baseRequestUrl: "",
    baseImageRequestUrl: "",
    featuredImages: [],
    hollisResults: [],
    onlineArticles: []
  }

  state = {
    queryString: queryString.parse(location.search),
    searchTerm: queryString.parse(location.search).keyword || "",
    requestedSearchTerm: queryString.parse(location.search).keyword || "",
    searchTermDisplay: "",
    totalHollisResults: 0,
    totalImages: 0,
    totalOnlineArticles: 0,
    featuredImages: [],
    featuredLibrary: this.props.featuredLibrary || null,
    hollisResults: [],
    onlineArticles: [],
    activeTabIndex: "0",
    filters: {
      label: "Show Me:",
      items: [{
        name: "search-tabs",
        value: "0",
        label: "HOLLIS"
      },{
        name: "search-tabs",
        value: "1",
        label: "Online Articles"
      },{
        name: "search-tabs",
        value: "2",
        label: "Site Search Results"
      }]
    }
  }

  componentDidMount = () => {
    const searchTerm = this.state.searchTerm;

    if (searchTerm.length > 0) {
      this.getData(searchTerm);
    }

    if (!this.state.featuredLibrary) {
      this.getFeaturedLibrary("131").done((data) => {
        this.setState({ featuredLibrary: data[0] });

      }).fail((xhr, status, error)=> {
        if(window.harvardLibrary.featuredLibrary) {
          console.warn("using static featured library");
          this.setState({ featuredLibrary: window.harvardLibrary.featuredLibrary });
        } else {
          console.error("failed to fetch featured library");
          this.setState({ featuredLibrary: null });
        }
      });
    }

    this.initializeGoogleSearch();

  }

  initializeGoogleSearch = () => {
    const $el = $("#hl__google-cse-results");
    // adding Google Custom Search code
    $el.html(`<gcse:searchresults-only gname="google_cse" queryParameterName="keyword"></gcse:searchresults-only>`);

    var cx = "002386710524587861402:bhhhefxf6ki";
    var gcse = document.createElement("script");
    gcse.type = "text/javascript";
    gcse.async = true;
    gcse.src = "https://cse.google.com/cse.js?cx=" + cx;
    var s = document.getElementsByTagName("script")[0];
    s.parentNode.insertBefore(gcse, s);

    $el.on("click",".gsc-url-bottom .gs-visibleUrl", function() {
      const url = $(this).closest(".gs-result").find("a.gs-title").first().attr("href");
      if (url) {
        window.location.href = url;
      }
    });
  }

  resetSearch = () => {
    this.setState({
      searchTerm: "",
      onlineArticles: [],
      hollisResults: [],
      featuredImages: [],
      totalHollisResults: 0,
      totalOnlineArticles: 0,
      totalImages: 0,
      requestedSearchTerm: "",
      searchTermDisplay: ""});
  }

  handleSearchTermChange = (keyword) => {
    this.setState({ searchTerm: keyword });
  }

  handleSubmit = () => {
    const gcse = window.google.search.cse.element.getElement("google_cse");
    const keyword = this.state.searchTerm;

    this.setState({requestedSearchTerm:keyword});

    if (keyword.length > 0) {
      this.state.queryString.keyword = keyword;

      if (gcse) {
        gcse.execute(keyword);
      }

      this.getData(keyword);
      this.updateUrl(keyword);
    } else {

      if (gcse) {
        gcse.clearAllResults();
      }

      this.resetSearch();
    }
  }

  getData = (keyword) => {

    // get images
    this.getImageData(keyword).done(data => {
      if(keyword != this.state.requestedSearchTerm) {
        return;
      }

      const results = data["SEGMENTS"]["JAGROOT"]["RESULT"];
      
      if( typeof results["DOCSET"] === "undefined") {
        this.setState({
          totalImages: 0,
          featuredImages: [],
          searchTermDisplay: keyword
        });
        return;
      }
      
      const totalHits = parseInt(results["DOCSET"]["@TOTALHITS"]);
      let parsedData = [];

      if (totalHits != 0) {
        parsedData = this.parseImageData(results["DOCSET"]["DOC"], keyword).slice(0,4);
      }

      this.setState({
        totalImages: totalHits,
        featuredImages: parsedData
      });
    }).fail( (xhr, status, error) => {
      this.setState({
        totalImages: 0,
        featuredImages: [],
        searchTermDisplay: keyword
      });
    });

    // Hollis everything results
    this.getHollisData(keyword).done(data => {
      if(keyword != this.state.requestedSearchTerm) {
        return;
      }

      const results = data["SEGMENTS"]["JAGROOT"]["RESULT"];
      
      if( typeof results["DOCSET"] === "undefined") {
        this.setState({
          totalHollisResults: 0,
          hollisResults: [],
          searchTermDisplay: keyword
        });
        return;
      }
      
      const totalHits = parseInt(results["DOCSET"]["@TOTALHITS"]);
      let parsedData = [];

      if (totalHits != 0) {
        parsedData = this.parseListingData(results["DOCSET"]["DOC"], keyword);
      }

      this.setState({
        totalHollisResults: totalHits,
        hollisResults: parsedData,
        searchTermDisplay: keyword
      });
    }).fail( (xhr, status, error) => {
      this.setState({
        totalHollisResults: 0,
        hollisResults: [],
        searchTermDisplay: keyword
      });
    });

    // Get Articles
    this.getArticleData(keyword, [{type:"rtype",values:["articles"]},{type:"tlevel",values:["online_resources"]},{type:"tlevel",values:["peer_reviewed"]}]).done(data => {
      if(keyword != this.state.requestedSearchTerm) {
        return;
      }

      const results = data["SEGMENTS"]["JAGROOT"]["RESULT"];
      
      if( typeof results["DOCSET"] === "undefined") {
        return;
      }

      const totalHits = parseInt(results["DOCSET"]["@TOTALHITS"]);
      let parsedData = [];

      if (totalHits != 0) {
        parsedData = this.parseListingData(results["DOCSET"]["DOC"], keyword);
      }

      this.setState({
        totalOnlineArticles: totalHits,
        onlineArticles: parsedData,
        searchTermDisplay: keyword
      });
    });
  }

  getFeaturedLibrary = (id) => {
    return $.ajax({
      url : `/api/v1/library/${ id }?_format=json`, // 131 - cabot
      data : null,
      timeout : 10000,
      dataType : "json"
    });
  }

  getHollisData = (keyword) => {
    const { searchRequest, baseRequestUrl } = this.props;

    const query = "query=any,contains," + encodeURIComponent(keyword);

    const queryUrl = `${baseRequestUrl}?institution=HVD&${query}&bulkSize=10&indx=${searchRequest.page}&lang=eng&loc=local,scope:(HVD)&loc=adaptor,primo_central_multiple_fe&json=true&pcAvailability=true&onCampus=true`;

    return $.ajax({
      url : queryUrl,
      data : null,
      timeout : 10000,
      dataType : "jsonp"
    });
  }

  getArticleData = (keyword, optionalFacets=[]) => {
    const { searchRequest, baseRequestUrl } = this.props;

    let query = "query=any,contains," + encodeURIComponent(keyword);

    optionalFacets.forEach(facet => {
      query += "&query=facet_" + facet.type + ",exact," + encodeURIComponent(facet.values.join(" "));
    });

    const queryUrl = `${baseRequestUrl}?institution=HVD&${query}&bulkSize=10&indx=${searchRequest.page}&lang=eng&loc=local,scope:(HVD)&loc=adaptor,primo_central_multiple_fe&onCampus=true&json=true`;

    return $.ajax({
      url : queryUrl,
      data : null,
      timeout : 10000,
      dataType : "jsonp"
    });
  }

  getImageData = (keyword) => {
    const { searchRequest, baseRequestUrl } = this.props;

    const query = "query=any,exact," + encodeURIComponent(this.state.searchTerm);

    const queryUrl = `${baseRequestUrl}?institution=HVD&${query}&bulkSize=10&indx=${searchRequest.page}&sortField=${searchRequest.sort}&loc=local,scope:(HVD_VIA)&lang=eng&json=true`;

    return $.ajax({
      url : queryUrl,
      data : null,
      timeout : 10000,
      dataType : "jsonp"
    });
  }

  setActiveTab = (index) => {
    if (index != this.state.activeTabIndex) {
      this.setState({activeTabIndex: index});
    }
  }

  updateUrl() {
    const stringifiedQueries = queryString.stringify(this.state.queryString);
    const newUrl = window.location.protocol + "//" + window.location.host + window.location.pathname + "?" + stringifiedQueries;

    window.history.replaceState({path: newUrl}, "", newUrl);
  }

  parseListingData(data, keyword="") {
    const results = Array.isArray(data) ? data : [data];

    return results.map(result => {
      const record = result.PrimoNMBib.record;
      const display = record.display;
      const addata = record.addata;
      const linkData = this.createLink(result, keyword);

      const author = addata.au ? squashArray(addata.au) : addata.aucorp ? squashArray(addata.aucorp) : "";
      const type = display.type ? squashArray(display.type) : "";

      let publisher = display.publisher ? squashArray(display.publisher) : "";
      publisher = display.edition ? publisher + ", " + squashArray(display.edition) : publisher;
      publisher = display.lds19 ? publisher + " " + squashArray(display.lds19) : publisher;

      return {
        title: display.title ? squashArray(display.title) : "",
        href: linkData.recordLink,
        isPartOf: display.ispartof ? squashArray(display.ispartof) : "",
        id: result.PrimoNMBib.record.control.recordid,
        type,
        link: linkData.buttonLink,
        additionaLink: linkData.additionalLink,
        publisher,
        author
      };
    });

    function squashArray(value) {
      return Array.isArray(value) ? value.join(" ") : value;
    }
  }

  parseImageData(data, keyword) {
    const results = Array.isArray(data) ? data : [data];
    return results.map(result => {
      const restricted = result.PrimoNMBib.record.addata.mis1 ? result.PrimoNMBib.record.addata.mis1.includes('restrictedImage="true"') : false;
      let thumbnail = result.LINKS.thumbnail || "";


      if(Array.isArray(result.LINKS.thumbnail)) {
        thumbnail = result.LINKS.thumbnail[0];
      }

      // restricted images will only render in the 150 x 150 aspect ratio
      if(!restricted) {
        thumbnail = thumbnail.replace(/=150/gi,"=270");
      }

      return {
        title: result.PrimoNMBib.record.display.title,
        thumbnail,
        id: result.PrimoNMBib.record.control.recordid,
        link: this.createLink(result, keyword).buttonLink,
        restricted
      };
    }).filter(result => result.thumbnail.startsWith("http"));
  }

  createLink(result, keyword) {
    const id = result.PrimoNMBib.record.control.recordid;
    const searchTerm = encodeURIComponent(keyword);
    const category = result.PrimoNMBib.record.delivery.delcategory;
    const validOpenUrl = typeof result.LINKS.linktorsrc === "string" || typeof result.LINKS.openurl === "string";
    const baseUrl = `https://hollis.harvard.edu/primo-explore/fulldisplay?docid=${id}&vid=HVD2&lang=en_US&search_scope=everything&query=any,contains,${searchTerm}`;

    return {
      recordLink: baseUrl,
      buttonLink: (() => {
        if(category === "Remote Search Resource" && validOpenUrl) {
          return {
            href: result.LINKS.linktorsrc || result.LINKS.openurl,
            text: "View online"
          };
        } else {
          return {
            href: baseUrl,
            text: "Find it at Harvard"
          };
        }
      })(),
      additionalLink: null // additional link removed
    };
  }

  renderIconPromos(iconPromos) {
    return iconPromos.map((iconPromo, i) => {
      return <IconPromo key={ i } { ...iconPromo } />;
    });
  }

  render() {
    const { iconPromos } = this.props;
    const { 
      featuredLibrary,
      featuredImages, 
      hollisResults, 
      onlineArticles, 
      searchTermDisplay, 
      totalHollisResults, 
      totalImages, 
      totalOnlineArticles, 
      activeTabIndex, 
      filters } = this.state;
  
    const button = {
      text: "Search",
      type: "submit",
      size: "small",
      theme: "inline"
    };

    const hollisButton = {
      href: `https://hollis.harvard.edu/primo-explore/search?query=any,contains,${encodeURIComponent(searchTermDisplay)}&search_scope=everything&vid=HVD2`,
      info: `view more results on Hollis for ${searchTermDisplay}`,
      text: "more from hollis",
      type: "button",
      size: "",
      theme: "secondary",
      outline: false,
      cssClass: ""
    };

    const articlesButton = {
      href: `https://hollis.harvard.edu/primo-explore/search?query=any,contains,${encodeURIComponent(searchTermDisplay)}&search_scope=everything&vid=HVD2&mfacet=rtype,include,articles,1`,
      info: `view more results on Hollis for ${searchTermDisplay}`,
      text: "more from hollis",
      type: "button",
      size: "",
      theme: "secondary",
      outline: false,
      cssClass: ""
    };

    const seeMoreImages = `https://images.hollis.harvard.edu/primo-explore/search?query=any,contains,${encodeURIComponent(searchTermDisplay)}&tab=default_tab&search_scope=default_scope&vid=HVD_IMAGES&lang=en_US&offset=0`

    return (
      <section className="hl__search-listing">
        <header className="hl__search-listing__header">
          <div className="hl__search-listing__container">
            <h1 className="hl__search-listing__title"><span>Beta</span> Search Results</h1>
            <div className="hl__search-listing__search-input">
              <SingleInputForm
                onSubmit={this.handleSubmit}
                onChange={this.handleSearchTermChange}
                action= "#"
                theme= "clear"
                button= { button }
                searchTerm= { this.state.searchTerm } />
              <div className="hl__search-listing__input-helper">
                <span>We're working to build a more helpful search experience.</span>
                <a className="hl__link-tag" href="/about/news/2018-01-26/building-better-library-search-experience">Read our vision for search.</a>
              </div>
            </div>
          </div>
        </header>
        <main className="hl__search-listing__main-content">
          <div className="hl__search-listing__filters">
            <div className="hl__search-listing__container">
              <FilterByType
                activeFilter = { activeTabIndex } 
                onChange= { this.setActiveTab }
                { ...filters } />
              <div className="hl__search-listing__more-filters">
                <span>More options coming soon!&nbsp;</span> <a href="https://harvard.az1.qualtrics.com/jfe/form/SV_em721xdnn2Kg6xL?Source=BetaSearch" className="hl__link-tag">What would you like to see?</a>
              </div>
            </div>
          </div>
          <div className="hl__search-listing__results-container">
            <div className={"hl__search-listing__images " + (activeTabIndex === "0" && featuredImages.length ? "is-active" : "")}>
              <FeaturedImageList
                featuredImages={featuredImages}
                totalImages={totalImages}
                seeMoreLink={ seeMoreImages } />
            </div>
            <div className="hl__search-listing__results">
              <div className={"hl__search-listing__result " + (activeTabIndex === "0" ? "is-active" : "")}>
                <div className="hl__search-listing__result-details">
                  Showing <b>{ hollisResults.length }</b> of <b>{ totalHollisResults }</b> HOLLIS results for <b>"{searchTermDisplay}"</b>
                </div>
                <ResultsList 
                  results={hollisResults} 
                  searchTerm={searchTermDisplay} 
                  totalResults={totalHollisResults} />
                { hollisResults.length > 0 && (
                  <div className="hl__search-listing__more-button">
                    <Button {...hollisButton} />
                  </div>
                )}
              </div>

              <div className={"hl__search-listing__result " + (activeTabIndex === "1" ? "is-active" : "")}>
                <div className="hl__search-listing__result-details">
                  Showing <b>{ onlineArticles.length }</b> of <b>{ totalOnlineArticles }</b> Online Articles results for <b>"{searchTermDisplay}"</b>
                </div>
                <ResultsList 
                  results={onlineArticles} 
                  searchTerm={searchTermDisplay} 
                  totalResults={totalOnlineArticles} />
                { onlineArticles.length > 0 && (
                  <div className="hl__search-listing__more-button">
                    <Button {...articlesButton} />
                  </div>
                )}
              </div>

              <div className={"hl__search-listing__result " + (activeTabIndex === "2" ? "is-active" : "")}>
                <div id="hl__google-cse-results"></div>
              </div>
            </div>
            <div className="hl__search-listing__sidebar">
              { featuredLibrary && (
                <div className="hl__search-listing__featured-library">
                  <div className="hl__search-listing__library-title">Featured Library</div>
                  <LibraryCard {...featuredLibrary } />
                </div>
              )}
              { this.renderIconPromos(iconPromos) }
            </div>
          </div>
        </main>
      </section>
    );
  }
}
