import $                      from "jquery";
import React, { Component }   from "react";
import R                      from "ramda";

import Button                 from "../../01-atoms/buttons/button.jsx";

import FilterCollections      from "../../02-molecules/forms/collection-filters.jsx";

import TeaserGrid             from "../../03-organisms/lists/teaser-grid.jsx";
import MasonryPromo           from "../../03-organisms/lists/masonry-promo.jsx";

import { string } from "prop-types";

export default class CollectionDirectoryApp extends Component {
  static propTypes = {
    requestUrl: string.isRequired,
    assetsUrl: string.isRequired
  };

  constructor(props) {
    super(props);

    this.state = {
      listView: window.innerWidth <= 480,
      filters: {
        keyword: ""
      },
      results: {
        totalCount: 0,
        items: []
      }
    };

    this.currentPage = 0;
    this.$ajaxResults = $.ajax();
    this.keywordDebounce = null;
  }

  componentDidMount() {

    // fetch results for filters in params
    this.fetchResults(this.state.filters);
  }

  /********** Data Fetching ***************/

  // returns an array of properly formated filters
  fetchResults = (filters, loadMore = false) => {
    if(!loadMore) {
      this.currentPage = 0;
    }

    let activeFilters = {
      _format: "json",
      page: this.currentPage
    };

    if(filters.keyword) {
      activeFilters.keywords = filters.keyword;
    }

    this.$ajaxResults.abort();

    this.$ajaxResults = $.ajax({
      url: `${ this.props.requestUrl }/api/v1/collections`,
      // cache: false,
      data: activeFilters
    });

    this.$ajaxResults.done(data => {
      const currentResults = this.state.results;
      let newResults = {
        totalCount: data.results.totalCount || 0,
        items: []
      };

      if (typeof data.results.items === "undefined" || data.results.items.length === 0) {

        if(!loadMore) {
          this.setState({ results: newResults });
        }
        return;
      }

      const newItems = this.parseResultItems(data.results.items);

      if(loadMore) {
        newResults.items = currentResults.items.concat(newItems);
      } else {
        newResults.items = newItems;
      }

      this.setState({ results: newResults });

    }).fail((xhr, status, error)=> {
      console.warn("failed to fetch results");

      let results = {
        totalCount: 0,
        items: []
      };

      this.setState({ results });
    });

  }

  /********** End Data Fetching ***************/

  /********** Data Manipulation ***************/
  parseResultItems = (data) => {
    return data.map((item, i) => {
      return {
        id: item.id || `${i}`,
        cardType: item.card_type,
        title: {
          text: item.title
        },
        collection_type: item.collection_type,
        subText: item.summary,
        subTitle: item.summary,
        description: "",
        link: {
          href: item.url,
          info: `Learn more about the '${ item.title }' collection`
        },
        card_image: item.card_image,
        image: item.list_image
      };
    });
  }
  /********** End Data Manipulation ***************/

  /********** Event Handlers ***************/

  handleKeywordChange = (event) => {
    // get current filters and update keyword to equal the new value
    const filters = R.assoc('keyword', event.target.value, this.state.filters);

    // clear previous keyword typed
    clearTimeout(this.keywordDebounce);

    // wait for user to stop typing before fetching results
    this.keywordDebounce = setTimeout(() => {
      this.fetchResults( filters );
    },300);

    // update the state with the current keyword
    this.setState({ filters });
  }

  handleOnlineChange = () => {
    // get current filters and toggle availableOnline value
    // const filters = R.assoc('availableOnline', !this.state.filters.availableOnline, this.state.filters);
    //
    // this.fetchResults( filters );
    // this.setState({ filters });
  }

  handleViewChange = () => {
    this.setState({ listView: !this.state.listView });
  }

  handleLoadMore = () => {
    this.currentPage = this.currentPage + 1;

    this.fetchResults(this.state.filters, true);
  }

  /********** End Event Handlers ***************/


  render() {
    const { filters, listView, results } = this.state;

    const button = {
      text: "Load More",
      theme: "secondary"
    };

    return(
      <div className="hl__collection-directory__app">
        <FilterCollections
          onViewChange= { this.handleViewChange }
          onKeywordChange= { this.handleKeywordChange }
          listView= { listView }
          { ...filters } />

        <div className="hl__collection-directory__results">
          { results.items && results.items.length > 0 && listView && (
            <TeaserGrid results={ results.items } />
          )}
          { results.items && results.items.length > 0 && !listView && (
            <MasonryPromo results={ results.items } />
          )}
          { results.items && results.items.length === 0 && (
            <span className="hl__collection-directory__no-results">No Results Found</span>
          )}
        </div>
        { results.items && results.items.length < results.totalCount && (
          <div className="hl__collection-directory__load-more">
            <Button
              handleClick = { this.handleLoadMore }
              { ...button } />
          </div>
        )}
      </div>
    );
  }
}
