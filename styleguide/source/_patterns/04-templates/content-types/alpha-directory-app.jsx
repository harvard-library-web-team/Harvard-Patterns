import React, {Component}     from "react";
import $                      from "jquery";
import R                      from "ramda";
import queryStringApp         from "query-string";

import SearchInputRow         from "../../02-molecules/forms/search-input-row.jsx";
import GroupedList            from "../../02-molecules/lists/grouped-list.jsx";

import { string } from "prop-types";

export default class AlphaDirectory extends Component {

  static propTypes = {
    requestUrl: string.isRequired,
    assetsUrl: string.isRequired,
    theme: string,
    pageType: string
  }

  constructor(props) {
    super(props);

    this.queryString = queryStringApp.parse(location.search); // params in the url

    let placeholder = 'Try "bookmarklet"';
    let label = "Search Services & Tools:";

    this.state = {
      filterKeyword: {
        label: label,
        param: "filter",
        onChange: this.onKeywordChange,
        placeholder: placeholder,
        theme: this.props.theme || "yellow",
        value: this.queryString.filter || ""
      },
      results: {
        items: {}
      }
    };

    this.$ajaxResults = $.ajax();
    this.emptyMessage = "Loading...";
  }

  componentDidMount() {
    this.fetchResults();
    this.emptyMessage = "No Results";

  }

  /********** Data Fetching ***************/
  fetchResults() {
    let activeFilters = Object.assign({
      _format: "json"
    }, this.queryString);

    this.$ajaxResults.abort();

    this.$ajaxResults = $.ajax({
      url: this.props.requestUrl,
      // cache: false,
      data: activeFilters
    });

    this.$ajaxResults.done(data => {
      const results = {
        items: {}
      };

      if (typeof data.results.items === "undefined" || data.results.items.length === 0) {
        this.setState({ results });
        return;
      }

      results.items = data.results.items;

      this.setState({ results });

    }).fail((xhr, status, error)=> {
      console.warn("failed to fetch results");

      const results = {
        items: {}
      };

      this.setState({ results });
    });
  }
  /********** End Data Fetching ***************/

  /********** Data Manipulation ***************/
  renderResults(results) {
    const { filterKeyword } = this.state;
    const keys = Object.keys(results);

    if(filterKeyword.value) {
      return keys.map(key => {
        return (
          <GroupedList
            key={ key }
            columns={ 1 }
            items={ results[key] } />
        );
      });
    } else {
      return keys.map(key => {
        return (
          <GroupedList
            key={ key }
            columns={ 1 }
            label={ key }
            items={ results[key] } />
        );
      });
    }
  }

  // function takes two optional values
  // the url parameter and the value it should equal
  // if neither a parameter or value are passed,
  // we assume the queryString has been updated else where
  // and we just need to reset the url
  updateUrl = (param, value = null) => {
    // if a parameter and a value are passed add the new value
    if(param && value) {
      this.queryString[param] = value;
    }
    // if a parameter is passed with a value remove the parameter
    if(param && (value === null || value.length === 0)) {
      delete this.queryString[param];
    }

    const stringifiedQueries = queryStringApp.stringify(this.queryString);
    const newUrl = window.location.origin + window.location.pathname + "?" + stringifiedQueries;

    window.history.replaceState({path: newUrl}, "", newUrl);
  }

  /********** End Data Manipulation ***************/

  /********** Event Handlers ***************/
  onKeywordChange = (event) => {
    let filterKeyword = Object.assign({}, this.state.filterKeyword);
    let value = event.target.value;

    filterKeyword.value = value;

    this.updateUrl(filterKeyword.param, value);
    this.setState({ filterKeyword });

    this.fetchResults();
  };

  render() {
    const { filterKeyword, results } = this.state;

    return (
      <section className="hl__alpha-directory__app">
        <div className="hl__alpha-directory__filters">
          <SearchInputRow { ...filterKeyword } />
        </div>
        <div className="hl__alpha-directory__results">
          { !R.isEmpty(results.items) && (
            <div>{ this.renderResults(results.items) }</div>
          )}
          { R.isEmpty(results.items) && (
            <div className="hl__alpha-directory__no-results">{ this.emptyMessage }</div>
          )}
        </div>
      </section>
    );
  }
}
