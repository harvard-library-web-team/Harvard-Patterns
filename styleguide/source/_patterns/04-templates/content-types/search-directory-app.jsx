import $                      from "jquery";
import React, { Component }   from "react";
import queryString            from "query-string";

import ResultsList            from "../../03-organisms/lists/results-list.jsx";

import { string } from "prop-types";

export default class SearchListingApp extends Component {
  static propTypes = {
    baseRequestUrl: string.isRequired
  }

  constructor(props) {
    super(props);

    this.state = {
      searchTerm: queryString.parse(location.search).keywords || "",
      hollisResults: []
    };
  }

  componentDidMount = () => {
    const searchTerm = this.state.searchTerm;

    if (searchTerm.length > 0) {
      this.getData(searchTerm);
    }
  }

  getData = (keyword) => {

    // Hollis everything results
    this.getHollisData(keyword).done(data => {
      const results = data;

      if( typeof results["docs"] === "undefined") {
        this.setState({ hollisResults: [] });
        return;
      }

      const totalHits = parseInt(results["info"]["total"]);

      this.setState({ hollisResults: totalHits });
    }).fail( (xhr, status, error) => {
      this.setState({ hollisResults: [] });
    });
  }

  getHollisData = (keyword) => {
    const { baseRequestUrl } = this.props;

    const query = "q=any,contains," + encodeURIComponent(keyword);

    const queryUrl = `${baseRequestUrl}?${query}&tab=everything&scope=everything`;

    return $.ajax({
      url : queryUrl,
      data : null,
      timeout : 10000,
      dataType : "json"
    });
  };

  render() {
    const { hollisResults } = this.state;

    return (
      <h2 className="hl__search-directory__hollis-results">
        Search HOLLIS catalog <span>({hollisResults} results)</span>
      </h2>
    );
  }
}
