import React     from "react";
import {render}  from "react-dom";
import App       from "./search-directory-app.jsx";

const el = document.querySelector(".js-search-directory-app");

if(el) {

  const props = {
    baseRequestUrl: el.getAttribute("data-request-url")
  };

  render(
    <App { ...props} />,
    el
  );
}

