import React     from "react";
import {render}  from "react-dom";
import App       from "./alpha-directory-app.jsx";

const el = document.querySelector(".js-alpha-directory-app");

if(el) {
  const props = {
    requestUrl: el.getAttribute("data-request-url"),
    assetsUrl: el.getAttribute("data-assets-url"),
    pageType: el.getAttribute("data-page-type")
  };

  props.requestUrl = `${ props.requestUrl }/api/v1/services`;
  props.theme = "yellow";

  render(
    <App { ...props} />,
    el
  );
}
