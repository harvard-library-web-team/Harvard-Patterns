import React     from "react";
import {render}  from "react-dom";
import App       from "./space-finder-app.jsx";

const el = document.querySelector(".js-space-finder");

if(el) {

  const props = {
    baseRequestUrl: el.getAttribute("data-request-url"),
    baseAssetsUrl: el.getAttribute("data-assets-url")
  };

  render(
    <App { ...props } />,
    el
  );
}

