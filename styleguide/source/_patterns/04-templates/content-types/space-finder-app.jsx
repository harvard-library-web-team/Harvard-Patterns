import $                      from "jquery";
import React, { Component }   from "react";
import R                      from "ramda";
import queryString            from "query-string";

import FilterRail             from "../../02-molecules/forms/filter-rail.jsx";

import SpacesResults          from "../../03-organisms/general/spaces-results.jsx";
import SpacesMap              from "../../03-organisms/media/spaces-map.jsx";

import { string } from "prop-types";

export default class SpaceFinderApp extends Component {

  static propTypes = {
    baseRequestUrl: string.isRequired,
    baseAssetsUrl: string.isRequired
  };

  static defaultProps = {};

  constructor(props) {
    super(props);

    this.state = {
      results: {
        totalCount: 0,
        items: []
      },
      mapResults: {},
      activeSpace: {},
      hideFilters: false,
      showDetails: false,
      showMap: true,
      showResults: false,
      filters: {
        roomTypes: {
          label: "Space Type",
          category: "room_types",
          param: "room_type",
          expanded: true,
          filterType: "checkbox",
          onChange: this.onFilterChange,
          items: []
        },
        noise: {
          label: "Noise Level",
          category: "noise_level",
          param: "noise_level",
          expanded: true,
          filterType: "inline-checkbox",
          onChange: this.onFilterChange,
          items: [{
            label: "Silent",
            checked: false,
            value: "0",
            icon: `${ this.props.baseAssetsUrl }/assets/images/svg-icons/noise-silent.svg`
          },{
            label: "Whispers",
            checked: false,
            value: "1",
            icon: `${ this.props.baseAssetsUrl }/assets/images/svg-icons/noise-whisper.svg`
          },{
            label: "Chatter",
            checked: false,
            value: "2",
            icon: `${ this.props.baseAssetsUrl }/assets/images/svg-icons/noise-chatter.svg`
          }]
        },
        seats: {
          label: "Number of Seats",
          category: "number_seats",
          param: "number_seats",
          expanded: true,
          filterType: "range_slider",
          onChange: this.onSeatChange,
          items: [{
            min: 1,
            max: 10,
            value: {
              min: 1,
              max: 10
            }
          }]
        },
        features: {
          label: "Features",
          category: "amenities",
          param: "features",
          expanded: false,
          filterType: "checkbox",
          onChange: this.onFilterChange,
          items: []
        },
        buildings: {
          label: "Buildings",
          category: "buildings",
          param: "library",
          expanded: false,
          filterType: "checkbox",
          onChange: this.onFilterChange,
          items: []
        },
      }
    };

    this.currentPage = 0;
    this.$ajaxResults = $.ajax();
    this.queryString = queryString.parse(location.search); // params in the url
  }

  componentDidMount = () => {
    // fetch all features
    const getFeatures = this.fetchFeatures();
    // fetch all room types
    const getRoomTypes = this.fetchRoomTypes();
    // fetch all building types
    const getBuildings = this.fetchBuildings().then(data => {
      const filters = data.map(library => {
        return {
          label: library.name,
          value: library.id,
          checked: false
        };
      });

      if(filters.length === 1) {
        filters[0].checked = true;
      }

      return {
        filters,
        buildings: data
      };
    });

    Promise
    .all([getRoomTypes,getFeatures,getBuildings])
    .then(values => {
      let filters = Object.assign({}, this.state.filters);

      filters.roomTypes.items = filters.roomTypes.items.concat(values[0]);
      filters.features.items = filters.features.items.concat(values[1]);
      filters.buildings.items = filters.buildings.items.concat(values[2].filters);

      // hide the filters on narrow screens (max-width: 620px)
      const hideFilters = document.documentElement.clientWidth <= 620;

      // mark filters found in the URL
      filters = this.initializeFilters(filters);

      // update state and render filters
      this.setState({filters, buildings: values[2].buildings, hideFilters});

      // fetch results for filters in params
      this.fetchResults(filters);
    });
  }

  // ******* Gather Data **************** //

  // returns an array of properly formated filters
  fetchRoomTypes = () => {
    return fetch(`${ this.props.baseRequestUrl }/api/v1/taxonomy/room_types?_format=json`)
    .then(response => response.json())
    .then(data => {
      return data.map(roomType => {
        roomType.value = roomType.tid;
        roomType.checked = false;
        return roomType;
      });
    }).catch(error => {
      console.warn("failed to fetch room types:", error);
      return [];
    });
  }

  // returns an array of properly formated filters

  fetchFeatures = () => {
    return fetch(`${ this.props.baseRequestUrl }/api/v1/taxonomy/amenities?_format=json`)
    .then(response => response.json())
    .then(data => {
      return data.filter(feature => {
        feature.value = feature.tid;
        feature.checked = false;
        feature.excludeFromSpacefinder = (feature.excludeFromSpacefinder === "0") ? false : feature.excludeFromSpacefinder;
        feature.excludeFromSpacefinder = (feature.excludeFromSpacefinder === "1") ? true : feature.excludeFromSpacefinder;
        return !feature.excludeFromSpacefinder;
      });
    })
    .catch(error => {
      console.warn("failed to fetch features:", error);
      return [];
    });
  }

  // returns an array of properly formated filters
  fetchBuildings = () => {
    return fetch(`${ this.props.baseRequestUrl }/api/v1/library/rooms?_format=json`)
    .then(response => response.json())
    .then(data => data)
    .catch(error => {
      console.warn("failed to fetch libraries:", error);
      return [];
    });
  }

  // returns a specific room
  fetchRoom = (id) => {
    return fetch(`${ this.props.baseRequestUrl }/api/v1/rooms?id=${ id }&_format=json`)
    .then(response => response.json())
    .then(data => {
      return data.results && data.results.items
        ? data.results.items[0]
        : null;
    })
    .catch(error => {
      console.warn("failed to fetch room:", error);
      return null;
    });
  }

  // fetches results from the api based on the filter values passed
  // optional loadMore boolean is used to append or replace the existing results
  fetchResults = (filters, loadMore = false) => {
    if(loadMore) {
      this.currentPage++;
    } else {
      this.currentPage = 0;
    }

    let activeFilters = Object.assign({
      page: this.currentPage,
      _format: "json"
    }, this.queryString);

    if(activeFilters.seats_min || activeFilters.seats_max){
      activeFilters.number_seats = {};
    }

    if(activeFilters.seats_min){
      activeFilters.number_seats.min = activeFilters.seats_min;
    }

    if(activeFilters.seats_max){
      activeFilters.number_seats.max = activeFilters.seats_max;
    }

    this.$ajaxResults.abort();

    this.$ajaxResults = $.ajax({
      url: `${ this.props.baseRequestUrl }/api/v1/rooms`,
      data: activeFilters
    });

    this.$ajaxResults.done(data => {
      const currentResults = this.state.results;
      let mapResults = null;
      let newResults = {
        totalCount: data.results.totalCount || 0,
        items: []
      };

      if (typeof data.results.items === "undefined" || data.results.items.length === 0) {

        if(!loadMore) {
          this.setState({ results: newResults, mapResults });
        }
        return;
      }

      const newItems = this.parseResultItems(data.results.items);

      if(loadMore) {
        newResults.items = currentResults.items.concat(newItems);
      } else {
        newResults.items = newItems;
      }

      mapResults = this.createMarkers(newResults.items);

      this.setState({ results: newResults, mapResults });

      // if a specific room is requested fetch that and update views.
      if(this.queryString.room) {
        this.fetchRoom(this.queryString.room).then(data => {
          if(data === null) {
            return;
          }
          const activeSpace = this.parseResultItems([data])[0];
          const mapResults = this.createMarkers([activeSpace]);

          this.setState({ activeSpace, mapResults, showDetails: true, showResults: true, showMap: false });
        });
      }
    }).fail((xhr, status, error)=> {
      console.warn("failed to fetch results");

      let { results, mapResults } = this.state;
      results.totalCount = 0;
      results.items = [];
      mapResults = null;

      this.setState({ results, mapResults, showDetails: false });
    });
  }
  // ******* End Gather Data **************** //

  // ******* Data Manipulation **************** //

  // expects an array of results from the api
  // returns a clean array with the proper structure
  parseResultItems = (data) => {
    const buildings = this.state.buildings;

    return data.map(result => {
      result.library = R.find(R.propEq("id", result.library))(buildings);
      result.images = result.images.map(image => {
        return {
          src: image
        };
      });
      return result;
    });
  }

  // takes an array of room objects
  createMarkers = (data) => {
    // group items by library id value
    const groupByLibrary = R.groupBy(item => item.library.id || "1" );
    const markers = {};

    R.forEachObjIndexed( (value, libId) => {
      markers[libId] = {
        coordinates: {
          lat: value[0].coordinates.latitude,
          lng: value[0].coordinates.longitude
        },
        items: value,
        name: value[0].library.name
      };
    }, groupByLibrary(data));

    return markers;
  }

  // takes a string value of the spaces id
  // returns the first object that matches
  createSpaceDetail = (spaceId) => {
    const { results } = this.state;
    return results.items.find(result => result.id === spaceId);
  }

  initializeFilters = (filters) => {
    const params = this.queryString;
    const maxSeatsAllowed = filters.seats.items[0].max;
    const minSeatsAllowed = filters.seats.items[0].min;

    if(params.features && filters.features.items.length) {
      if(typeof params.features === "string") {
        params.features = [params.features];
      }
      filters.features.items = markItem(params.features, filters.features.items);
    }

    if(params.library && filters.buildings.items.length) {
      if(typeof params.library === "string") {
        params.library = [params.library];
      }
      filters.buildings.items = markItem(params.library, filters.buildings.items);
    }

    if(params.noise_level && filters.noise.items.length) {
      if(typeof params.noise_level === "string") {
        params.noise_level = [params.noise_level];
      }
      filters.noise.items = markItem(params.noise_level, filters.noise.items);
    }

    if(params.room_type && filters.roomTypes.items.length) {
      if(typeof params.room_type === "string") {
        params.room_type = [params.room_type];
      }
      filters.roomTypes.items = markItem(params.room_type, filters.roomTypes.items);
    }

    if(params.seats_min) {
      const minSeats = parseInt(params.seats_min);

      filters.seats.items[0].value.min = minSeats < minSeatsAllowed
        ? minSeatsAllowed
        : minSeats > maxSeatsAllowed
          ? maxSeatsAllowed
          : minSeats;
    }

    if(params.seats_max) {
      const maxSeats = parseInt(params.seats_max);

      filters.seats.items[0].value.max = maxSeats < minSeatsAllowed
        ? minSeatsAllowed
        : maxSeats > maxSeatsAllowed
          ? maxSeatsAllowed
          : maxSeats;
    }

    return filters;

    function markItem(param, filterItems) {
      param.forEach(paramValue => {
        const filterIndex = filterItems.findIndex(item => {
          return item.value === paramValue;
        });
        if(filterItems[filterIndex]){
          filterItems[filterIndex].checked = true;
        }
      });

      return filterItems;
    }
  }

  // function takes two optional values
  // the url parameter and the value it should equal
  // if neither a parameter or value are passed,
  // we assume the queryString has been updated else where
  // and we just need to reset the url
  updateUrl(param, value = null) {
    // if a parameter and a value are passed add the new value
    if(param && value) {
      this.queryString[param] = value;
    }
    // if a parameter is passed with a value remove the parameter
    if(param && (value === null || value.length === 0)) {
      delete this.queryString[param];
    }

    const stringifiedQueries = queryString.stringify(this.queryString);
    const newUrl = window.location.origin + window.location.pathname + "?" + stringifiedQueries;

    window.history.replaceState({path: newUrl}, "", newUrl);
  }

  // ******* End Data Manipulation **************** //


  // ******* Action Handlers **************** //
  // when an filter accordion expands or collapses
  // receives the key value of the filter and a boolean state
  onAccordionChange = (filterName, value) => {
    let filters = Object.assign({}, this.state.filters);

    for (var filterObj in filters) {
      if(filterName === filters[filterObj].param) {
        filters[filterObj].expanded = value;
        break;
      }
    }

    this.setState({ filters });
  }

  // when the clear all button is clicked
  // receives the event from the button
  onClearAll = (event) => {
    event.preventDefault();
    let filters = Object.assign({}, this.state.filters);
    // reset each group
    filters.roomTypes.items = filters.roomTypes.items.map(item => R.merge(item, {checked:false}));
    filters.noise.items = filters.noise.items.map(item => R.merge(item, {checked:false}));
    filters.features.items = filters.features.items.map(item => R.merge(item, {checked:false}));
    filters.buildings.items = filters.buildings.items.map(item => R.merge(item, {checked:false}));
    filters.seats.items[0].value.min = filters.seats.items[0].min;
    filters.seats.items[0].value.max = filters.seats.items[0].max;

    // clear the parameters in the url
    this.queryString = {};
    this.updateUrl();

    this.fetchResults(filters);

    this.setState({filters});
  }

  // when a filter is clicked
  // recieves the event from the checkbox
  onFilterChange = (event) => {
    // loops through the array looking for the checked property as true
    const allCheckedValues = R.compose(
      R.map(R.prop("value")),
      R.filter(R.prop("checked"))
    );

    const filterValue = event.target.value;
    const filterName = event.target.name;

    let filters = Object.assign({}, this.state.filters);

    for (var filterObj in filters) {
      if(filterName === filters[filterObj].param) {
        const filterIndex = filters[filterObj].items.findIndex(item => {
          return item.value === filterValue;
        });

        filters[filterObj].items[filterIndex].checked = !filters[filterObj].items[filterIndex].checked;
        this.updateUrl(filters[filterObj].param, allCheckedValues(filters[filterObj].items));

        // fetch new results
        this.fetchResults(filters);

        // update the state of the filters
        this.setState({ filters });

        break;
      }
    }
  }

  // when the user closes the room detail or expands the filters
  // hide the detail and repopulate the map
  onHideDetail = () => {
    const { results } = this.state;
    const mapResults = this.createMarkers(results.items);

    this.updateUrl("room", null);

    this.setState({ mapResults, showDetails: false});
  }

  // When the user clicks the >> or << button to show or hide the filters
  // receives the desired state as a boolean
  // true == hidden
  // false == visible
  onHideFilters = (value) => {
    this.setState({ hideFilters: value });

    if(this.state.showDetails) {
      this.onHideDetail();
    }
  }

  // When a user clicks on a result
  // links in Result list and the Map
  // recieves the id of the room
  onResultClick = (id) => {
    const activeSpace = this.createSpaceDetail(id);
    const mapResults = this.createMarkers([activeSpace]);

    this.updateUrl("room",id);

    this.setState({ activeSpace, mapResults, showDetails: true, showResults: true, showMap: false });
  }

  // When the user changes the range slider
  // receives an object with the desired values
  // min
  // max
  onSeatChange = (values) => {
    let filters = Object.assign({}, this.state.filters);
    const item = filters.seats.items[0];
    const maxSeatsAllowed = filters.seats.items[0].max;
    const minSeatsAllowed = filters.seats.items[0].min;

    // plugin allows keyboard navigation beyond the min and max scope
    // ignore these values
    if(values.min < item.min || values.max > item.max) {
      return false;
    }

    filters.seats.items[0].value.min = values.min;
    filters.seats.items[0].value.max = values.max;

    this.queryString.seats_min = values.min === minSeatsAllowed ? "0" : values.min;
    this.queryString.seats_max = values.max === maxSeatsAllowed ? "1000" : values.max;

    this.updateUrl();

    this.fetchResults(filters);
    this.setState({ filters });
  }

  onShowMore = (event) => {
    event.preventDefault();
    this.fetchResults(this.state.filters, true);
  }

  onTabChange = (tab) => {
    if(tab === "list") {
      this.setState({ showMap: false, showResults: true });
    }
    if(tab === "map") {
      this.setState({ showMap: true, showResults: false });
    }
    // if(this.state.showDetails) {
    //   this.onHideDetail();
    // }
  }

  // ******* End Action Handlers **************** //


  render() {
    const {
      activeSpace,
      filters,
      mapResults,
      results,
      showDetails,
      showMap,
      showResults,
      hideFilters
    } = this.state;

    const { baseAssetsUrl } = this.props;

    const gridModifier = showDetails
      ? "hl__space-finder__container--show-details"
      : hideFilters
        ? "hl__space-finder__container--no-filters"
        : " ";

    const resultsClass = !showResults ? "hl__space-finder__results--hidden" : "";
    const mapClass = !showMap ? "hl__space-finder__map--hidden" : "";

    const filterArray = [
      filters.roomTypes,
      filters.buildings,
      filters.noise,
      filters.seats,
      filters.features
    ];

    return (
      <div className={ `hl__space-finder__container ${ gridModifier }` }>
        <section className="hl__space-finder__menu-bar">
          <button
            disabled={ showResults }
            onClick={ this.onTabChange.bind(null,"list") }>List</button>
          <button
            disabled={ showMap }
            onClick={ this.onTabChange.bind(null,"map") }>Map</button>
        </section>
        <section className="hl__space-finder__filters">
          <FilterRail
            onAccordionChange={ this.onAccordionChange }
            onHideFilters={ this.onHideFilters }
            onClearAll={ this.onClearAll }
            assetsUrl={ baseAssetsUrl }
            title="Filter Spaces"
            hideFilters={ hideFilters || showDetails }
            message="With thanks to our colleagues at Cambridge University"
            groups= { filterArray } />
        </section>
        <section className={ `hl__space-finder__results ${ resultsClass }` }>
          <SpacesResults
            { ...results }
            activeSpace={ activeSpace }
            showDetails={ showDetails }
            noise={ filters.noise }
            assetsUrl={ baseAssetsUrl }
            handleResultClick={ this.onResultClick }
            handleCloseDetail={ this.onHideDetail }
            handleLoadMore={ this.onShowMore } />
        </section>
        <section className={ `hl__space-finder__map ${ mapClass }` }>
          <SpacesMap
            markers={ mapResults }
            assetsUrl={ baseAssetsUrl }
            handleInfoClick={ this.onResultClick } />
        </section>
      </div>
    );
  }
}
