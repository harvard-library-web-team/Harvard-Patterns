### Description
This pattern shows the breadcrumb navigation links.

### Status
* Stable as of 1.0.0

### Pattern Contains
* Link Tag

### Variables
~~~
breadcrumb: {
  light:
    type: boolean
  align:
    type string ("left", "") / optional
  items:
    type: array of linkTag / required
}
~~~
