import R                      from "ramda";
import React, { Component }   from "react";

import { object, func, string } from "prop-types";

export default class SpacesMap extends Component {

  static propTypes = {
    markers: object,
    assetsUrl: string.isRequired,
    handleInfoClick: func.isRequired
  };

  constructor(props) {
    super(props);

    this.map = null;
    this.markers = {};
    this.infoWindow = null;

    this.markerIcon = {};


    //https://snazzymaps.com/style/83/muted-blue
    this.mapTheme = [{
      "featureType": "all",
      "stylers": [{
        "saturation": 0
      },{
        "hue": "#e7ecf0"
      }]
    },{
      "featureType": "road",
      "stylers": [{
        "saturation": -70
      }]
    },{
      "featureType": "transit",
      "stylers": [{
        "visibility": "off"
      }]
    },{
      "featureType": "poi",
      "stylers": [{
        "visibility": "off"
      }]
    },{
      "featureType": "water",
      "stylers": [{
        "visibility": "simplified"
      },{
        "saturation": -60
      }]
    }];
  }

  componentDidMount() {
    this.loadGoogleJS();

    window.addEventListener("resize", this.centerMap);

    this.mapDiv.addEventListener("click", (event) => {
      if (event.target.classList.contains("js-spaces-info-window-link")) {
        this.props.handleInfoClick(event.target.getAttribute("data-id").toString());
      }
    });
  }

  componentWillUnmount() {
    this.mapDiv.removeEventListener("click");
  }

  componentDidUpdate() {
    this.updateMap(this.props.markers);
  }

  loadGoogleJS = () => {
    window.initGoogleMap = () => {

      this.googleMap = window.google.maps;

      this.markerIcon = {
        url: `${ this.props.assetsUrl }/assets/images/spaces-marker.png`,
        size: new this.googleMap.Size(22, 27),
        origin: new this.googleMap.Point(0, 0),
        anchor: new this.googleMap.Point(0, 32),
        labelOrigin: new this.googleMap.Point(11,13)
      };

      this.renderMap();
    };

    const script = document.createElement("script");
    const key ="AIzaSyAfD5FhipzkH1aLarvSxg8GlY24PvV56mo";

    script.type = "text/javascript";
    script.src = `//maps.googleapis.com/maps/api/js?key=${ key }&callback=initGoogleMap`;
    document.getElementsByTagName("head")[0].appendChild(script);
  }

  renderMap = () => {
    const styledMapType = new this.googleMap.StyledMapType(this.mapTheme,{name: "Map"});

    // Create a map object, and include the MapTypeId to add
    // to the map type control.
    this.map = new this.googleMap.Map(
      this.mapDiv,
      {
        center: {lat: 42.377252, lng: -71.116653},
        zoom: 18,
        mapTypeControlOptions: {
          mapTypeIds: ["styled_map","satellite"]
        }
      }
    );

    //Associate the styled map with the MapTypeId and set it to display.
    this.map.mapTypes.set("styled_map", styledMapType);
    this.map.setMapTypeId("styled_map");

    this.updateMap(this.props.markers);
  }

  updateMap = (data) => {
    // remove existing markers
    R.forEachObjIndexed( (marker) => {
      marker.setMap(null);
    }, this.markers);

    this.markers = {};

    // exit if the map hasn't been created
    if(this.map === null || !data) {
      // exit here
      return;
    }

    R.forEachObjIndexed( (value, libId) => {
      if(!value.coordinates.lat || !value.coordinates.lng) {
        // Harvard University Coordinates found in Google Maps
        value.coordinates.lat = 42.374276;
        value.coordinates.lng = -71.116240;
      }

      const infoWindowContent = this.createInfoWindow(value);

      const markerData = {
        position: new this.googleMap.LatLng(value.coordinates),
        label: {
          text: value.items.length >= 1 ? `${ value.items.length }` : " ", // the space is required
          color: "white"
        },
        icon: this.markerIcon,
        title: value.name,
        map: this.map
      };

      this.markers[libId] = new this.googleMap.Marker(markerData);

      this.markers[libId].addListener("click", (e) => {
        if (this.infowindow) {
          this.infowindow.close();
        }
        this.infowindow = new this.googleMap.InfoWindow({
          content: infoWindowContent
        });
        // show this info window
        this.infowindow.open(this.map, this.markers[libId]);
      });

      this.centerMap();
    }, data);

  }

  createInfoWindow = (data) => {
    if(data.items.length > 1) {
      const template = data.items.map((item) => {
        return (`
          <li class="hl__spaces-info-window__item">
            <a href="#" class="hl__spaces-info-window__link js-spaces-info-window-link" data-id=${ item.id }>
              ${ item.name } &mdash; ${ item.floor }
            </a>
          </li>
        `);
      }).join("\n");

      // return the list view
      return (`
        <section class="hl__spaces-info-window">
          <h2 class="hl__spaces-info-window__title">
            ${ data.name }
          </h2>
          <ul class="hl__spaces-info-window__items">
            ${ template }
          </ul>
        </section>
      `);
    } else {
      const item = data.items[0];
      const features = item.features.map(feature => {
        return (`
          <li class="hl__spaces-info-window__feature">
            <img src=${ feature.icon } alt=${ feature.name } title=${ feature.name } height="25" width="25" />
          </li>
        `);
      }).join("\n");
      // return the single view
      return (`
        <section class="hl__spaces-info-window">
          <h2 class="hl__spaces-info-window__title">
            <a href="#" class="hl__spaces-info-window__link js-spaces-info-window-link" data-id=${ item.id }>
              ${ item.name } &mdash; ${ item.floor }
            </a>
          </h2>
          <p class="hl__spaces-info-window__description">
            ${ item.description }
          </p>
          <ul class="hl__spaces-info-window__features">
            ${ features }
          </ul>
        </section>
      `);
    }
  }

  centerMap = () => {
    // reset the zoom to show all markers
    let bounds = new this.googleMap.LatLngBounds();
    R.forEachObjIndexed( (marker) => {
      bounds.extend(marker.getPosition());
    }, this.markers);

    this.map.fitBounds(bounds);

    // set a minimum zoom
    // if you got only 1 marker or all markers are on the same address map will be zoomed too much.
    if(this.map.getZoom() > 18){
      this.map.setZoom(18);
    }
  }

  render() {
    return (
      <div className="hl__spaces-map" ref={el => this.mapDiv = el}>Loading Google Maps</div>
    );
  }
}
