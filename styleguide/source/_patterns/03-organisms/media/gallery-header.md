### Description
This pattern shows a page header, image gallery, hours and other important information about the page details.

### Status
* Stable as of 1.0.0

### Pattern Contains
* Page Header
* Icon Callout
* Image Gallery
* Hours List
* Registration CTA

### JavaScript Used
* This pattern uses JavaScript for the gallery included

### Variables
~~~
galleryHeader: {
    type: pageHeader / required
  },
  hoursToday: {
    type: iconCallout / optional
  },
  megaIcon:
    type: string (path to icon) / optional
  imageGallery: {
    type: imageGallery / optional
  },

  hours: {
    type: hoursList / required
  },

  registration: {
    type: registrationCTA / optional
  }
}
~~~
