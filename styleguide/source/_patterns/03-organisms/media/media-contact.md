### Description
This pattern is used to display media contact information.

### Status
* Stable as of 1.0.0

### Pattern Contains
* Comp Heading
* Contact List
* Download List

### Variant options
* If more than one download is provided, this pattern will render in a [stacked layout](./?p=organisms-media-contact-with-more-files)

### Variables
~~~
mediaContact: {
  compHeading: {
    type: compHeading / required
  },
  contacts: {
    title: 
      type: string / required,
    contactList: {
      type: contactList / required
    }
  },
  downloads: {
    title: 
      type: string / required,
    downloadList: {
      type: downloadList / required
    }
  }
}
~~~
