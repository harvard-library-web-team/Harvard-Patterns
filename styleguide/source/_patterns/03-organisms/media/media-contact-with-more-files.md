### Description
This is a variant of the [Media Contact](./?p=organisms-media-contact) pattern showing an example with multiple items to download.

### How to generate
* Increase the length of the 'items' array to include multiple download links
