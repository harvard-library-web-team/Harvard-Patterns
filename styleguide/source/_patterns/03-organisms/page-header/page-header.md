### Description
This is pattern is used as a simple text based page header for story content.

### Status
* Stable as of 1.0.0

### Pattern Contains
* Page Title or Illustrated Title
* Story Meta
* Sub Text

### Variant options
* An [illustrated title](./?p=organisms-page-header-illustrated) can be used.
* A [background image](./?p=organisms-page-header-background) can be shown behind the text.


### Variables
~~~
pageHeader: {
  align:
    type string ("left", "") / optional
  bgNarrow:
    type: String (path to img file) / optional
  bgWide:
    type: String (path to img file) / optional
  light:
    type: boolean
  eyebrow:
    type: string / optional,
  eyebrowIcon:
    type: string (path to include) / optional
  title: 
    type: pageTitle or illustratedTitle / required,
  subText: {
    type: subText / optional
  }
  description:
    type: String / optional
  storyMeta: 
    type: storyMeta / optional
}
~~~
