### Description
This is variant of the [Page Header](./?p=organisms-page-header) showing an illustrated title and sub text with a background image.

### How to generate
* polulate the `bgWide` and `bgNarrow` variables with a path to an image
* polulate the `pageTitle.image` variable with a path to an image
* polulate the `subText` variable 
