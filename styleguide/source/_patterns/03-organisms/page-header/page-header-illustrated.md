### Description
This is variant of the [Page Header](./?p=organisms-page-header) showing an illustrated title and sub text.

### How to generate
* polulate the `pageTitle.icon` variable with a path to an SVG icon
* polulate the `subText` variable 
