### Description
This pattern shows a message and a button.

### Status
* Stable as of 1.0.0

### Pattern Contains
* Button

### Variables
~~~
"notificationBar": {
  "button": {
    type: button / required
  },
  "message": 
    type: string / required
}
~~~
