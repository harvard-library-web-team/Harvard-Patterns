### Description
This pattern shows the header for a child page.

### Status
* Stable as of 1.0.0

### Pattern Contains
* Linked Image

### Variant options
This pattern can be rendered with different colored bars: [blue](/?p=organisms-header-child), [teal](/?p=organisms-header-child-as-teal), [crimson](/?p=organisms-header-child-as-crimson), and [yellow](/?p=organisms-header-child-as-yellow).

### Variables
~~~
headerSpaces: {
  theme: string / optional
  title:
    type: string (html) / required
  logo: {
    type: linkedImage / required
  }
}
~~~
