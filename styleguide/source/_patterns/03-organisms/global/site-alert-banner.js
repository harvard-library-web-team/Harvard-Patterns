import $ from "jquery";

export default function () {

  function setCookie(name, value, expires) {
    if(typeof(expires) === 'number') {
      var d = new Date();
      d.setTime(d.getTime() + (expires*24*60*60*1000));
      var expires = "expires="+d.toUTCString();
      document.cookie = name + "=" + value + "; " + expires + "; path=/";
    } else {
      document.cookie = name + "=" + value + "; path=/";
    }
  }

  function getCookie(name) {
    var value = "; " + document.cookie;
    var parts = value.split("; " + name + "=");
    if (parts.length == 2) return parts.pop().split(";").shift();
  }

  const $classList = $("#hl__site-alert-banner").attr('class').split(/\s+/);
  // gets the timestamp class
  const $siteAlertTime = $classList[2];

  if (getCookie($siteAlertTime) == "dismissed") {
    $(".js-site-alert-banner").hide();
  }

  else {
    $(".js-site-alert-banner button").on("click",function(){
      $(".js-site-alert-banner").slideUp(300, function(){
        $(this).remove();
        setCookie($siteAlertTime, "dismissed", 30);
      });
    });
  }
}
