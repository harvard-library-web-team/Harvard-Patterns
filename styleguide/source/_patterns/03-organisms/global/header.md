### Description
This pattern is used at the top of every page on the site to provide navigation.

### Status
* Stable as of 1.0.0

### Pattern Contains
* Hamburger Button
* Linked Image
* Main Nav

### JavaScript Used
* This pattern uses JavaScript to control the menu interaction with the hamburger button

### Variables
~~~
header: {
  hamburgerButton: {
    type: hamburgerButton / required
  },
  logo: 
    type: linkedImage / required,
  mainNav: {
    type: mainNav / required
  }
}
~~~
