### Description
This pattern show two columns of content constricted to the page width.

### Status
* Stable as of 1.0.0

### Pattern Contains
* Comp Heading
* Rich Text
* Any pattern can be rendered in this pattern by setting the 'path' variable to the location of the pattern and setting the 'data' variable to container of the data object of that pattern.  
  * {% include item.path with item.data %}

### Variables
~~~
splitRow: {
  compHeading: {
    type: compHeading / optional
  },
  description: {
    type: richText / optional
  },
  leftColumn: [{
    path: 
      type: string / required,
    data: {
      type: object of pattern path above / required
    }
  }],
  rightColumn: [{
    path: 
      type: string / required,
    data: {
      type: object of pattern path above / required
    }
  }]
}
~~~
