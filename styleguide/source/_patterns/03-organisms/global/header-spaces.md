### Description
This pattern shows a header without a main nav.

### Status
* Stable as of 1.0.0

### Pattern Contains
* Linked Image

### Variables
~~~
headerSpaces: {
  title:
    type: string (html) / required
  logo: {
    type: linkedImage / required
  }
}
~~~
