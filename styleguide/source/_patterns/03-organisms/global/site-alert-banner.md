### Description
This pattern shows an important message.

### Status
* Stable as of 1.0.0

### Pattern Contains
* Rich Text

### Variables
~~~
siteAlertBanner: {
  icon:
    type: string (icon path) / required
  label:
    type: string / optional
  message: {
    type: richText / required
  }
}
~~~
