
export default function (window = window, document = document) {
  const state = {
    // find the header element
    el: document.querySelector(".js-header"),
    // default the sticky state to false
    isSticky: false
  };

  if(state.el) {

    // listen for the window to scroll
    window.addEventListener("scroll", (e) => {
      const newIsSticky = state.el.getBoundingClientRect().top === 0 && window.scrollY > 0;

      if(state.isSticky === newIsSticky) {
        return;
      }

      state.isSticky = newIsSticky;

      state.isSticky ? state.el.classList.add("is-sticky") : state.el.classList.remove("is-sticky");
    });
  }
}
