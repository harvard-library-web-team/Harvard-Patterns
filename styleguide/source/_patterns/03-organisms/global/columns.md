### Description
This pattern is used to display content in columns

### Status
* Stable as of 1.0.0

### Pattern Contains
* Any pattern can be rendered in this pattern by setting the 'path' variable to the location of the pattern and setting the 'data' variable to container of the data object of that pattern.  
  * {% include item.path with item.data %}

### Variables
~~~
columns: {
  showGap: 
    type: boolean
  columnContent: [{
    path: 
      type: string / required,
    data: {
      type: object of pattern path above / required
    }
  }]
}
~~~
