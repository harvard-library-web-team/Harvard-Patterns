### Description
This is a variant of the [Header Child](./?p=organisms-header-child) pattern to display it with the teal theme.

### How to generate
* set the `theme` variable to `teal`
