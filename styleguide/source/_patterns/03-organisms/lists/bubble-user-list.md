### Description
This pattern shows a list of bubble user images.

### Status
* Stable as of 1.0.0

### Pattern Contains
* Comp Heading
* Rich Text
* Bubble Figure

### Variables
~~~
contactList: {
  richText:
    type: richText / optional
  items:[{
    type: array of Bubble Figure / required
  }]
}
~~~
