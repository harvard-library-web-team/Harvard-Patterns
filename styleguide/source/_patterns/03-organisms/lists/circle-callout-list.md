### Description
This pattern shows a collection of circle callouts.

### Status
* Stable as of 1.0.0

### Pattern Contains
* Comp Heading
* Rich Text
* Circle Callout

### Variables
~~~
circleCalloutList: {
  compHeading: {
    type: compHeading / optional
  },
  description: {
    type: richText / optional
  },
  items: [{
    type: arrayOf circleCallout / required
  }]
}
~~~
