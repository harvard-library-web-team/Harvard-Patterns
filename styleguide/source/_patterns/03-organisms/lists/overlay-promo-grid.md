### Description
This is a pattern showing a grid of overlayed content.

### Status
* Stable as of 1.0.0

### Pattern Contains
* Overlay Promo
* Link Tag
* Comp Heading
* Rich Text

### Variant options
* This pattern can render with each item as a [link](./?p=organisms-promo-grid-as-links), [accordion](./?p=organisms-overlay-promo-grid-accordion), or [six-across](./?p=organisms-overlay-promo-grid-six-across)

### JavaScript Used
* This pattern uses JavaScript for the accordions (js/helpers/accordions.js)

### Variables
~~~
overlayPromoGrid: {
  maxItems:
    type: number / optional
  level:
    type: number / optional (defaults to 2)
  compHeading:
    type: compHeading / optional
  description:
    type: richText / optional
  seeAll:
    type: LinkTag / optional
  overlayPromos:
    type: Array of orverlayPromo / required
}
~~~
