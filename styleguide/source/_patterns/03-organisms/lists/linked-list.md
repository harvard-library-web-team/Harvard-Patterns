### Description
This pattern shows a list of links.

### Status
* Stable as of 1.0.0

### Pattern Contains
* Link Tag
* User link
* Comp Heading
* Pattern Heading

### Variant options
* With [bullets](./?p=organisms-linked-list-with-bullets)
* With a [user image](./?p=organisms-linked-list-with-users)
* With a [heading](./?p=organisms-linked-list-with-heading)

### Variables
~~~
linkedList: {
  bullets: 
    type: boolean,
  dividers:
    type: boolean,
  compHeading: {
    type: compHeading / optional
  }
  patternHeading: {
    type: patternHeading / optional
  },
  seeAll: {
    type: linkTag / optional
  }
  items: [{
    type: 
      type: string ("", "user") / optional,
    link: {
      type: linkTag / required
    },
    image: {
      type: image / optional
    }
  }]
}
~~~
