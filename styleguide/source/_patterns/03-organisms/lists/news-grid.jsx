import React from "react";
import NewsSummary from "../../02-molecules/teasers/news-teaser.jsx"

import { arrayOf, string, shape } from "prop-types";

const propTypes = {
  events: arrayOf(shape({
    id: string
  })).isRequired
};

const NewsGrid = (props) => {

  const renderNews = (events) => {
    return events.map((event, i) => {
      return (
        <NewsSummary
          key={ event.id + i }
          {...event}
        ></NewsSummary>
      );
    });
  };

  const { events } = props;
  return (
    <section className="hl__news-grid">
      <div className="hl__news-grid__container">
        { renderNews(events.slice(0,4)) }
        <section className="hl__pattern-cta ">
          <div className="hl__pattern-cta__container">
            <div className="hl__pattern-cta__title">
              Give Back
            </div>
            <div className="hl__pattern-cta__description">
              Giving back to the Harvard Library means you can make a difference for the bright minds of the future.
            </div>
            <div className="hl__pattern-cta__links">
              <a className="hl__button hl__button--small " href="https://library.harvard.edu/support-library"
                 title="Giving back to the Harvard Library means you can make a difference for the bright minds of the future."
                 data-ga-link="Primary CTA Large">
                Give back today
              </a>
            </div>
          </div>
        </section>
        { renderNews(events.slice(4)) }
      </div>
    </section>
  );
};

NewsGrid.propTypes = propTypes; // defined at the top of the file

export default NewsGrid;
