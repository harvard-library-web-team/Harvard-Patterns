### Description
This pattern shows a three by grid of category teasers and optional title and description.

### Status
* Stable as of 1.0.0

### Pattern Contains
* Comp Heading
* Rich Text
* Category Teaser
* Link Tag

### Variables
~~~
categoryTeaserGrid: {
  level: 
    type number / optional (defaults to 2)
  compHeading: {
    type: compHeading / optional
  },
  description: {
    type: richText / optional
  },
  seeAll: {
    type: linkTag / optional
  }
  items: [{
    type: arrayOf categoryTeaser / required
  }]
}
~~~
