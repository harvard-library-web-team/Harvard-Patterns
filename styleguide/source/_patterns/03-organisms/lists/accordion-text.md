### Description
This is a pattern showing text broken down by expandable sections.

### Status
* Stable

### Pattern Contains
* Comp Heading
* Rich Text
* Link Tag

### JavaScript Used
* This pattern uses JavaScript for the accordions (js/helpers/accordions.js) and expand/collapse feature

### Variables
~~~
accordionText: {
  level:
    type: number / optional
  compHeading:
    type: compHeading / optional
  description:
    type: richText / optional
  accordionSections:
    type: array of accordionItem / minimum of 2 required
}
~~~
