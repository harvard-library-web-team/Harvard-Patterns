### Description
This pattern shows a list of file for download.

### Status
* Stable as of 1.0.0

### Pattern Contains
* Comp Heading
* File Download

### Variables
~~~
downloadList: {
  compHeading: { 
    type: compHeading / optional
  items:[{
    type: array of File Download / required
  }]
}
~~~
