### Description
This is a variant of the [Linked List](./?p=organisms-linked-list) pattern showing an example with bullets.

### How to generate
* set the `bullets` variable to `true`
