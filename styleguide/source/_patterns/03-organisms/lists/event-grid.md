### Description
This pattern shows a collection of Event Teasers.

### Status
* Stable as of 1.0.0

### Pattern Contains
* Comp Heading
* Event Teaser
* Link Tag

### Variant options
* this pattern can be set to render as [two columns](./?p=organisms-event-grid-double) or with [pagination](./?p=organisms-event-grid-directory)


### Variables
~~~
{
  eventGrid: {
    level:
      type: number / optional (defaults to 2)
    compHeading: {
      type: compHeading / optional
    },
    columns:
      type: string ("double","single","triple","") / optional
    seeAll: {
      type: linkTag / optional
    },
    events: [{
      type: arrayOf Event Teaser / required
    }]
  }
}
~~~
