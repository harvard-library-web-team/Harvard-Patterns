### Description
This is a variant of the [Event Grid](./?p=organisms-event-grid) pattern showing an example rendered with pagination at the bottom of the page.

### How to generate
* set `eventDirectory` variable to `true`
