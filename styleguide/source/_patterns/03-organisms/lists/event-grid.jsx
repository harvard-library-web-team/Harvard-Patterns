import React from "react";
import EventSummary from "../../02-molecules/teasers/event-teaser.jsx"

import { arrayOf, string, shape } from "prop-types";

const propTypes = {
  events: arrayOf(shape({
    id: string
  })).isRequired
};

const EventGrid = (props) => {

  const renderEvents = (events) => {
    return events.map((event, i) => {
      return (
        <EventSummary
          key={ event.id + i }
          {...event}
        />
      );
    });
  };

  const { events } = props;

  return (
    <section className="hl__event-grid hl__event-grid--single">
      <div className="hl__event-grid__container">
        <div className="hl__event-grid__events">
          { renderEvents(events) }
        </div>
      </div>
    </section>
  );
};

EventGrid.propTypes = propTypes; // defined at the top of the file

export default EventGrid;
