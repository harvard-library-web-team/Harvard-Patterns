### Description
This is a variant of the [Promo Grid](./?p=organisms-promo-grid) pattern showing an example with the entire promo as a link without images.

### How to generate
* set each `imagePromo.isLink` variable to `true`
* set each `imagePromo.image` variable to `null`

