### Description
This pattern shows a collection of type promos.

### Status
* Stable as of 1.0.0

### Pattern Contains
* Comp Heading
* Rich Text
* Type Promo

### Variables
~~~
typeTeaserList: {
  level:
    type: number / optional (defaults to 2),
  compHeading: {
    type: compHeading / optional
  },
  description: {
    type: richText / optional
  },
  items: [{
    type: arrayOf typePromo / required
  }]
}
~~~
