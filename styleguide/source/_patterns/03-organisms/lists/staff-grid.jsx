import React        from "react";
import StaffCard  from "../../02-molecules/general/staff-card.jsx";

import { string, arrayOf, shape, bool } from "prop-types";

const propTypes = {
  assetsUrl: string.isRequired,
  results: arrayOf(shape({
    id: string.isRequired
  }))
};

const StaffGrid = (props) => {

  const renderResults = (results) => {
    return results.map(r => <StaffCard key={ r.id } assetsUrl={ props.assetsUrl } {...r } />);
  };

  const { results } = props;

  const gridClass = "hl__staff-grid--plain";

  return (
    <section className={ `hl__staff-grid ${ gridClass }` }>
      <div className="hl__staff-grid__container">
        { renderResults(results) }
      </div>
    </section>
  );
};

StaffGrid.propTypes = propTypes; // defined at the top of the file

export default StaffGrid;
