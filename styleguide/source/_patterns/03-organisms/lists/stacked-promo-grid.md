### Description
This is a pattern showing a grid of stacked promo content.

### Status
* Alpha

### Pattern Contains
* Comp Heading
* Rich Text
* Stacked Promo

### Variant options
* This pattern can be shown with [no image](./?p=organisms-stacked-promo-grid-no-image)

### Variables
~~~
stackedPromoGrid: {
  level:
    type: number / optional
  compHeading: 
    type: compHeading / optional
  description: 
    type: richText / optional
  stackedPromos: 
    type: Array of stackedPromo / required
}
~~~