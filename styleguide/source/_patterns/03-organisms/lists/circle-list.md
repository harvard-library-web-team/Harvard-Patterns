### Description
This is a sample of documentation to use for patterns.

### Status
* Stable as of 1.0.0

### Pattern Contains
* Comp Heading
* Rich Text
* Circle Promo

### Variables
~~~
contactList: {
  centered: 
    type: boolean
  compHeading: {
    type: compHeading / optional
  },
  description: {
    type: richText / optional
  },
  items: [{
    type: array of circlePromo / required
  }]
}
~~~
