import React        from "react";

import { string, arrayOf, shape, number } from "prop-types";

const propTypes = {
  featuredImages: arrayOf(shape({
    title: string.isRequired,
    id: string.isRequired,
    thumbnail: string,
    link: shape({
      href: string.isRequired,
      text: string
    }).isRequired
  })),
  totalImages: number.isRequired,
  seeMoreLink: string
};

const FeaturedImageList = (props) => {
  const renderImages = (featuredImages) => {
    return featuredImages.map(image => {
      const style = {
        backgroundImage: `url("${image.thumbnail}")`
      };

      return (<a 
          key={image.id} 
          className="hl__featured-image-list__image" 
          href={ image.link.href }
          title={ image.title }
          style={ style }>
          <img  
            src={ image.thumbnail } 
            alt={ image.title } />
        </a>);
    });
  };

  const { featuredImages, totalImages, seeMoreLink } = props;

  return (
    <section className="hl__featured-image-list">
      <div className="hl__featured-image-list__images">
        { renderImages(featuredImages) }
      </div>
      <div className="hl__featured-image-list__footer">
        <div className="hl__featured-image-list__count"><b>{ featuredImages.length }</b> of <b>{ totalImages }</b> Images</div>
        <div className="hl__featured-image-list__see-more">
          { seeMoreLink && (
            <a 
              className="hl__link-tag hl__link-tag--with-arrow " 
              href={ seeMoreLink } 
              title=""><span>More images from hollis</span></a>
          )}
        </div>
      </div>
    </section>
  );
};

FeaturedImageList.propTypes = propTypes; // defined at the top of the file

export default FeaturedImageList;
