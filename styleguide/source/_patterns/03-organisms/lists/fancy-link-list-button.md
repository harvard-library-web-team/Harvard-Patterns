### Description
This is a variant of the [Fancy Link List](./?p=organisms-fancy-link-list) pattern with centered title, centered text, and centered blue button below to see more fancy links.

### How to generate
* include fancyLinkList.see-more into twig file
