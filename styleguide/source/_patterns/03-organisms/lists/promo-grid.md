### Description
This is a pattern showing four image promos with an optional title.

### Status
* Stable as of 1.0.0

### Pattern Contains
* Image Promo
* Link Tag
* Comp Heading

### Variant options
* This pattern can render with each item as a [link](./?p=organisms-promo-grid-as-links)

### Variables
~~~
promoGrid: {
  hideDividers:
    type: boolean
  compHeading: 
    type: compHeading / optional
  seeAll: 
    type: LinkTag / optional
  imagePromos: 
    type: Array of imagePromo / required
}
~~~
