import React                      from "react";
import LibraryHoursTable          from "../../02-molecules/lists/library-hours-table.jsx";
import moment                     from "moment";

import { string, arrayOf, shape, number } from "prop-types";
import { momentObj }              from "react-moment-proptypes";

const propTypes = {
  libraries: arrayOf(shape({
    id: string.isRequired
  })),
  startDate: momentObj.isRequired,
  week: number.isRequired
};

const LibraryHoursList = (props) => {

  const renderResults = () => {
    const { libraries, startDate, week } = props;
    const days=["Sun","Mon","Tue","Wed","Thu","Fri","Sat"];

    const headerDates = days.map((day,i) =>{
      return {
        day,
        date: moment(startDate).add(i, "days").format("MMM DD")
      };
    });

    const dayToMark = week === 0 ? moment().day() : null;

    return libraries.map(library => {
      return (
        <div 
          key={ library.id }
          className="hl__library-hours-list__row">
          <LibraryHoursTable
            headerDates={ headerDates }
            mark={ dayToMark } 
            { ...library }/>
        </div>
      );
    });
  };


  return (
    <section className="hl__library-hours-list">
      <div className="hl__library-hours-list__container">
        { renderResults() }
      </div>
    </section>
  );
};

LibraryHoursList.propTypes = propTypes; // defined at the top of the file

export default LibraryHoursList;
