### Description
This is a pattern shows a grid of Teaser Links with an optional title.

### Status
* Stable as of 1.0.0

### Pattern Contains
* Comp Heading
* Teaser Link
* Link Tag

### Variables
~~~
promoGrid: {
  hideDividers:
    type: boolean,
  level: 
    type: number / optional (defaults to 2)
  compHeading: 
    type: compHeading / optional
  seeAll: 
    type: LinkTag / optional
  teaserLinks: 
    type: Array of imagePromo / required
}
~~~
