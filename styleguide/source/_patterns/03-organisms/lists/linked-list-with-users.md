### Description
This is a variant of the [Linked List](./?p=organisms-linked-list) pattern showing an example with a user profile.

### How to generate
* set the `type` variable to `user`
* populate the optional image fields
