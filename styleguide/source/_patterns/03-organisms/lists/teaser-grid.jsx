import React        from "react";
import TeaserLink   from "../../02-molecules/teasers/teaser-link.jsx";

import { arrayOf, string, shape } from "prop-types";

const propTypes = {
  results: arrayOf(shape({
    id: string
  })).isRequired
};

const TeaserGrid = (props) => {

  const renderList = (items) => {
    return items.map((item, i) => {
      return (
        <TeaserLink
          key={ item.id }
          { ...item }
        />
      );
    });
  };

  const { results } = props;

  return (
    <section className="hl__teaser-grid hl__teaser-grid--double hl__teaser-grid--no-dividers">
      <div className="hl__teaser-grid__container">
        <div className="hl__teaser-grid__grid">
          { renderList(results) }
        </div>
      </div>
    </section>

  );
};

TeaserGrid.propTypes = propTypes; // defined at the top of the file

export default TeaserGrid;
