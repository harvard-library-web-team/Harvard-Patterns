### Description
This is pattern shows a grid of Image Promos with a Patterned CTA in the fourth place.

### Status
* Stable as of 1.0.0

### Pattern Contains
* Image Promo
* Pattern CTA


### Variables
~~~
newsGrid: {
  patternCta: {
    type: patternCta / optional,
  },
  newsItems: [{
    type: array of imagePromo / required,
  }]
}
~~~
