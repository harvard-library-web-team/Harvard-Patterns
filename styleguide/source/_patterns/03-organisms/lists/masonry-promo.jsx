import React, { Component }   from "react";
import Masonry                from "react-masonry-component";
import R                      from "ramda";

import OverlayPromo           from "../../02-molecules/teasers/overlay-promo.jsx";
// import PatternCta             from "../../02-molecules/text/pattern-cta.jsx";

import { string, arrayOf, shape, object } from "prop-types";

export default class MasonryPromo extends Component {

  static propTypes = {
    results: arrayOf(shape({
      id: string.isRequired,
      title: shape({
        text: string.isRequired
      }).isRequired,
      cardType: string,
      card_image: object
    }))
  };

  renderItems = (items) => {
    return items.map(item => {
      let promo = R.assoc("title", item.title.text, item);
      promo.image = promo.card_image;

      return (
        <div
          className={ `hl__masonry-promo__item hl__masonry-promo__item--${ item.cardType }` }
          key={ promo.id }>
          <OverlayPromo { ...promo } />
        </div>
      );
    });
  }

  render() {
    const { results } = this.props;

    const masonryOptions = {
      columnWidth: ".hl__masonry-promo__sizing",
      itemSelector: ".hl__masonry-promo__item",
      percentPosition: true
    };

    return (
      <section className="hl__masonry-promo">
        <Masonry
          className="hl__masonry-promo__container" // default ''
          elementType={ "section" } // default 'div'
          options={ masonryOptions } // default {}
          disableImagesLoaded={ false } // default false
          updateOnEachImageLoad={ false } // default false and works only if disableImagesLoaded is false
        >
          <div className="hl__masonry-promo__sizing" aria-hidden="true" />
          { this.renderItems(results) }
        </Masonry>
      </section>
    );
  }
}
