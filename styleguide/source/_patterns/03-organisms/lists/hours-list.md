### Description
This patterns shows a highlighted label followed be a table of labelled data.

### Status
* Stable as of 1.0.0

### Pattern Contains
* Highlighted Text
* Tabular List

### Variables
~~~
"hoursList": {
  "label": {
    type: highlightedText / required
  }
  "schedule": {
    type: tabularList / required
  }
}
~~~
