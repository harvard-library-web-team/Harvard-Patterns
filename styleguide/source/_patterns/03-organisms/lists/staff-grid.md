### Description
This pattern is a list of staff cards for the staff directory. This is a React Component only.

### Status
* Stable

### Pattern Contains
* Staff Card
