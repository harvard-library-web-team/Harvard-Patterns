### Description
This pattern shows a list of contacts.

### Status
* Stable as of 1.0.0

### Pattern Contains
* Comp Heading
* Contact Info

### Variables
~~~
contactList: {
  compHeading: { 
    type: compHeading / optional
  items:[{
    type: array of Contact Info / required
  }]
}
~~~
