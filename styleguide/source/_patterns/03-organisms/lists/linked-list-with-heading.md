### Description
This is a variant of the [Linked List](./?p=organisms-linked-list) pattern showing an example with a Patterned heading and dividers.

### How to generate
* populate the `patternHeading` fields
* set the first item's `type` variable to `user` and populate the optional `image` fields
* set the third items's `href` variable to `""`
