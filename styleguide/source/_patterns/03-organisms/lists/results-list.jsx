import React              from "react";
import SearchResultItem   from "../../02-molecules/text/search-result-item.jsx";

import { string, arrayOf, shape } from "prop-types";

const propTypes = {
  results: arrayOf(shape({
    id: string.isRequired
  }))
};

const ResultsList = (props) => {
  const renderResults = (results) => {
    return results.map(result => {

      return (
        <SearchResultItem
          key={result.id}
          {...result}
        />
      );
    });
  };

  return (
    <section className="hl__results-list">
      <div className="hl__results-list__container">
        { renderResults(props.results) }
      </div>
    </section>
  );
};

ResultsList.propTypes = propTypes; // defined at the top of the file

export default ResultsList;
