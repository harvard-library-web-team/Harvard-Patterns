### Description
This pattern shows a list of ricth text.

### Status
* Stable as of 1.0.0

### Pattern Contains
* Comp Heading
* Rich text

### Variables
~~~
labeledList: {
  compHeading: {
    type: compHeading / optional
  }
  items:[{
    type: array of Rich Text / required
  }]
}
~~~
