import React        from "react";
import LibraryCard  from "../../02-molecules/teasers/library-card.jsx";
import currentDate  from "../../../js/helpers/currentDate.js";

import { string, arrayOf, shape } from "prop-types";

const propTypes = {
  libraries: arrayOf(shape({
    id: string.isRequired
  }))
};

const LibraryGrid = (props) => {

  const renderResults = (libraries) => {
    return libraries.map(library => {
      return <LibraryCard key={library.id} timeLabel={ currentDate() } { ...library } />;
    });
  };

  const { libraries } = props;

  return (
    <section className="hl__library-grid">
      <div className="hl__library-grid__container">
        { renderResults(libraries) }
      </div>
    </section>
  );
};

LibraryGrid.propTypes = propTypes; // defined at the top of the file

export default LibraryGrid;
