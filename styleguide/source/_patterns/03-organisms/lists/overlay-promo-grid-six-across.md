### Description
This is a variant of the [overlay promo grid](./?p=organisms-overlay-promo-grid) pattern showing a grid of overlayed content at with six items across.

### How to
* set `max-items` to `6`
* add `overlayPromos.library` field to .json file
