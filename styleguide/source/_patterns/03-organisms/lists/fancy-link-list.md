### Description
This pattern shows a list of Fancy Link patterns.

### Status
* Stable as of 1.0.0

### Pattern Contains
* Comp Heading
* Rich Text
* Fancy Link

### Variant options
* This pattern can render with each item as an [accordion](./?p=organisms-fancy-link-list-accordion) or with a [button](./?p=organisms-fancy-link-list-button)

### Variables
~~~
fancyLinkList: {
  maxItems:
    type: number / optional
  level:
    type: number / optional,
  compHeading: {
    type: compHeading / optional
  },
  description: {
    type: richText / optional
  },
  items: [{
    type: arrayOf fancyLink / required
  }],
  see_more:
    type: button / optional
}
~~~
