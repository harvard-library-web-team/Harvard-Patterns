### Description
This is a variant of the [Event Grid](./?p=organisms-event-grid) pattern showing an example rendered as two columns with images.

### How to generate
* set the `columns` variable to `double`
* populate the `image` variable for each item in `events`
