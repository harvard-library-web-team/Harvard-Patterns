### Description
This pattern shows a list of plain text items with a circled numbered list with optional title and description on top of the list.

### Status
* Alpha

### Pattern Contains
* Comp Heading
* Rich Text
* Circled Numbered List

### Variables
~~~
numberedList: {
  compHeading: {
    type: compHeading / optional
  },
  description: {
    type: richText / optional
  },
  circledNumberedList {
    type: circledNumberedList / required
  }
}
~~~
