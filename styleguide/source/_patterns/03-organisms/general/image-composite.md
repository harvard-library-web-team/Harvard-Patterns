### Description
This pattern shows an array of images with the option for content overlays using a dotted pattern.

### Status
* Stable as of 1.0.0

### Pattern Contains
* Rich Text
* Overlay Image

### Variant options
* [various image composite layouts (1-3 images)](./?p=pages-image-composite-variants)

### Variables
~~~
imageComposite: {
  compHeading: {
    type: compHeading / optional
  }
  description: {
    type: richText / optional
  }
  overlayImages: [{
    type: array of Overlay Image / required
  }]
}
~~~
