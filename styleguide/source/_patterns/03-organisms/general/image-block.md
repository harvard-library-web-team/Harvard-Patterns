### Description
This pattern shows full width image.

### Status
* Stable as of 1.0.0

### Variables
~~~
"imageBlock": {
  "narrow":
    type: string (path) / required,
  "wide": "",
    type: string (path) / required,
  "text": string / required
}
~~~
