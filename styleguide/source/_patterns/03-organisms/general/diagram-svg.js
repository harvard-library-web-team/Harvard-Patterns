import $ from "jquery";

export default function (window = window, document = document) {
  $(".js-diagram-jump-links").each(function() {
    const $el = $(this);
    const $links = $el.find("a").addClass("js-diagram-jump-links-icon");
    const activeClass = "is-active";

    // add class to jump links
    $links.addClass("js-diagram-jump-links-icon");

    // wrapper is used to take up the space when the links become sticky
    $el.wrap('<div class="hl__jump-links__wrapper" />');
    // once the wrapper exist, record it.
    const $wrapper = $el.parent();
    const headerBuffer = 90;

    let upperLimit;
    let debounceTimer;
    let activeAnchorIndex = -1;
    let anchors = [];
    let numAnchors = 0;
    let linkScrolling = false;

    $links.first().addClass(activeClass);

    setVariables();

    $el.addClass("is-ready");

    // update variables one more time to catch any post page load changes
    window.setTimeout(function(){
      setVariables();
      setPosition();
    },1000);

    $links.on('click',function(e) {
      e.preventDefault();

      let $link = $(this);

      activeAnchorIndex = $(this).data('index');
      // find the location of the desired link and scroll the page
      let position = anchors[activeAnchorIndex].position;
      // prevent the scroll event from updating active links
      linkScrolling = true;

      $("html,body").stop(true,true).animate({scrollTop:position}, '750', function(){
        linkScrolling = false;
        // Get the link hash target so we can bring focus to it
        let hash = anchors[activeAnchorIndex].hash;
        // bring focus to the item we just scrolled to
        $(hash).focus();
        // set the URL to the clicked hash
        history.replaceState(null, null, hash);
        // timing issue with window.scroll event firing.
        setTimeout(function(){
          // set this link as active.
          $el.find('.' + activeClass).removeClass(activeClass);
          $link.addClass(activeClass);
        },30);
      });
    });

    // make the links sticky
    $(window).resize(function() {
      if(typeof debounceTimer === "number") {
        window.clearTimeout(debounceTimer);
      }
      debounceTimer = window.setTimeout(function(){
        setVariables();
        setPosition();
      },300);
    });

    $(window).scroll(function () {
      setPosition();

      if(!linkScrolling){
      }
    });

    function setVariables() {
      const elHeight = $(".js-jump-links").outerHeight(true) || 0;

      upperLimit = $wrapper.offset().top - headerBuffer

      // locate the position of all of the anchor targets
      anchors = new Array;
      $links.each(function(i,e){
        const $thisLink = $(this),
          hash = $thisLink.attr("href"),
          position = $(hash).offset() ? $(hash).offset().top - elHeight - headerBuffer : upperLimit;

        anchors[i] = { hash, position };

        $thisLink.data('index',i);
      });

      // record the number of anchors for performance
      numAnchors = anchors.length;
    }

    function setPosition() {
      const windowTop = $(window).scrollTop();
    }
  });
}
