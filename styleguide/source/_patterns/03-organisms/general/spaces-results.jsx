import $                      from "jquery";
import React, { Component }   from "react";
import SpaceTeaser            from "../../02-molecules/teasers/space-teaser.jsx";
import SpaceDetail            from "../../02-molecules/general/spaces-detail.jsx";
import Button                 from "../../01-atoms/buttons/button.jsx";

import { string, arrayOf, number, shape, func, object, bool } from "prop-types";

export default class SpacesResults extends Component {

  static propTypes = {
    items: arrayOf(shape({
      name: string.isRequired,
      floor: string.isRequired
    })).isRequired,
    totalCount: number.isRequired,
    activeSpace: object.isRequired,
    assetsUrl: string.isRequired,
    noise: object.isRequired,
    showDetails: bool.isRequired,
    handleCloseDetail: func.isRequired,
    handleLoadMore: func.isRequired,
    handleResultClick: func.isRequired
  };

  componentDidUpdate() {
    if(this.props.items.length > 0) {
      const scrollIndex = this.props.items.length > 10
        ? Math.floor((this.props.items.length - 1) / 10) * 10
        : 0;
      const scrollPos = $(".hl__spaces-results__items").scrollTop() + $(".hl__space-teaser").eq(scrollIndex).position().top;

      $(".hl__spaces-results__items")
        .stop(true,true)
        .animate({scrollTop: scrollPos}, '750');
    }

    if(this.props.showDetails) {
      window.setTimeout(() => {
        this.closeLink.focus();
        window.dispatchEvent(new Event("resize")); // fixes carousel width
      }, 500);
    }
  }

  renderItems = (items) => {
    if(items.length === 0) {
      return (
        <div className="hl__spaces-results__no-results">
          No Results Found.
        </div>
      );
    }
    return items.map(item => {
      const keyValue = (`${ item.name }-${ item.floor }`).replace(/\s/g, "");
      const image = item.images[0] || {};

      return (
        <SpaceTeaser
          key={ keyValue }
          image= { image }
          assetsUrl = { this.props.assetsUrl }
          handleClick={ this.props.handleResultClick }
          {...item } />
      );
    });
  }

  onCloseDetail = (e) => {
    e.preventDefault();
    this.closeLink.blur();
    this.props.handleCloseDetail();
  }

  render() {

    const {
      assetsUrl,
      items,
      totalCount,
      showDetails,
      noise,
      activeSpace,
      handleLoadMore
    } = this.props;

    return (
      <section className="hl__spaces-results">
        { totalCount === items.length && (
          <header
            className="hl__spaces-results__header"
            aria-hidden={ showDetails }>
          <div className="hl__spaces-results__count">
            Showing <span>{ totalCount }</span> spaces
          </div>
          </header>
        )}
        { totalCount > items.length && (
          <header
            className="hl__spaces-results__header"
            aria-hidden={ showDetails }>
          <div className="hl__spaces-results__load-more">
          <Button
            text="Load More"
            info="load more rooms"
            size="small"
            type="button"
            handleClick={ handleLoadMore }/>
          </div>
          </header>
        )}
        <div
          className="hl__spaces-results__items"
          aria-hidden={ showDetails }>
          { this.renderItems(items) }
        </div>
        <div
          className="hl__spaces-results__detail"
          id="hl__spaces-results__detail"
          aria-hidden={ !showDetails }
          ref={(el) => { this.detailDiv = el; }}>
          <header><button
              className="hl__spaces-results__close-detail"
              type="button"
              onClick={ this.onCloseDetail }
              ref={(el) => { this.closeLink = el; }}>close</button>
          </header>
          { activeSpace.name && (
            <SpaceDetail
              noise={ noise }
              assetsUrl={ assetsUrl }
              { ... activeSpace } />
          )}
        </div>
      </section>
    );
  }
}
