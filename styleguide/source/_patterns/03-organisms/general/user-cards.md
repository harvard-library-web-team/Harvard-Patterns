### Description
This pattern shows information about a user for a given library.

### Status
* Stable as of 1.0.0

### Pattern Contains
* Comp Heading
* Rich Text
* User Bio
* Link Tag

### Variables
~~~
userCards: {
  compHeading: {
    type: compHeading / optional
  },
  description: {
    type: richText / optional
  },
  seeAll: {
    type: linkTag / optional
  },
  users: [{
    type: array of userBio / required
  }]
}
~~~
