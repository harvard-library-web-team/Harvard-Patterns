### Description
This pattern show featured news using promo image. It also display a sign up form and the follow us content.

### Status
* Stable as of 1.0.0

### Pattern Contains
* Comp Heading
* Rich Text
* Button
* Image Promo
* Single input form
* Follow us

### Variant options
* If [three promos](/?p=organisms-featured-news-with-three-promos) are used the social links and CTA are removed

### Variables
~~~
featuredNews: {
  compHeading: {
    type: compHeading / optional
  }
  description: {
    type: richText / optional
  }
  see_more: {
    type: button / optional
  }
  imagePromos: [{
    type: array of Image promo / required
  }]
  singleInputForm: {
    type: singleInputForm / required (if only two promos)
  }
  followUs: {
    type: followUs / required (if only two promos)
  }
}
~~~
