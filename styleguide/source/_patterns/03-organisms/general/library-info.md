### Description
This pattern shows library information.

### Status
* Alpha

### Pattern Contains
* Library Hours
* Illustrated List - small
* Contact Info
* Button - small
* Icon List
* Rich Text

### Variables
~~~
libraryInfo: {
  hours: {
    type: libraryHours / required
  },
  address: {
    type: contactInfo / required
  },
  contact: {
    type: contactInfo / required
  },
  links: {
    type: lingTag / optional
  }
  staffDirectoryLink: {
    type: button / optional
  },
  connect: {
    type: iconList / optional
  },
  moreInfo: {
    type: linkTag / required
  }
  compHeading: required,
  highlights: {
    type: richText / required
  },
  access: {
    type: richText / required,
    type: linkTag / required
  },
  cta: optional,
  patternCta: {
    type: patternCta / optional
  }



}
~~~
