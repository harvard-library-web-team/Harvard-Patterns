### Description
This is a variant of the [Show More Text](./?p=organisms-show-more-text) pattern showing an example where content editors can manually set the breakpoint to show/hide content in the rich text.

### How to generate
* Add `<!--break-->` into the rich-text where you want to begin hiding content
