### Description
This pattern shows a featured promo full width.

### Status
* Stable as of 1.0.0

### Pattern Contains
* Comp Heading
* Rich Text
* Image Promo

### Variables
~~~
featuredNews: {
  compHeading: {
    type: compHeading / optional
  }
  description: {
    type: richText / optional
  }
  imagePromo: {
    type: imagePromo / required
  }
}
~~~
