### Description
This pattern shows rich text cut off at a max height with a control to show all the text.

### Status
* Stable as of 1.0.0

### Variants
* [Manually set breakpoint](./?p=organisms-show-more-text-break)

### Pattern Contains
* Comp Heading
* Rich Text
* Link Tag

### JavaScript Used
* This pattern uses JavaScript for the expand and collapse feature

### Variables
~~~
showMoreText: {
  compHeading: {
    type: compHeading / optional
  },
  controls: {
    type: linkTag / required
  },
  content: {
    type: richText / required
  }
}
~~~
