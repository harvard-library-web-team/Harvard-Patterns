### Description
This pattern shows the contact information of a staff member.

### Status
* Stable

### Pattern Contains
* Comp Heading
* Image
* Contact Info

### Variant Options
* Set `photo` to `false` if image is not included
* A variant of this pattern shows the contact information of a library or office [Contact Info Banner Office](./?p=organisms-contact-info-banner-office)


### Variables
~~~
contactInfoBanner: {
  compHeading: {
    type: compHeading / required
  },
  image: {
    src: {
      type: string (image path) / optional
    }
  },
  name: {
    type: string / required
  },
  office: {
    type: boolean
  },
  contact: {
    type: contactInfo / required
  }
}
~~~
