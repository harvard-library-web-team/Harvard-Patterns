### Description
This pattern show featured events using Event teasers. 

### Status
* Dev

### Pattern Contains
* Comp Heading
* Rich Text
* Button
* Event teaser

### Variables
~~~
featuredEvents: {
  align:
    type: string ("left", "") / optional
  level: 
    type: number / optional (defaults to 2)
  compHeading: {
    type: compHeading / optional
  }
  description: {
    type: richText / optional
  }
  see_more: {
    type: Button / optional
  }
  seeAll: {
    type: linkTag / optional
  }
  eventTeasers:
    type: array of Event teaser / required
}
~~~
