import checkActive  from "../../../js/helpers/cssControlCode.js";
import $            from "jquery";

export default function () {

  $(".js-show-more-text").each(function(index){
    const $el = $(this);
    const $link = $el.find(".js-show-more-text-link");
    const $linkToggle = $el.find(".hl__show-more-text__controls");
    const $content = $el.find(".js-show-more-text-content");

    // for new version, based on breakpoint
    const $content_html = $content.html();
    const $findBreak = $content_html.indexOf("<!--break-->");

    if ($findBreak != -1) {
      // change classes so that they match up with accordions.js
      $el.addClass("hl__show-more-text--break");
      $content.removeClass("js-show-more-text-content");

      // toggle aria-hidden to "false"
      $linkToggle.attr("aria-hidden", "false");

      // split rich text into two sections
      var part1 = $content_html.substring(0, $findBreak);
      var part2 = $content_html.substring($findBreak);
      $content.html(part1 + "</section><section class='hl__rich-text js-show-more-text-content'>" + part2);

      const $richText = $el.find(".hl__rich-text.js-show-more-text-content");
      const id = $richText.attr("id") || "showMoreTextBreak" + (index + 1);
      let active = checkActive($el);
      let open = $el.hasClass("is-open");

      $richText.attr("id", id);
      $link.attr("aria-expanded",open).attr("aria-controls", id);

      if(open) {
        // setup the inline display block
        $richText.stop(true,true).slideDown();
      }

      $link.on("click",function(e){
        if(active) {
          e.preventDefault();
          open = $el.hasClass("is-open");
          if(open){
            $richText.stop(true,true).slideUp();
          } else {
            $richText.stop(true,true).slideDown();
          }
          $link.attr("aria-expanded",!open);
          $el.toggleClass("is-open");
        }
      });

      $(window).resize(function () {
        let temp = checkActive($el);

        if(temp !== active && !temp) {
          $richText.removeAttr("style");
          $el.removeClass("is-open");
          $link.attr("aria-expanded","false");
        }

        active = temp;
      }).resize();

    } else {
      // for old version, based on height
      const $content = $el.find(".js-show-more-text-content");

      const id = $content.attr("id") || "showMoreText" + (index + 1);
      let open = $el.hasClass("is-open");

      $content.attr("id", id);
      $link.attr("aria-expanded",open).attr("aria-controls", id);
      const contentHeight = $content.height();

      // only activate the code if there is enough content to warrent it.
      // using 50 as a random buffer to prevent hiding only a few lines of text
      if(contentHeight >= $content.find(".hl__rich-text").height() - 50) {
        $content.height("auto");
        return;
      }

      $el.addClass("is-active");

      if(open) {
        // setup the inline display block
        $content.height($content.find(".hl__rich-text").height());
      }

      $link.on("click",function(e){
        e.preventDefault();
        open = $el.hasClass("is-open");
        if(open){
          $content.height(contentHeight);
        } else {
          $content.height($content.find(".hl__rich-text").height());
        }
        $link.attr("aria-expanded",!open);
        $el.toggleClass("is-open");
      });
    }

  });
}
