### Description
Research Lifecycle diagram

### Status
* Stable

### Pattern Contains
* Comp Heading
* SVG


### Variables
~~~
diagramSVG: {
  compHeading: {
    type: compHeading / optional
}
~~~
