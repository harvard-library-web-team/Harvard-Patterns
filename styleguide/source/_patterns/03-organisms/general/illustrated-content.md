### Description
This pattern shows a title, description, and a call to action button with a variety of backgrounds.

### Status
* Stable as of 1.0.0

### Pattern Contains
* Comp Heading
* Rich Text
* Button

### Variant options
* [background image](./?p=organisms-illustrated-content-image)
* [light](./?p=organisms-illustrated-content-light)
* [dark](./?p=organisms-illustrated-content-dark)
* [light + background image](./?p=organisms-illustrated-content-light-image)
* [dark + background image](./?p=organisms-illustrated-content-dark-image)

### Variables
~~~
"illustratedContent": {
  "bgNarrow": 
    type: string (path) / optional,
  "bgWide": "",
    type: string (path) / optional,
  "theme": 
    type: string ("", "light", "dark") / optional,
  "compHeading": {
    type: compHeading / optional
  },
  "description": {
    type: richText / required
  },
  "cta": {
    type: button / optional
  }
}
~~~
