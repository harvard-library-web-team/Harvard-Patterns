### Description
This is a variant of the [Featured News](./?p=organisms-featured-news) pattern showing an example with three promos.

### How to generate
* populate the `imagePromos` variable to with three items
