### Description
This is a variant of the [Illustrated Content](./?p=organisms-illustrated-content) pattern showing an example with a background.

### How to generate
* set the `bgWide` and `bgNarrow` variables to an image path
