### Description
This is a variant of the [Illustrated Content](./?p=organisms-illustrated-content) pattern showing an example with a dark background.

### How to generate
* set the `theme` variable to `dark`
