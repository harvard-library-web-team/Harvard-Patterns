### Description
This is a variant of the [Contact Info Banner](./?p=organisms-contact-info-banner) showing the contact information of a library or office.

### How to generate
* Set `office` variable to `true`
* Include `address` variables from [Contact Info](.//?p=molecules-contact-info) in json file
* Set `photo` to `false` if image is not included
