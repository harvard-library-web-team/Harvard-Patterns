### Description
This is a variant of the [Illustrated Content](./?p=organisms-illustrated-content) pattern showing an example with a light background.

### How to generate
* set the `theme` variable to `light`
