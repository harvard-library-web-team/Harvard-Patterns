### Description
This pattern shows a Rich Text component with title and includes an id to target with anchor links.

### Status
* Stable as of 1.0.0

### Pattern Contains
* Comp Heading
* Rich Text


### Variables
~~~
compHeader: {
  id: 
    type: string (unique per page) / optional,
  compHeading: 
    type: compHeading / optional,
  description: {
    type: richText / optional
  }
}
~~~
