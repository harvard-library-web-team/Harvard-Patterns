import React     from "react";
import {render}  from "react-dom";

// list of react components
import LibraryFilters    from "../_patterns/02-molecules/forms/library-filters.jsx";
import LibraryCard       from "../_patterns/02-molecules/teasers/library-card.jsx";
import LibraryGrid       from "../_patterns/03-organisms/lists/library-grid.jsx";

// Library Grid
(function() {
  const el = document.querySelector(".js-library-grid-react");

  if(!el) {
    return;
  }
  const props = {
    libraries: [{
      id: "7031",
      featured: true,
      external: false,
      name: "Cabot Science Library",
      subTitle: "Optional Subtitle",
      icon: "/assets/images/svg-icons/atom.svg",
      href: "#",
      times: {
        currently_open: true,
        status: "24hours",
        hours: [{}],
        rendered: "24 Hours"
      },
      image: {
        src: "../../assets/images/sample/cabot-science-library.png",
        alt: "",
        height: "",
        width: ""
      },
      amenities: [{
        label: "Document Cameras",
        icon: "/assets/images/svg-icons/amenities-tech-media.svg"
      },{
        label: "Study Carrels",
        icon: "/assets/images/svg-icons/amenities-group-study-space.svg"
      },{
        label: "Scanners (B&W)",
        icon: "/assets/images/svg-icons/amenities-cafe.svg"
      },{
        label: "Printers (Color)",
        icon: "/assets/images/svg-icons/amenities-media.svg"
      },{
        label: "Scanners (Color)",
        icon: "/assets/images/svg-icons/amenities-tech-media.svg"
      }]
    },{
      id: "7049",
      featured: false,
      external: true,
      name: "Gutman Library",
      subTitle: "",
      icon: "",
      href: "#",
      times: {
        currently_open: true,
        status: "open",
        hours: [{
          from: "8am",
          to: "11pm"
        }],
        rendered: "8am - 11pm"
      },
      image: {
        src: "../../assets/images/sample/biblioteca-berenson.png",
        alt: "",
        height: "",
        width: ""
      },
      amenities: []
    },{
      id: "2",
      featured: false,
      external: true,
      name: "Biblioteca Berenson",
      subTitle: "",
      icon: "",
      href: "#",
      times: null,
      image: {
        src: "../../assets/images/sample/biblioteca-berenson.png",
        alt: "",
        height: "",
        width: ""
      },
      amenities: []
    }]
  };

  render(
    <LibraryGrid { ...props} />,
    el
  );
})();

// Library Card
(function() {
  const el = document.querySelector(".js-library-card-react");

  if(!el) {
    return;
  }
  const props = {
    id: "7031",
    featured: true,
    external: false,
    name: "Cabot Science Library",
    subTitle: "Optional Subtitle",
    icon: "/assets/images/svg-icons/atom.svg",
    href: "#",
    times: {
      currently_open: true,
      status: "24hours",
      hours: [{}],
      rendered: "24 Hours"
    },
    image: {
      src: "../../assets/images/sample/cabot-science-library.png",
      alt: "",
      height: "",
      width: ""
    },
    amenities: [{
      label: "Document Cameras",
      icon: "../images/svg-icons/amenities-tech-media.svg"
    },{
      label: "Study Carrels",
      icon: "../images/svg-icons/amenities-group-study-space.svg"
    },{
      label: "Scanners (B&W)",
      icon: "../images/svg-icons/amenities-cafe.svg"
    },{
      label: "Printers (Color)",
      icon: "../images/svg-icons/amenities-media.svg"
    },{
      label: "Scanners (Color)",
      icon: "../images/svg-icons/amenities-tech-media.svg"
    }],
    subjects: ["subject1","subject2"]
  };

  render(
    <LibraryCard { ...props} />,
    el
  );
})();

// Library Filters
(function() {
  const el = document.querySelector(".js-library-filters-react");

  if(!el) {
    return;
  }

  const handleFilterChange = (type, values) => {
  };

  const props = {
    filterValues: {
      amenities: [{
        label: "Document Cameras",
        value: "DocumentCameras",
        icon: "/assets/images/svg-icons/amenities-tech-media.svg"
      }],
      names: [],
      openNow: false,
      subjects: []
    },
    filterChange: handleFilterChange,
    libraries: [{
      id: "7031",
      name: "Cabot Science Library",
      amenities: [{
        label: "Document Cameras",
        value: "DocumentCameras",
        icon: "/assets/images/svg-icons/amenities-tech-media.svg"
      },{
        label: "Study Carrels",
        value: "StudyCarrels",
        icon: "/assets/images/svg-icons/amenities-group-study-space.svg"
      },{
        label: "Scanners (B&W)",
        value: "ScannersBW",
        icon: "/assets/images/svg-icons/amenities-cafe.svg"
      },{
        label: "Printers (Color)",
        value: "PrintersColor",
        icon: "/assets/images/svg-icons/amenities-media.svg"
      },{
        label: "Scanners (Color)",
        value: "ScannersColor",
        icon: "/assets/images/svg-icons/amenities-tech-media.svg"
      }],
      subjects: [{
        label: "subject1",
        value: "subject1"
      },{
        label: "subject2",
        value: "subject2"
      }]
    },{
      id: "7049",
      name: "Gutman Library",
      amenities: [{
        label: "Document Cameras",
        value: "DocumentCameras",
        icon: "/assets/images/svg-icons/amenities-tech-media.svg"
      },{
        label: "Printers (Color)",
        value: "PrintersColor",
        icon: "/assets/images/svg-icons/amenities-media.svg"
      },{
        label: "Scanners (Color)",
        value: "ScannersColor",
        icon: "/assets/images/svg-icons/amenities-tech-media.svg"
      }],
      subjects: [{
        label: "subject2",
        value: "subject2"
      },{
        label: "subject3",
        value: "subject3"
      }]
    }]
  };

  render(
    <LibraryFilters { ...props} />,
    el
  );
})();

