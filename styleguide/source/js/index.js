import accordion        from "./helpers/accordions.js";

import image            from "../_patterns/01-atoms/media/image.js";
import hamburgerButton  from "../_patterns/01-atoms/buttons/hamburger-button.js";

import utilityNav       from "../_patterns/02-molecules/navigation/utility-nav.js";

import searchBanner     from "../_patterns/02-molecules/forms/search-banner.js";
import imageGallery     from "../_patterns/02-molecules/media/image-gallery.js";
import mainNav          from "../_patterns/02-molecules/navigation/main-nav.js";
import jumpLinks        from "../_patterns/02-molecules/navigation/jump-links.js";

import homeHeading      from "../_patterns/02-molecules/general/home-heading.js";

import diagramSVG       from "../_patterns/03-organisms/general/diagram-svg.js";
import showMoreText     from "../_patterns/03-organisms/general/show-more-text.js";
import header           from "../_patterns/03-organisms/global/header.js";
import dismiss          from "../_patterns/03-organisms/global/site-alert-banner.js";

// global javascript
accordion();

// pattern specific
hamburgerButton(window,document);

mainNav(window,document);

jumpLinks(window,document);

image();

imageGallery();

homeHeading(window,document);
utilityNav(window,document);

searchBanner(window,document);

diagramSVG(window, document);

showMoreText();

header(window,document);

dismiss();
