export default () => {

  const months=["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
  const days=["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];

  const date = new Date( Date.now() );
  return `${ days[date.getDay()] }, ${ months[date.getMonth()] } ${ date.getDate() }`;
};
