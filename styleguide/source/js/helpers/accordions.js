import checkActive  from "./cssControlCode.js";
import $            from "jquery";

export default function () {

  $(".js-accordion").each(function(index){
    const $el = $(this);
    const $link = $el.find(".js-accordion-link");
    const $content = $el.find(".js-accordion-content");
    const id = $content.attr("id") || "accordion" + (index + 1);
    let active = checkActive($el);
    let open = $el.hasClass("is-open");

    $content.attr("id", id);
    $link.attr("aria-expanded",open).attr("aria-controls", id);

    if(open) {
      // setup the inline display block
      $content.stop(true,true).slideDown();
    }

    $link.on("click",function(e){
      if(active) {
        e.preventDefault();
        open = $el.hasClass("is-open");
        if(open){
          $content.stop(true,true).slideUp();
        } else {
          $content.stop(true,true).slideDown();
        }
        $link.attr("aria-expanded",!open);
        $el.toggleClass("is-open");
      }
    });

    $(window).resize(function () {
      let temp = checkActive($el);

      if(temp !== active && !temp) {
        $content.removeAttr("style");
        $el.removeClass("is-open");
        $link.attr("aria-expanded","false");
      }

      active = temp;
    }).resize();
  });
}
