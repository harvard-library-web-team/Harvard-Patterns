const gulp   = require("gulp");
const quench = require("./quench.js");
const debug  = require("gulp-debug");
const run    = require("gulp-run");
const R      = require("ramda");

module.exports = function copyTask(taskName, userConfig) {

  const plConfig = R.merge({
    /**
     * src       : glob of files to copy
     * dest      : destination folder
     * base      : *optional https://github.com/gulpjs/gulp/blob/master/docs/API.md#optionsbase
     * watch     : files to watch that will trigger a rerun when changed
     */
  }, userConfig);

  const { src, dest, watch } = plConfig;
  const command = "php " + src + "/core/console --generate --patternsonly";

  if (!src){
    quench.throwError(
      "Pattern Lab task requires a src!\n",
      `Given plConfig: ${JSON.stringify(plConfig, null, 2)}`
    );
  }

  gulp.task(taskName, gulp.series(function(cb) {
    run(command, {verbosity: 3}).exec("", function(err) {
      if (err) return cb(err); // return error
      cb(); // finished task
    });
  }));

  // run this task and watch if specified
  quench.maybeWatch(taskName, watch || src);

};
