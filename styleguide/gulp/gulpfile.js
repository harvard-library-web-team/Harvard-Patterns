/**
 *  See ./readme.md for usage
**/

// Include gulp and plugins
const gulp   = require("gulp");
const quench = require("./quench/quench.js");
const path   = require("path");

const projectRoot = path.resolve(__dirname, "..");

const buildTask    = require("./tasks/build.js")(projectRoot);

/* gulp local */
gulp.task("local", gulp.series(buildTask));

/* gulp build */
gulp.task("build", gulp.series(async function(cb) {
  
  quench.setDefaults({
    watch: false
  });

  buildTask(cb);
}));

/* gulp */
gulp.task("default", gulp.series(quench.logHelp));
