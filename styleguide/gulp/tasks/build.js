const gulp                  = require("gulp");
const runSequence           = require("gulp4-run-sequence");

const quench                = require("../quench/quench.js");
const createCopyTask        = require("../quench/createCopyTask.js");
const createJsTask          = require("../quench/createJsTask.js");
const createCssTask         = require("../quench/createCssTask.js");
const createPatternLabTask  = require("../quench/createPatternLabTask.js");
const createBrowserSyncTask = require("../quench/createBrowserSyncTask.js");

module.exports = function buildTask(projectRoot) {

  const buildDir = `${projectRoot}/public`;
  const clientDir = `${projectRoot}/source`;

  return function(cb){

    createCopyTask("build-copy", {
      src: [
        `${clientDir}/js/vendor/**`,
        `${clientDir}/fonts/**`,
        `${clientDir}/images/**`
      ],
      dest: `${buildDir}/assets`,
      base: `${clientDir}`
    });


    createJsTask("build-js", {
      dest: `${buildDir}/assets/js/`,
      files: [
        {
          gulpTaskId: "build-js-index",
          entry: `${clientDir}/js/index.js`,
          filename: "index.js",
          watch: [
            `${clientDir}/js/**/*.js`,
            `${clientDir}/js/**/*.jsx`,
            `${clientDir}/_patterns/**/*.js`,
            `${clientDir}/_patterns/**/*.jsx`
          ]
        },
        {
          gulpTaskId: "build-js-library-directory",
          entry: `${clientDir}/js/library-directory.js`,
          filename: "library-directory.js",
          watch: [
            `${clientDir}/js/**/*.js`,
            `${clientDir}/js/**/*.jsx`,
            `${clientDir}/_patterns/**/*.js`,
            `${clientDir}/_patterns/**/*.jsx`
          ]
        },
        {
          gulpTaskId: "build-js-alpha-directory",
          entry: `${clientDir}/js/alpha-directory.js`,
          filename: "alpha-directory.js",
          watch: [
            `${clientDir}/js/**/*.js`,
            `${clientDir}/js/**/*.jsx`,
            `${clientDir}/_patterns/**/*.js`,
            `${clientDir}/_patterns/**/*.jsx`
          ]
        },
        {
          gulpTaskId: "build-js-search-directory",
          entry: `${clientDir}/js/search-directory.js`,
          filename: "search-directory.js",
          watch: [
            `${clientDir}/js/**/*.js`,
            `${clientDir}/js/**/*.jsx`,
            `${clientDir}/_patterns/**/*.js`,
            `${clientDir}/_patterns/**/*.jsx`
          ]
        },
        {
          gulpTaskId: "build-js-event-directory",
          entry: `${clientDir}/js/event-directory.js`,
          filename: "event-directory.js",
          watch: [
            `${clientDir}/js/**/*.js`,
            `${clientDir}/js/**/*.jsx`,
            `${clientDir}/_patterns/**/*.js`,
            `${clientDir}/_patterns/**/*.jsx`
          ]
        },
        {
          gulpTaskId: "build-js-collection-directory",
          entry: `${clientDir}/js/collection-directory.js`,
          filename: "collection-directory.js",
          watch: [
            `${clientDir}/js/**/*.js`,
            `${clientDir}/js/**/*.jsx`,
            `${clientDir}/_patterns/**/*.js`,
            `${clientDir}/_patterns/**/*.jsx`
          ]
        },
        {
          gulpTaskId: "build-js-staff-directory",
          entry: `${clientDir}/js/staff-directory.js`,
          filename: "staff-directory.js",
          watch: [
            `${clientDir}/js/**/*.js`,
            `${clientDir}/js/**/*.jsx`,
            `${clientDir}/_patterns/**/*.js`,
            `${clientDir}/_patterns/**/*.jsx`
          ]
        },
        {
          gulpTaskId: "build-js-news-listing",
          entry: `${clientDir}/js/news-listing.js`,
          filename: "news-listing.js",
          watch: [
            `${clientDir}/js/**/*.js`,
            `${clientDir}/js/**/*.jsx`,
            `${clientDir}/_patterns/**/*.js`,
            `${clientDir}/_patterns/**/*.jsx`
          ]
        },
        {
          gulpTaskId: "build-react-patterns",
          entry: `${clientDir}/js/react-patterns.js`,
          filename: "react-patterns.js",
          watch: [
            `${clientDir}/js/**/*.js`,
            `${clientDir}/js/**/*.jsx`,
            `${clientDir}/_patterns/**/*.js`,
            `${clientDir}/_patterns/**/*.jsx`
          ]
        },
        {
          gulpTaskId: "build-space-finder",
          entry: `${clientDir}/js/space-finder.js`,
          filename: "space-finder.js",
          watch: [
            `${clientDir}/js/**/*.js`,
            `${clientDir}/js/**/*.jsx`,
            `${clientDir}/_patterns/**/*.js`,
            `${clientDir}/_patterns/**/*.jsx`
          ]
        },
        {
          gulpTaskId: "build-js-polyfill",
          entry: `${clientDir}/polyfill/index.js`,
          filename: "polyfill.js",
          watch: [
            `${clientDir}/polyfill/**`
          ]
        }
      ]
    });

    createCssTask("build-css", {
      sass: {
        includePaths: [
          `${projectRoot}/node_modules/react-select/scss`,
          `${projectRoot}/node_modules/node-normalize-scss`,
          `${projectRoot}/node_modules/slick-carousel/slick`
        ]
      },
      src: [
        `${clientDir}/scss/index.scss`,
        `${clientDir}/js/**/*.scss`
      ],
      dest: `${buildDir}/assets/css/`,
      watch: [
        `${clientDir}/**/*.scss`,
        `${clientDir}/js/**/*.scss`
      ],
      filename: "index.css"
    });

    createPatternLabTask("build-pattern-lab", {
      src: [
        projectRoot
      ],
      watch: [
        `${clientDir}/**/*.twig`,
        `${clientDir}/**/*.md`,
        `${clientDir}/**/*.json`
      ]
    });


    createBrowserSyncTask("build-browser-sync", {
      server: buildDir,
      files: [
        `${buildDir}/assets/**`,
        // prevent browser sync from reloading twice when the regular file (eg. index.js)
        // and the map file (eg. index.js.map) are generated
        "!**/*.map",
        // reload when pattern lab finishes building new patterns
        `${buildDir}/latest-change.txt` //,
        //`${buildDir}/styleguide/html/styleguide.html`
      ]
    });


    const buildTasks = ["build-js", "build-css", "build-copy", "build-pattern-lab"];

    if (quench.isWatching()){
      return runSequence(buildTasks, "build-browser-sync");
    }
    else {
      return runSequence(buildTasks);
    }
  };

};
