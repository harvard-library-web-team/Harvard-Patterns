# This script was borrowed from the open source Mass Gov Mayflower project.

#!/bin/bash
#
# Deploying Harvard to Github Pages
# -----------------------------------------------------
#
# Run from the repo root, must have a clean working directory.
#
# Usage:
# ./scripts/deploy-gh-pages.sh [-b (git-branch-or-tag)] [-t (remote-repo)]
#   -b Build source: the git branch or tag to build from (required)
#   -t Target repo owner: the target remote repo owner whose gh-pages branch is being pushed (required).
#              This will often be the <your-github-username>.
#
#   Example: ./scripts/deploy-gh-pages.sh -t velir -b DP-1234-my-branch-name
#
# Description:
# 1. Validate the passed arguments: build source and target repo
# 2. Attempt to checkout passed build source
# 3. --Removed this step --
# 4. Build pattern lab static assets
# 5. Copy static assets (build output: styleguide/public/) into a new temp directory
# 6. Initialize a temp repo in the temp directory
# 7. Commit all of the static asset files
# 8. Add the passed target as remote
# 9. Push all build assets to target remote gh-pages branch
# 10. Get back to Harvard-Patterns/styleguide directory
# 11. Remove the temp directory
# 12. Check out prior branch

# @todo
# - use AWS cli to push/rsync to bucket
# - build into ci process

# Steps to clean up after script execution
# Runs on success and failure.
function cleanup {
    # 10. Get back to Harvard-Patterns/styleguide directory
    line="Getting back to previous directory..."
    log "log" "$line";

    cd -

    # 11. Remove temp directory
    line="Cleaning up tmp dir..."
    log "log" "$line";

    rm -rf ~/tmp/hardvard-patterns

    # 12. Check out the previous branch
    line="Checking out your previous branch..."
    log "log" "$line";

    git checkout @{-1}
}

# Output success or error log during execution
function log {
    # parameters
    local theLogType=$1
    local theMessage=$2

    if [ "$theLogType" == "success" ]; then
        echo -e "\n\x1B[01;92m [success] "${theMessage}"\x1B[0m \n"
    elif [ "$theLogType" == "error" ]; then
        echo -e "\n\x1B[01;91m [error] "${theMessage}"\x1B[0m \n" >&2
    else
        echo -e "\n\x1B[01;34m [info] "${theMessage}"\x1B[0m \n"
    fi
}

# Default arguments
targetEnv=false
buildSrc=false
cname=false
assetsPath=false

# Get passed arguments
while getopts :b:t: option
do
    case "${option}" in
        b) buildSrc=${OPTARG};;
        t) owner=${OPTARG}
            targetEnv="${owner}/Harvard-Patterns";;
        : ) line="Missing argument for parameter [-${OPTARG}]";
              log "error" "$line";
              exit 1;;
        \? ) line="Whoops, this script only accepts arguments for: git build branch/tag [-b] and target repo [-t]";
             log "error" "$line";
             exit 1;;
    esac
done

# 1. Validate build source environment argument exists
if [ "$buildSrc" = false ];
then
    line="Whoops, we need a git branch or tag name to checkout and build from [-b]."
    log "error" "$line";
    exit 1;
fi

# Validate target environment argument exists
if [ "$targetEnv" = false ];
then
    line="Whoops, we need a target repo that we can push to [-t]."
    log "error" "$line";
    exit 1;
fi

# Validate that passed build source is a valid git branch or tag
git rev-parse ${buildSrc} &>/dev/null
if [ "$?" -ne 0 ];
then
    line="Hmmm, couldn't find a branch/tag named ${buildSrc}... check spelling and make sure you've pulled it."
    log "error" "$line";
    exit 1;
else
    line="Validated git build source: ${buildSrc}..."
    log "success" "$line";
fi

# Validate that passed target argument is a valid remote repo
TARGET_URL="git@github.com:"${targetEnv}".git"
git ls-remote ${TARGET_URL} &>/dev/null
if [ "$?" -ne 0 ];
then
    line="Unable to reach remote repo at '${TARGET_URL}'. Check your target repo, should be something like '<your-github-username>/Harvard-Patterns'."
    log "error" "$line";
    exit 1;
else
    line="Validated target remote repo url: ${TARGET_URL}..."
    log "success" "$line";
fi

# Local variables
NOW=$(date +"%c")
MESSAGE="GH Pages deployed ${buildSrc} on: ${NOW}"

# 2. Checkout the build source
line="Checking out the build source: ${buildSrc}"
log "log" "$line";

git checkout ${buildSrc}

# Get to styleguide directory (inside of repo root), does not assume repo root is "Harvard-Patterns"
line="Changing directory into Harvard-Patterns/styleguide..."
log "log" "$line";

cd $(git rev-parse --show-toplevel)/styleguide

# 4. Build pattern to generate prod static assets
line="Generating Harvard Patterns patterns..."
log "log" "$line";
php core/console --generate

line="Building Harvard Patterns static assets..."
log "log" "$line";
gulp build

# Make temp directory to copy public  assets
line="Making ~/tmp/harvard-patterns directory..."
log "log" "$line";

if [ -d "~/tmp" ];
then
    mkdir ~/tmp/harvard-patterns
else
    mkdir ~/tmp
    mkdir ~/tmp/harvard-patterns
fi

# 5. Copy built assets in /public into temp directory
line="Copying Pattern Lab build output to ~/tmp/harvard-patterns directory..."
log "log" "$line";

cp -R public ~/tmp/harvard-patterns >/dev/null

# Get to temp directory build output
line="Changing directory to ~/tmp/harvard-patterns/public..."
log "log" "$line";

cd ~/tmp/harvard-patterns/public

# 6. Initialize temp git repo + push up to gh-pages
line="Creating temporary repo and committing build to master branch..."
log "log" "$line";

git init
git add . >/dev/null

# 7. Commit the built assets
git commit -m "$MESSAGE" >/dev/null

# 8. Add target as remote repo
line="Adding ${TARGET_URL} as a remote and force pushing build to gh-pages branch..."
log "log" "$line";

git remote add target ${TARGET_URL}

# 9. Make sure we can push to remote, return success or error based on result.
if [[ "$(git push target master:refs/heads/gh-pages --force --porcelain)" == *"Done"* ]]
then
    line="Git push was successful!"
    log "success" "$line";
    cleanup
    # Success message.
    line="Woo-hoo! Deploy complete! You should be able to see your updates at your Harvard-Patterns fork's Github Pages: \n https://${owner}.github.io/Harvard-Patterns"
    log "success" "$line";
else
    line="Hmmm, looks like we couldn't push.  Check your remote repo permissions."
    log "error" "$line";
    cleanup
    line="Bummer! Deploy unsuccessful. Check your spellings for git branches + remotes.  And check your permissions.  Then try again!"
    log "error" "$line";
    exit 1;
fi
