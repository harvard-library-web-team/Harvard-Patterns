# Harvard Patterns 
Welcome to the Harvard Library's Pattern Library.

## Setting up your environment

In order to run Harvard Patterns locally, you need to have some things installed and set up on your machine:

1. Install PHP ([v8.2](https://www.php.net/downloads.php))
    - See steps for [windows](https://www.sitepoint.com/how-to-install-php-on-windows/) || [mac os](https://formulae.brew.sh/formula/php@7.4)
2. Install NodeJS ([v21.6.1+](https://nodejs.org/en/download/)
3. Install GulpJS [globally](https://docs.npmjs.com/getting-started/installing-npm-packages-globally) 
    - Run `sudo npm install -g gulp-cli` from your command line

4. Clone the repo (pick one below)
    - SSH -> 'git@gitlab.com:harvard-library-web-team/Harvard-Patterns.git'
    - https -> 'https://gitlab.com/harvard-library-web-team/Harvard-Patterns.git'

1. Install Composer
    - Install by command linke instructions can be found here: https://getcomposer.org/download/
    - I had to add a fourth command to those steps to make it work on a Mac
    ~~~
    sudo mv composer.phar /usr/local/bin/composer
    ~~~

5. Install Pattern Lab
    - Browse to the 'styleguide' folder within your local repo using Terminal or Command Prompt
    - Run `composer install`
    - Warning: do not update the `styleguideKitPath` or `ishMinimum` variables when prompted

6. Install Node Modules
    - Browse to the 'styleguide' folder within your local repo using Terminal or Command Prompt
    - Run `npm install` successfully

## Starting up Pattern Lab

1. Browse to the 'styleguide' folder within your local repo using Terminal or Command Prompt

2. Run `gulp local` successfully

3. Open your browser and go to the hostname shown (ie: localhost:3000)

## GitLab Pages for this project
This project is viewable at https://harvard-library-web-team.gitlab.io/Harvard-Patterns

